--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;

insert into usr_institution (id,keyword,name,url,institution_type_id) values
    (nextval('usr_institution_id_seq'),'INSTITution1','INSTITution1 name','http://none',1);
insert into usr_institution (id,keyword,name,url,institution_type_id) values
    (nextval('usr_institution_id_seq'),'institution2','institution2 name','http://none',2);
insert into usr_institution (id,keyword,name,url,institution_type_id) values
    (nextval('usr_institution_id_seq'),'institution3','institution3 name','http://none',3);
insert into usr_institution (id,keyword,name,url,institution_type_id) values
    (nextval('usr_institution_id_seq'),'institution4','institution4 name','http://none',4);

insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'useR1@gazelle.com',MD5('aZeRtY'),'useR1','useR1 fn','useR1 ln',
        (SELECT  id from usr_institution where keyword = 'INSTITution1'),true,false);
--insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
--    (nextval('usr_users_id_seq'),'useR1@gazelle.com',crypt(MD5('aZeRtY'), gen_salt('bf', 12)),'useR1','useR1 fn','useR1 ln',
--        (SELECT  id from usr_institution where keyword = 'INSTITution1'),true,false);
insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user2@gazelle.com',MD5('aZeRtY'),'user2','user2 fn','user2 ln',
        (SELECT  id from usr_institution where keyword = 'institution2'),true,false);
insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user3@gazelle.com',MD5('aZeRtY'),'user3','user3 fn','user3 ln',
        (SELECT  id from usr_institution where keyword = 'institution3'),true,false);
insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user4@gazelle.com',MD5('aZeRtY'),'user4','user4 fn','user4 ln',
        (SELECT  id from usr_institution where keyword = 'institution4'),true,false);

insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user5@gazelle.com',MD5('aZeRtY'),'user5','user5 fn','user5 ln',
        (SELECT  id from usr_institution where keyword = 'institution4'),true,false);
insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'User5@gazelle.com',MD5('aZeRtY'),'user5b','user5b fn','user5b ln',
        (SELECT  id from usr_institution where keyword = 'institution4'),true,false);

insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user6@gazelle.com',MD5('aZeRtY'),'user6','user6 fn','user6 ln',
        (SELECT  id from usr_institution where keyword = 'institution4'),true,false);
insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values
    (nextval('usr_users_id_seq'),'user6b@gazelle.com',MD5('aZeRtY'),'USEr6','USEr6 fn','USEr6 ln',
        (SELECT  id from usr_institution where keyword = 'institution4'),true,false);

insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'useR1'), (select id from usr_role where name ='admin_role'));
insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'user2'), (select id from usr_role where name ='monitor_role'));
insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'user2'), (select id from usr_role where name ='accounting_role'));
insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'user3'), (select id from usr_role where name ='vendor_role'));
insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'user3'), (select id from usr_role where name ='vendor_admin_role'));
insert into usr_user_role (user_id,role_id) values
    ((select id from usr_users where username = 'user4'), (select id from usr_role where name ='user_role'));
