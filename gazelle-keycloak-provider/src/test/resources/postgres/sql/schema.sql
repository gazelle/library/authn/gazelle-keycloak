--
-- PostgreSQL database dump
--

-- Dumped from database version 10.19 (Ubuntu 10.19-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.19 (Ubuntu 10.19-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cmn_application_preference; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_application_preference (
                                                   id integer NOT NULL,
                                                   class_name character varying(255) NOT NULL,
                                                   description character varying(255) NOT NULL,
                                                   preference_name character varying(64) NOT NULL,
                                                   preference_value character varying(512) NOT NULL
);


ALTER TABLE public.cmn_application_preference OWNER TO gazelle;

--
-- Name: cmn_application_preference_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_application_preference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_application_preference_id_seq OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.cmn_path_linking_a_document (
                                                    id integer NOT NULL,
                                                    comment character varying(255),
                                                    path character varying(255) NOT NULL,
                                                    type character varying(255) NOT NULL
);


ALTER TABLE public.cmn_path_linking_a_document OWNER TO gazelle;

--
-- Name: cmn_path_linking_a_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.cmn_path_linking_a_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cmn_path_linking_a_document_id_seq OWNER TO gazelle;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO gazelle;

--
-- Name: message_validation_service; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.message_validation_service (
                                                   id integer NOT NULL,
                                                   tm_test_step_message_profile_id integer,
                                                   validation_service_id integer
);


ALTER TABLE public.message_validation_service OWNER TO gazelle;

--
-- Name: message_validation_service_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.message_validation_service_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.message_validation_service_sequence OWNER TO gazelle;

--
-- Name: pr_crawler_changed_integration_statements; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_crawler_changed_integration_statements (
                                                                  crawler_report_id integer NOT NULL,
                                                                  system_id integer NOT NULL
);


ALTER TABLE public.pr_crawler_changed_integration_statements OWNER TO gazelle;

--
-- Name: pr_crawler_reporting; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_crawler_reporting (
                                             id integer NOT NULL,
                                             execution_elapsed_time integer,
                                             execution_starting_date timestamp without time zone,
                                             number_of_changed_integration_statements integer,
                                             number_of_integration_statements_checked integer,
                                             number_of_successfully_checked integer,
                                             number_of_unmatching_hashcode_for_integration_statements integer,
                                             number_of_unreachable_integration_statements integer
);


ALTER TABLE public.pr_crawler_reporting OWNER TO gazelle;

--
-- Name: pr_crawler_reporting_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_crawler_reporting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_crawler_reporting_id_seq OWNER TO gazelle;

--
-- Name: pr_crawler_unmatching_hascode_for_integration_statements; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_crawler_unmatching_hascode_for_integration_statements (
                                                                                 crawler_report_id integer NOT NULL,
                                                                                 system_id integer NOT NULL
);


ALTER TABLE public.pr_crawler_unmatching_hascode_for_integration_statements OWNER TO gazelle;

--
-- Name: pr_crawler_unreachable_integration_statements; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_crawler_unreachable_integration_statements (
                                                                      crawler_report_id integer NOT NULL,
                                                                      system_id integer NOT NULL
);


ALTER TABLE public.pr_crawler_unreachable_integration_statements OWNER TO gazelle;

--
-- Name: pr_integration_statement_download; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_integration_statement_download (
                                                          id integer NOT NULL,
                                                          browser_information character varying(255),
                                                          date timestamp without time zone,
                                                          performer_hostname character varying(255),
                                                          performer_ip_address character varying(255),
                                                          performer_username character varying(255),
                                                          search_log_report_id integer,
                                                          system_id integer NOT NULL
);


ALTER TABLE public.pr_integration_statement_download OWNER TO gazelle;

--
-- Name: pr_integration_statement_download_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_integration_statement_download_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_integration_statement_download_id_seq OWNER TO gazelle;

--
-- Name: pr_intro; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_intro (
                                 id integer NOT NULL,
                                 language character varying(255),
                                 main_panel_content text,
                                 main_panel_header text
);


ALTER TABLE public.pr_intro OWNER TO gazelle;

--
-- Name: pr_intro_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_intro_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_intro_id_seq OWNER TO gazelle;

--
-- Name: pr_mail; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_mail (
                                id integer NOT NULL,
                                page_name character varying(255),
                                sent boolean,
                                crawlerreporting_id integer,
                                system_id integer
);


ALTER TABLE public.pr_mail OWNER TO gazelle;

--
-- Name: pr_mail_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_mail_id_seq OWNER TO gazelle;

--
-- Name: pr_search_criteria_per_report; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_search_criteria_per_report (
                                                      search_log_report_id integer NOT NULL,
                                                      search_log_criterion_id integer NOT NULL
);


ALTER TABLE public.pr_search_criteria_per_report OWNER TO gazelle;

--
-- Name: pr_search_log_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_search_log_criterion (
                                                id integer NOT NULL,
                                                criterion character varying(255),
                                                isbeforedate boolean,
                                                value_date timestamp without time zone,
                                                value_id integer,
                                                text_value character varying(255)
);


ALTER TABLE public.pr_search_log_criterion OWNER TO gazelle;

--
-- Name: pr_search_log_criterion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_search_log_criterion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_search_log_criterion_id_seq OWNER TO gazelle;

--
-- Name: pr_search_log_report; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_search_log_report (
                                             id integer NOT NULL,
                                             browser_information character varying(255),
                                             date timestamp without time zone,
                                             performer_hostname character varying(255),
                                             performer_ip_address character varying(255),
                                             performer_username character varying(255),
                                             number_of_criteria integer,
                                             number_of_found_integration_statements integer
);


ALTER TABLE public.pr_search_log_report OWNER TO gazelle;

--
-- Name: pr_search_log_report_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.pr_search_log_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pr_search_log_report_id_seq OWNER TO gazelle;

--
-- Name: pr_search_log_systems_found; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.pr_search_log_systems_found (
                                                    search_report_id integer NOT NULL,
                                                    system_id integer NOT NULL
);


ALTER TABLE public.pr_search_log_systems_found OWNER TO gazelle;

--
-- Name: proxy_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.proxy_type (
                                   id integer NOT NULL,
                                   description character varying(255),
                                   keyword character varying(255),
                                   label_key_for_display character varying(255)
);


ALTER TABLE public.proxy_type OWNER TO gazelle;

--
-- Name: proxy_type_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.proxy_type_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proxy_type_sequence OWNER TO gazelle;

--
-- Name: revinfo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.revinfo (
                                rev integer NOT NULL,
                                revtstmp bigint
);


ALTER TABLE public.revinfo OWNER TO gazelle;

--
-- Name: sys_dicom_documents_for_systems; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_dicom_documents_for_systems (
                                                        system_id integer NOT NULL,
                                                        path_linking_a_document_id integer NOT NULL
);


ALTER TABLE public.sys_dicom_documents_for_systems OWNER TO gazelle;

--
-- Name: sys_hl7_documents_for_systems; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_hl7_documents_for_systems (
                                                      system_id integer NOT NULL,
                                                      path_linking_a_document_id integer NOT NULL
);


ALTER TABLE public.sys_hl7_documents_for_systems OWNER TO gazelle;

--
-- Name: sys_system_subtype; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_system_subtype (
                                           id integer NOT NULL,
                                           last_changed timestamp with time zone,
                                           last_modifier_id character varying(255),
                                           subtype_for_quick_search boolean,
                                           description character varying(255),
                                           keyword character varying(255) NOT NULL
);


ALTER TABLE public.sys_system_subtype OWNER TO gazelle;

--
-- Name: sys_system_subtype_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.sys_system_subtype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_system_subtype_id_seq OWNER TO gazelle;

--
-- Name: sys_system_subtypes_per_system_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_system_subtypes_per_system_type (
                                                            id integer NOT NULL,
                                                            last_changed timestamp with time zone,
                                                            last_modifier_id character varying(255),
                                                            system_subtype_id integer,
                                                            system_type_id integer NOT NULL
);


ALTER TABLE public.sys_system_subtypes_per_system_type OWNER TO gazelle;

--
-- Name: sys_system_subtypes_per_system_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.sys_system_subtypes_per_system_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_system_subtypes_per_system_type_id_seq OWNER TO gazelle;

--
-- Name: sys_system_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_system_type (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        description character varying(255),
                                        keyword character varying(255) NOT NULL,
                                        is_visible boolean
);


ALTER TABLE public.sys_system_type OWNER TO gazelle;

--
-- Name: sys_system_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.sys_system_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_system_type_id_seq OWNER TO gazelle;

--
-- Name: sys_table_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.sys_table_session (
                                          id integer NOT NULL,
                                          lastchanged timestamp without time zone,
                                          lastmodifierid character varying(255),
                                          table_keyword character varying(255) NOT NULL
);


ALTER TABLE public.sys_table_session OWNER TO gazelle;

--
-- Name: sys_table_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.sys_table_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_table_session_id_seq OWNER TO gazelle;

--
-- Name: tf_actor; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor (
                                 id integer NOT NULL,
                                 last_changed timestamp with time zone,
                                 last_modifier_id character varying(255),
                                 combined boolean,
                                 description character varying(2048),
                                 keyword character varying(128) NOT NULL,
                                 name character varying(128)
);


ALTER TABLE public.tf_actor OWNER TO gazelle;

--
-- Name: tf_actor_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_aud (
                                     id integer NOT NULL,
                                     rev integer NOT NULL,
                                     revtype smallint,
                                     combined boolean,
                                     description character varying(2048),
                                     keyword character varying(128),
                                     name character varying(128)
);


ALTER TABLE public.tf_actor_aud OWNER TO gazelle;

--
-- Name: tf_actor_default_ports; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_default_ports (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               config_name character varying(255),
                                               port_non_secure integer,
                                               port_secure integer,
                                               type_of_config character varying(255) NOT NULL,
                                               actor_id integer,
                                               CONSTRAINT tf_actor_default_ports_port_non_secure_check CHECK (((port_non_secure >= 0) AND (port_non_secure <= 65635))),
                                               CONSTRAINT tf_actor_default_ports_port_secure_check CHECK (((port_secure >= 0) AND (port_secure <= 65635)))
);


ALTER TABLE public.tf_actor_default_ports OWNER TO gazelle;

--
-- Name: tf_actor_default_ports_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_default_ports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_default_ports_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile (
                                                     id integer NOT NULL,
                                                     last_changed timestamp with time zone,
                                                     last_modifier_id character varying(255),
                                                     actor_id integer,
                                                     integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_aud (
                                                         id integer NOT NULL,
                                                         rev integer NOT NULL,
                                                         revtype smallint,
                                                         actor_id integer,
                                                         integration_profile_id integer
);


ALTER TABLE public.tf_actor_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option (
                                                            id integer NOT NULL,
                                                            last_changed timestamp with time zone,
                                                            last_modifier_id character varying(255),
                                                            maybe_supportive boolean,
                                                            actor_integration_profile_id integer NOT NULL,
                                                            document_section integer,
                                                            integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_actor_integration_profile_option_aud (
                                                                id integer NOT NULL,
                                                                rev integer NOT NULL,
                                                                revtype smallint,
                                                                maybe_supportive boolean,
                                                                actor_integration_profile_id integer,
                                                                document_section integer,
                                                                integration_profile_option_id integer
);


ALTER TABLE public.tf_actor_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_actor_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_actor_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_actor_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain (
                                           id integer NOT NULL,
                                           description character varying(255),
                                           keyword character varying(255),
                                           label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain OWNER TO gazelle;

--
-- Name: tf_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_affinity_domain_aud (
                                               id integer NOT NULL,
                                               rev integer NOT NULL,
                                               revtype smallint,
                                               description character varying(255),
                                               keyword character varying(255),
                                               label_to_display character varying(255)
);


ALTER TABLE public.tf_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_affinity_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_affinity_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_affinity_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_audit_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_audit_message (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         audited_event integer,
                                         comment character varying(255),
                                         event_code_type character varying(255),
                                         oid character varying(255),
                                         audited_transaction integer,
                                         document_section integer,
                                         issuing_actor integer
);


ALTER TABLE public.tf_audit_message OWNER TO gazelle;

--
-- Name: tf_audit_message_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_audit_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_audit_message_id_seq OWNER TO gazelle;

--
-- Name: tf_document; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document (
                                    id integer NOT NULL,
                                    last_changed timestamp with time zone,
                                    last_modifier_id character varying(255),
                                    document_dateofpublication timestamp without time zone,
                                    md5_hash_code character varying(255) NOT NULL,
                                    document_lifecyclestatus integer,
                                    document_linkstatus integer,
                                    document_linkstatusdescription character varying(255),
                                    document_name character varying(255) NOT NULL,
                                    document_revision character varying(255),
                                    document_title character varying(255),
                                    document_type integer NOT NULL,
                                    document_url character varying(255) NOT NULL,
                                    document_volume character varying(255),
                                    document_domain_id integer NOT NULL
);


ALTER TABLE public.tf_document OWNER TO gazelle;

--
-- Name: tf_document_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_aud (
                                        id integer NOT NULL,
                                        rev integer NOT NULL,
                                        revtype smallint,
                                        document_dateofpublication timestamp without time zone,
                                        md5_hash_code character varying(255),
                                        document_lifecyclestatus integer,
                                        document_linkstatus integer,
                                        document_linkstatusdescription character varying(255),
                                        document_name character varying(255),
                                        document_revision character varying(255),
                                        document_title character varying(255),
                                        document_type integer,
                                        document_url character varying(255),
                                        document_volume character varying(255),
                                        document_domain_id integer
);


ALTER TABLE public.tf_document_aud OWNER TO gazelle;

--
-- Name: tf_document_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_id_seq OWNER TO gazelle;

--
-- Name: tf_document_sections; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             section character varying(255),
                                             type integer,
                                             document_id integer
);


ALTER TABLE public.tf_document_sections OWNER TO gazelle;

--
-- Name: tf_document_sections_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_document_sections_aud (
                                                 id integer NOT NULL,
                                                 rev integer NOT NULL,
                                                 revtype smallint,
                                                 section character varying(255),
                                                 type integer,
                                                 document_id integer
);


ALTER TABLE public.tf_document_sections_aud OWNER TO gazelle;

--
-- Name: tf_document_sections_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_document_sections_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_document_sections_id_seq OWNER TO gazelle;

--
-- Name: tf_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain (
                                  id integer NOT NULL,
                                  last_changed timestamp with time zone,
                                  last_modifier_id character varying(255),
                                  description character varying(2048),
                                  keyword character varying(128) NOT NULL,
                                  name character varying(128) NOT NULL
);


ALTER TABLE public.tf_domain OWNER TO gazelle;

--
-- Name: tf_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_aud (
                                      id integer NOT NULL,
                                      rev integer NOT NULL,
                                      revtype smallint,
                                      description character varying(2048),
                                      keyword character varying(128),
                                      name character varying(128)
);


ALTER TABLE public.tf_domain_aud OWNER TO gazelle;

--
-- Name: tf_domain_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_domain_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_domain_id_seq OWNER TO gazelle;

--
-- Name: tf_domain_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile (
                                          domain_id integer NOT NULL,
                                          integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_domain_profile OWNER TO gazelle;

--
-- Name: tf_domain_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_domain_profile_aud (
                                              rev integer NOT NULL,
                                              domain_id integer NOT NULL,
                                              integration_profile_id integer NOT NULL,
                                              revtype smallint
);


ALTER TABLE public.tf_domain_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               hl7_version character varying(255) NOT NULL,
                                               message_order_control_code character varying(8),
                                               message_type character varying(15) NOT NULL,
                                               profile_content character varying(255),
                                               profile_oid character varying(64) NOT NULL,
                                               profile_type character varying(255),
                                               actor_id integer,
                                               domain_id integer,
                                               transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain (
                                                               hl7_message_profile_id integer NOT NULL,
                                                               affinity_domain_id integer NOT NULL
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_affinity_domain_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_affinity_domain_aud (
                                                                   rev integer NOT NULL,
                                                                   hl7_message_profile_id integer NOT NULL,
                                                                   affinity_domain_id integer NOT NULL,
                                                                   revtype smallint
);


ALTER TABLE public.tf_hl7_message_profile_affinity_domain_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_hl7_message_profile_aud (
                                                   id integer NOT NULL,
                                                   rev integer NOT NULL,
                                                   revtype smallint,
                                                   hl7_version character varying(255),
                                                   message_order_control_code character varying(8),
                                                   message_type character varying(15),
                                                   profile_content character varying(255),
                                                   profile_oid character varying(64),
                                                   profile_type character varying(255),
                                                   actor_id integer,
                                                   domain_id integer,
                                                   transaction_id integer
);


ALTER TABLE public.tf_hl7_message_profile_aud OWNER TO gazelle;

--
-- Name: tf_hl7_message_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_hl7_message_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_hl7_message_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               description character varying(2048),
                                               keyword character varying(32) NOT NULL,
                                               name character varying(128),
                                               documentsection integer,
                                               integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile OWNER TO gazelle;

--
-- Name: tf_integration_profile_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_aud (
                                                   id integer NOT NULL,
                                                   rev integer NOT NULL,
                                                   revtype smallint,
                                                   description character varying(2048),
                                                   keyword character varying(32),
                                                   name character varying(128),
                                                   documentsection integer,
                                                   integration_profile_status_type_id integer
);


ALTER TABLE public.tf_integration_profile_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      description character varying(2048),
                                                      keyword character varying(128) NOT NULL,
                                                      name character varying(128),
                                                      reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_option_aud (
                                                          id integer NOT NULL,
                                                          rev integer NOT NULL,
                                                          revtype smallint,
                                                          description character varying(2048),
                                                          keyword character varying(128),
                                                          name character varying(128),
                                                          reference character varying(255)
);


ALTER TABLE public.tf_integration_profile_option_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_option_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type (
                                                           id integer NOT NULL,
                                                           last_changed timestamp with time zone,
                                                           last_modifier_id character varying(255),
                                                           description character varying(2048),
                                                           keyword character varying(128) NOT NULL,
                                                           name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_status_type_aud (
                                                               id integer NOT NULL,
                                                               rev integer NOT NULL,
                                                               revtype smallint,
                                                               description character varying(2048),
                                                               keyword character varying(128),
                                                               name character varying(128)
);


ALTER TABLE public.tf_integration_profile_status_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type (
                                                    id integer NOT NULL,
                                                    last_changed timestamp with time zone,
                                                    last_modifier_id character varying(255),
                                                    description character varying(2048),
                                                    keyword character varying(128) NOT NULL,
                                                    name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_aud (
                                                        id integer NOT NULL,
                                                        rev integer NOT NULL,
                                                        revtype smallint,
                                                        description character varying(2048),
                                                        keyword character varying(128),
                                                        name character varying(128)
);


ALTER TABLE public.tf_integration_profile_type_aud OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_integration_profile_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_integration_profile_type_id_seq OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link (
                                                         integration_profile_type_id integer NOT NULL,
                                                         integration_profile_id integer NOT NULL
);


ALTER TABLE public.tf_integration_profile_type_link OWNER TO gazelle;

--
-- Name: tf_integration_profile_type_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_integration_profile_type_link_aud (
                                                             rev integer NOT NULL,
                                                             integration_profile_id integer NOT NULL,
                                                             integration_profile_type_id integer NOT NULL,
                                                             revtype smallint
);


ALTER TABLE public.tf_integration_profile_type_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        actor_integration_profile_id integer,
                                        transaction_id integer,
                                        transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link OWNER TO gazelle;

--
-- Name: tf_profile_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_profile_link_aud (
                                            id integer NOT NULL,
                                            rev integer NOT NULL,
                                            revtype smallint,
                                            actor_integration_profile_id integer,
                                            transaction_id integer,
                                            transaction_option_type_id integer
);


ALTER TABLE public.tf_profile_link_aud OWNER TO gazelle;

--
-- Name: tf_profile_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_profile_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_profile_link_id_seq OWNER TO gazelle;

--
-- Name: tf_rule; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule (
                                id integer NOT NULL,
                                comment character varying(1024),
                                cause_id integer,
                                consequence_id integer
);


ALTER TABLE public.tf_rule OWNER TO gazelle;

--
-- Name: tf_rule_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_criterion (
                                          dtype character varying(31) NOT NULL,
                                          id integer NOT NULL,
                                          list_or boolean,
                                          single_actor_id integer,
                                          aiporule_id integer,
                                          single_integration_profile_id integer,
                                          single_option_id integer
);


ALTER TABLE public.tf_rule_criterion OWNER TO gazelle;

--
-- Name: tf_rule_criterion_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_criterion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_criterion_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_rule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_rule_id_seq OWNER TO gazelle;

--
-- Name: tf_rule_list_criterion; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_rule_list_criterion (
                                               tf_rule_criterion_id integer NOT NULL,
                                               aipocriterions_id integer NOT NULL,
                                               tf_rule_list_criterion_id integer NOT NULL
);


ALTER TABLE public.tf_rule_list_criterion OWNER TO gazelle;

--
-- Name: tf_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard (
                                    id integer NOT NULL,
                                    last_changed timestamp with time zone,
                                    last_modifier_id character varying(255),
                                    keyword character varying(255),
                                    name text,
                                    network_communication_type integer,
                                    url text,
                                    version character varying(255)
);


ALTER TABLE public.tf_standard OWNER TO gazelle;

--
-- Name: tf_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_standard_aud (
                                        id integer NOT NULL,
                                        rev integer NOT NULL,
                                        revtype smallint,
                                        keyword character varying(255),
                                        name text,
                                        network_communication_type integer,
                                        url text,
                                        version character varying(255)
);


ALTER TABLE public.tf_standard_aud OWNER TO gazelle;

--
-- Name: tf_standard_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_standard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_standard_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction (
                                       id integer NOT NULL,
                                       last_changed timestamp with time zone,
                                       last_modifier_id character varying(255),
                                       description character varying(2048),
                                       keyword character varying(128) NOT NULL,
                                       name character varying(128),
                                       reference character varying(2048),
                                       documentsection integer,
                                       transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction OWNER TO gazelle;

--
-- Name: tf_transaction_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_aud (
                                           id integer NOT NULL,
                                           rev integer NOT NULL,
                                           revtype smallint,
                                           description character varying(2048),
                                           keyword character varying(128),
                                           name character varying(128),
                                           reference character varying(2048),
                                           documentsection integer,
                                           transaction_status_type_id integer
);


ALTER TABLE public.tf_transaction_aud OWNER TO gazelle;

--
-- Name: tf_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_link; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            from_actor_id integer,
                                            to_actor_id integer,
                                            transaction_id integer
);


ALTER TABLE public.tf_transaction_link OWNER TO gazelle;

--
-- Name: tf_transaction_link_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_link_aud (
                                                id integer NOT NULL,
                                                rev integer NOT NULL,
                                                revtype smallint,
                                                from_actor_id integer,
                                                to_actor_id integer,
                                                transaction_id integer
);


ALTER TABLE public.tf_transaction_link_aud OWNER TO gazelle;

--
-- Name: tf_transaction_link_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_link_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_option_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type (
                                                   id integer NOT NULL,
                                                   last_changed timestamp with time zone,
                                                   last_modifier_id character varying(255),
                                                   description character varying(2048),
                                                   keyword character varying(128) NOT NULL,
                                                   name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_option_type_aud (
                                                       id integer NOT NULL,
                                                       rev integer NOT NULL,
                                                       revtype smallint,
                                                       description character varying(2048),
                                                       keyword character varying(128),
                                                       name character varying(128)
);


ALTER TABLE public.tf_transaction_option_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_option_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_option_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_option_type_id_seq OWNER TO gazelle;

--
-- Name: tf_transaction_standard; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard (
                                                transaction_id integer NOT NULL,
                                                standard_id integer NOT NULL
);


ALTER TABLE public.tf_transaction_standard OWNER TO gazelle;

--
-- Name: tf_transaction_standard_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_standard_aud (
                                                    rev integer NOT NULL,
                                                    transaction_id integer NOT NULL,
                                                    standard_id integer NOT NULL,
                                                    revtype smallint
);


ALTER TABLE public.tf_transaction_standard_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type (
                                                   id integer NOT NULL,
                                                   last_changed timestamp with time zone,
                                                   last_modifier_id character varying(255),
                                                   description character varying(255),
                                                   keyword character varying(32) NOT NULL,
                                                   name character varying(64) NOT NULL
);


ALTER TABLE public.tf_transaction_status_type OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_transaction_status_type_aud (
                                                       id integer NOT NULL,
                                                       rev integer NOT NULL,
                                                       revtype smallint,
                                                       description character varying(255),
                                                       keyword character varying(32),
                                                       name character varying(64)
);


ALTER TABLE public.tf_transaction_status_type_aud OWNER TO gazelle;

--
-- Name: tf_transaction_status_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_transaction_status_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_transaction_status_type_id_seq OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage (
                                                id integer NOT NULL,
                                                last_changed timestamp with time zone,
                                                last_modifier_id character varying(255),
                                                usage character varying(255),
                                                transaction_id integer NOT NULL
);


ALTER TABLE public.tf_ws_transaction_usage OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tf_ws_transaction_usage_aud (
                                                    id integer NOT NULL,
                                                    rev integer NOT NULL,
                                                    revtype smallint,
                                                    usage character varying(255),
                                                    transaction_id integer
);


ALTER TABLE public.tf_ws_transaction_usage_aud OWNER TO gazelle;

--
-- Name: tf_ws_transaction_usage_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tf_ws_transaction_usage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tf_ws_transaction_usage_id_seq OWNER TO gazelle;

--
-- Name: tm_abstract_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_abstract_configuration (
                                                  id integer NOT NULL,
                                                  last_changed timestamp with time zone,
                                                  last_modifier_id character varying(255),
                                                  comments character varying(255),
                                                  configuration_id integer NOT NULL
);


ALTER TABLE public.tm_abstract_configuration OWNER TO gazelle;

--
-- Name: tm_annotation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_annotation (
                                      id integer NOT NULL,
                                      last_changed timestamp with time zone,
                                      last_modifier_id character varying(255),
                                      value character varying(500)
);


ALTER TABLE public.tm_annotation OWNER TO gazelle;

--
-- Name: tm_annotation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_annotation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_annotation_id_seq OWNER TO gazelle;

--
-- Name: tm_conf_mapping_w_aipo_w_conftypes; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_conf_mapping_w_aipo_w_conftypes (
                                                           tm_conf_mapped_id integer NOT NULL,
                                                           tm_conftypes_id integer NOT NULL
);


ALTER TABLE public.tm_conf_mapping_w_aipo_w_conftypes OWNER TO gazelle;

--
-- Name: tm_conf_mapping_w_aipo_w_conftypes_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_conf_mapping_w_aipo_w_conftypes_aud (
                                                               rev integer NOT NULL,
                                                               tm_conf_mapped_id integer NOT NULL,
                                                               tm_conftypes_id integer NOT NULL,
                                                               revtype smallint
);


ALTER TABLE public.tm_conf_mapping_w_aipo_w_conftypes_aud OWNER TO gazelle;

--
-- Name: tm_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         comment character varying(255),
                                         approved boolean NOT NULL,
                                         is_secured boolean,
                                         actor_id integer,
                                         configurationtype_id integer NOT NULL,
                                         host_id integer,
                                         system_in_session_id integer
);


ALTER TABLE public.tm_configuration OWNER TO gazelle;

--
-- Name: tm_configuration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_configuration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_configuration_id_seq OWNER TO gazelle;

--
-- Name: tm_configuration_mapped_with_aipo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration_mapped_with_aipo (
                                                          id integer NOT NULL,
                                                          last_changed timestamp with time zone,
                                                          last_modifier_id character varying(255),
                                                          aipo_id integer NOT NULL
);


ALTER TABLE public.tm_configuration_mapped_with_aipo OWNER TO gazelle;

--
-- Name: tm_configuration_mapped_with_aipo_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration_mapped_with_aipo_aud (
                                                              id integer NOT NULL,
                                                              rev integer NOT NULL,
                                                              revtype smallint,
                                                              aipo_id integer
);


ALTER TABLE public.tm_configuration_mapped_with_aipo_aud OWNER TO gazelle;

--
-- Name: tm_configuration_mapped_with_aipo_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_configuration_mapped_with_aipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_configuration_mapped_with_aipo_id_seq OWNER TO gazelle;

--
-- Name: tm_configuration_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration_type (
                                              id integer NOT NULL,
                                              last_changed timestamp with time zone,
                                              last_modifier_id character varying(255),
                                              category character varying(255),
                                              class_name character varying(255),
                                              type_name character varying(255),
                                              used_for_proxy boolean
);


ALTER TABLE public.tm_configuration_type OWNER TO gazelle;

--
-- Name: tm_configuration_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_configuration_type_aud (
                                                  id integer NOT NULL,
                                                  rev integer NOT NULL,
                                                  revtype smallint,
                                                  category character varying(255),
                                                  class_name character varying(255),
                                                  type_name character varying(255),
                                                  used_for_proxy boolean
);


ALTER TABLE public.tm_configuration_type_aud OWNER TO gazelle;

--
-- Name: tm_configuration_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_configuration_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_configuration_type_id_seq OWNER TO gazelle;

--
-- Name: tm_conftype_w_ports_wstype_and_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_conftype_w_ports_wstype_and_sop_class (
                                                                 id integer NOT NULL,
                                                                 comment character varying(255),
                                                                 port_non_secure integer,
                                                                 port_secure integer,
                                                                 transaction_description character varying(255),
                                                                 configurationtype_id integer NOT NULL,
                                                                 sop_class_id integer,
                                                                 transport_layer_id integer,
                                                                 web_service_type_id integer,
                                                                 ws_transaction_usage_id integer,
                                                                 CONSTRAINT tm_conftype_w_ports_wstype_and_sop_class_port_non_secure_check CHECK (((port_non_secure >= 80) AND (port_non_secure <= 65535))),
                                                                 CONSTRAINT tm_conftype_w_ports_wstype_and_sop_class_port_secure_check CHECK (((port_secure <= 65535) AND (port_secure >= 1024)))
);


ALTER TABLE public.tm_conftype_w_ports_wstype_and_sop_class OWNER TO gazelle;

--
-- Name: tm_conftype_w_ports_wstype_and_sop_class_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_conftype_w_ports_wstype_and_sop_class_aud (
                                                                     id integer NOT NULL,
                                                                     rev integer NOT NULL,
                                                                     revtype smallint,
                                                                     comment character varying(255),
                                                                     port_non_secure integer,
                                                                     port_secure integer,
                                                                     transaction_description character varying(255),
                                                                     configurationtype_id integer,
                                                                     sop_class_id integer,
                                                                     transport_layer_id integer,
                                                                     web_service_type_id integer,
                                                                     ws_transaction_usage_id integer
);


ALTER TABLE public.tm_conftype_w_ports_wstype_and_sop_class_aud OWNER TO gazelle;

--
-- Name: tm_conftype_w_ports_wstype_and_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_conftype_w_ports_wstype_and_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_conftype_w_ports_wstype_and_sop_class_id_seq OWNER TO gazelle;

--
-- Name: tm_connectathon_participant; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_connectathon_participant (
                                                    id integer NOT NULL,
                                                    last_changed timestamp with time zone,
                                                    last_modifier_id character varying(255),
                                                    email character varying(255) NOT NULL,
                                                    firstname character varying(128) NOT NULL,
                                                    friday_meal boolean,
                                                    institution_name character varying(255),
                                                    institution_id bytea,
                                                    lastname character varying(128) NOT NULL,
                                                    monday_meal boolean,
                                                    social_event boolean,
                                                    thursday_meal boolean,
                                                    tuesday_meal boolean,
                                                    vegetarian_meal boolean,
                                                    wednesday_meal boolean,
                                                    status_id integer NOT NULL,
                                                    institution_ok_id integer,
                                                    testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_connectathon_participant OWNER TO gazelle;

--
-- Name: tm_connectathon_participant_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_connectathon_participant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_connectathon_participant_id_seq OWNER TO gazelle;

--
-- Name: tm_connectathon_participant_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_connectathon_participant_status (
                                                           id integer NOT NULL,
                                                           description character varying(255),
                                                           name character varying(255) NOT NULL
);


ALTER TABLE public.tm_connectathon_participant_status OWNER TO gazelle;

--
-- Name: tm_connectathon_participant_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_connectathon_participant_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_connectathon_participant_status_id_seq OWNER TO gazelle;

--
-- Name: tm_contextual_information; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_contextual_information (
                                                  id integer NOT NULL,
                                                  last_changed timestamp with time zone,
                                                  last_modifier_id character varying(255),
                                                  label character varying(255) NOT NULL,
                                                  value character varying(255),
                                                  path_id integer NOT NULL
);


ALTER TABLE public.tm_contextual_information OWNER TO gazelle;

--
-- Name: tm_contextual_information_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_contextual_information_aud (
                                                      id integer NOT NULL,
                                                      rev integer NOT NULL,
                                                      revtype smallint,
                                                      label character varying(255),
                                                      value character varying(255),
                                                      path_id integer
);


ALTER TABLE public.tm_contextual_information_aud OWNER TO gazelle;

--
-- Name: tm_contextual_information_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_contextual_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_contextual_information_id_seq OWNER TO gazelle;

--
-- Name: tm_contextual_information_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_contextual_information_instance (
                                                           id integer NOT NULL,
                                                           last_changed timestamp with time zone,
                                                           last_modifier_id character varying(255),
                                                           value character varying(255),
                                                           contextual_information integer NOT NULL
);


ALTER TABLE public.tm_contextual_information_instance OWNER TO gazelle;

--
-- Name: tm_contextual_information_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_contextual_information_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_contextual_information_instance_id_seq OWNER TO gazelle;

--
-- Name: tm_data_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_data_type (
                                     id integer NOT NULL,
                                     last_changed timestamp with time zone,
                                     last_modifier_id character varying(255),
                                     description character varying(255),
                                     keyword character varying(255) NOT NULL
);


ALTER TABLE public.tm_data_type OWNER TO gazelle;

--
-- Name: tm_data_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_data_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_data_type_id_seq OWNER TO gazelle;

--
-- Name: tm_demonstration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_demonstration (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         is_active boolean,
                                         beginning_date timestamp without time zone,
                                         description character varying(1024),
                                         ending_date timestamp without time zone,
                                         name character varying(255) NOT NULL,
                                         url character varying(255),
                                         country character varying(2)
);


ALTER TABLE public.tm_demonstration OWNER TO gazelle;

--
-- Name: tm_demonstration_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_demonstration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_demonstration_id_seq OWNER TO gazelle;

--
-- Name: tm_demonstration_system_in_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_demonstration_system_in_session (
                                                           demonstration_id integer NOT NULL,
                                                           system_in_session_id integer NOT NULL
);


ALTER TABLE public.tm_demonstration_system_in_session OWNER TO gazelle;

--
-- Name: tm_demonstrations_in_testing_sessions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_demonstrations_in_testing_sessions (
                                                              demonstration_id integer NOT NULL,
                                                              testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_demonstrations_in_testing_sessions OWNER TO gazelle;

--
-- Name: tm_dicom_scp_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_dicom_scp_configuration (
                                                   id integer NOT NULL,
                                                   last_changed timestamp with time zone,
                                                   last_modifier_id character varying(255),
                                                   comments character varying(255),
                                                   configuration_id integer NOT NULL,
                                                   ae_title character varying(16) NOT NULL,
                                                   modality_type character varying(255),
                                                   port integer,
                                                   port_proxy integer,
                                                   port_secure integer,
                                                   transfer_role character varying(255) NOT NULL,
                                                   sop_class_id integer NOT NULL,
                                                   CONSTRAINT tm_dicom_scp_configuration_port_check CHECK (((port >= 0) AND (port <= 65635))),
                                                   CONSTRAINT tm_dicom_scp_configuration_port_proxy_check CHECK (((port_proxy <= 65635) AND (port_proxy >= 0))),
                                                   CONSTRAINT tm_dicom_scp_configuration_port_secure_check CHECK (((port_secure >= 0) AND (port_secure <= 65635)))
);


ALTER TABLE public.tm_dicom_scp_configuration OWNER TO gazelle;

--
-- Name: tm_dicom_scu_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_dicom_scu_configuration (
                                                   id integer NOT NULL,
                                                   last_changed timestamp with time zone,
                                                   last_modifier_id character varying(255),
                                                   comments character varying(255),
                                                   configuration_id integer NOT NULL,
                                                   ae_title character varying(16) NOT NULL,
                                                   modality_type character varying(255),
                                                   port integer,
                                                   port_proxy integer,
                                                   port_secure integer,
                                                   transfer_role character varying(255) NOT NULL,
                                                   sop_class_id integer NOT NULL,
                                                   CONSTRAINT tm_dicom_scu_configuration_port_check CHECK (((port >= 0) AND (port <= 65635))),
                                                   CONSTRAINT tm_dicom_scu_configuration_port_proxy_check CHECK (((port_proxy <= 65635) AND (port_proxy >= 0))),
                                                   CONSTRAINT tm_dicom_scu_configuration_port_secure_check CHECK (((port_secure >= 0) AND (port_secure <= 65635)))
);


ALTER TABLE public.tm_dicom_scu_configuration OWNER TO gazelle;

--
-- Name: tm_domains_in_testing_sessions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_domains_in_testing_sessions (
                                                       testing_session_id integer NOT NULL,
                                                       domain_id integer NOT NULL
);


ALTER TABLE public.tm_domains_in_testing_sessions OWNER TO gazelle;

--
-- Name: tm_generic_oids_institution_for_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_generic_oids_institution_for_session (
                                                                id integer NOT NULL,
                                                                last_changed timestamp with time zone,
                                                                last_modifier_id character varying(255),
                                                                current_oid_value integer NOT NULL,
                                                                oid_for_session character varying(255) NOT NULL,
                                                                oid_root_value character varying(255) NOT NULL,
                                                                testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_generic_oids_institution_for_session OWNER TO gazelle;

--
-- Name: tm_generic_oids_institution_for_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_generic_oids_institution_for_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_generic_oids_institution_for_session_id_seq OWNER TO gazelle;

--
-- Name: tm_generic_oids_ip_config_param_for_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_generic_oids_ip_config_param_for_session (
                                                                    id integer NOT NULL,
                                                                    last_changed timestamp with time zone,
                                                                    last_modifier_id character varying(255),
                                                                    current_oid_value integer NOT NULL,
                                                                    oid_for_session character varying(255),
                                                                    oid_root_value character varying(255) NOT NULL,
                                                                    profile_for_id integer NOT NULL,
                                                                    testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_generic_oids_ip_config_param_for_session OWNER TO gazelle;

--
-- Name: tm_generic_oids_ip_config_param_for_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_generic_oids_ip_config_param_for_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_generic_oids_ip_config_param_for_session_id_seq OWNER TO gazelle;

--
-- Name: tm_hl7_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_hl7_initiator_configuration (
                                                       id integer NOT NULL,
                                                       last_changed timestamp with time zone,
                                                       last_modifier_id character varying(255),
                                                       comments character varying(255),
                                                       configuration_id integer NOT NULL,
                                                       assigning_authority character varying(255),
                                                       ass_auth_oid character varying(255),
                                                       sending_receiving_application character varying(255) NOT NULL,
                                                       sending_receiving_facility character varying(255) NOT NULL
);


ALTER TABLE public.tm_hl7_initiator_configuration OWNER TO gazelle;

--
-- Name: tm_hl7_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_hl7_responder_configuration (
                                                       id integer NOT NULL,
                                                       last_changed timestamp with time zone,
                                                       last_modifier_id character varying(255),
                                                       comments character varying(255),
                                                       configuration_id integer NOT NULL,
                                                       assigning_authority character varying(255),
                                                       ass_auth_oid character varying(255),
                                                       sending_receiving_application character varying(255) NOT NULL,
                                                       sending_receiving_facility character varying(255) NOT NULL,
                                                       port integer,
                                                       port_out integer,
                                                       port_proxy integer,
                                                       port_secure integer,
                                                       CONSTRAINT tm_hl7_responder_configuration_port_check CHECK (((port >= 0) AND (port <= 65635))),
                                                       CONSTRAINT tm_hl7_responder_configuration_port_out_check CHECK (((port_out <= 65635) AND (port_out >= 0))),
                                                       CONSTRAINT tm_hl7_responder_configuration_port_proxy_check CHECK (((port_proxy >= 1024) AND (port_proxy <= 65635))),
                                                       CONSTRAINT tm_hl7_responder_configuration_port_secure_check CHECK (((port_secure <= 65635) AND (port_secure >= 0)))
);


ALTER TABLE public.tm_hl7_responder_configuration OWNER TO gazelle;

--
-- Name: tm_hl7_v3_initiator_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_hl7_v3_initiator_configuration (
                                                          id integer NOT NULL,
                                                          last_changed timestamp with time zone,
                                                          last_modifier_id character varying(255),
                                                          comments character varying(255),
                                                          configuration_id integer NOT NULL,
                                                          assigning_authority character varying(255),
                                                          ass_auth_oid character varying(255),
                                                          sending_receiving_application character varying(255) NOT NULL,
                                                          sending_receiving_facility character varying(255) NOT NULL
);


ALTER TABLE public.tm_hl7_v3_initiator_configuration OWNER TO gazelle;

--
-- Name: tm_hl7_v3_responder_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_hl7_v3_responder_configuration (
                                                          id integer NOT NULL,
                                                          last_changed timestamp with time zone,
                                                          last_modifier_id character varying(255),
                                                          comments character varying(255),
                                                          configuration_id integer NOT NULL,
                                                          assigning_authority character varying(255),
                                                          ass_auth_oid character varying(255),
                                                          sending_receiving_application character varying(255) NOT NULL,
                                                          sending_receiving_facility character varying(255) NOT NULL,
                                                          port integer,
                                                          port_proxy integer,
                                                          port_secured integer,
                                                          url character varying(255) NOT NULL,
                                                          usage character varying(255),
                                                          ws_transaction_usage integer,
                                                          CONSTRAINT tm_hl7_v3_responder_configuration_port_check CHECK (((port <= 65635) AND (port >= 0))),
                                                          CONSTRAINT tm_hl7_v3_responder_configuration_port_proxy_check CHECK (((port_proxy <= 65635) AND (port_proxy >= 1024))),
                                                          CONSTRAINT tm_hl7_v3_responder_configuration_port_secured_check CHECK (((port_secured >= 0) AND (port_secured <= 65635)))
);


ALTER TABLE public.tm_hl7_v3_responder_configuration OWNER TO gazelle;

--
-- Name: tm_home; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_home (
                                id integer NOT NULL,
                                language character varying(255),
                                main_panel_content text,
                                main_panel_header text,
                                secondary_panel_content text,
                                secondary_panel_header text,
                                secondary_panel_position character varying(255)
);


ALTER TABLE public.tm_home OWNER TO gazelle;

--
-- Name: tm_home_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_home_id_seq OWNER TO gazelle;

--
-- Name: tm_host; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_host (
                                id integer NOT NULL,
                                last_changed timestamp with time zone,
                                last_modifier_id character varying(255),
                                alias character varying(255),
                                comment character varying(255),
                                hostname character varying(255) NOT NULL,
                                ignore_domain_name boolean,
                                ip character varying(255),
                                used_by_more_one_system_in_session boolean,
                                institution_id integer,
                                testing_session_id integer
);


ALTER TABLE public.tm_host OWNER TO gazelle;

--
-- Name: tm_host_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_host_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_host_id_seq OWNER TO gazelle;

--
-- Name: tm_institution_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_institution_system (
                                              id integer NOT NULL,
                                              last_changed timestamp with time zone,
                                              last_modifier_id character varying(255),
                                              comment character varying(255),
                                              institution_id integer,
                                              system_id integer
);


ALTER TABLE public.tm_institution_system OWNER TO gazelle;

--
-- Name: tm_institution_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_institution_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_institution_system_id_seq OWNER TO gazelle;

--
-- Name: tm_invoice; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_invoice (
                                   id integer NOT NULL,
                                   last_changed timestamp with time zone,
                                   last_modifier_id character varying(255),
                                   admin_invoice_notes text,
                                   contract_out_of_date boolean,
                                   contract_received boolean,
                                   contract_received_date date,
                                   fees_amount numeric(19,2),
                                   fees_discount numeric(19,2),
                                   fees_due numeric(19,2),
                                   fees_paid numeric(19,2),
                                   invoice_generation_date timestamp without time zone,
                                   invoice_number integer,
                                   invoice_paid boolean,
                                   invoice_payment_date timestamp without time zone,
                                   invoice_path character varying(255),
                                   invoice_sent boolean,
                                   invoice_sent_date date,
                                   last_contract_generation bigint,
                                   note character varying(255),
                                   number_extra_participant integer,
                                   number_participant integer,
                                   number_system integer,
                                   purchase_order character varying(255),
                                   reasons_for_discount character varying(255),
                                   special_instructions text,
                                   vat_amount numeric(19,2),
                                   vat_due boolean,
                                   vat_number character varying(255),
                                   vat_validity boolean,
                                   currency character varying(12),
                                   institution_id integer NOT NULL,
                                   testing_session_id integer NOT NULL,
                                   vat_country character varying(2)
);


ALTER TABLE public.tm_invoice OWNER TO gazelle;

--
-- Name: tm_invoice_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_invoice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_invoice_id_seq OWNER TO gazelle;

--
-- Name: tm_jira_issues_to_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_jira_issues_to_test (
                                               id integer NOT NULL,
                                               jira_key character varying(255),
                                               jira_status character varying(255),
                                               jira_summary character varying(255),
                                               test_id integer
);


ALTER TABLE public.tm_jira_issues_to_test OWNER TO gazelle;

--
-- Name: tm_jira_issues_to_test_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_jira_issues_to_test_aud (
                                                   id integer NOT NULL,
                                                   rev integer NOT NULL,
                                                   revtype smallint,
                                                   jira_key character varying(255),
                                                   jira_status character varying(255),
                                                   jira_summary character varying(255),
                                                   test_id integer
);


ALTER TABLE public.tm_jira_issues_to_test_aud OWNER TO gazelle;

--
-- Name: tm_jira_issues_to_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_jira_issues_to_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_jira_issues_to_test_id_seq OWNER TO gazelle;

--
-- Name: tm_meta_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_meta_test (
                                     id integer NOT NULL,
                                     last_changed timestamp with time zone,
                                     last_modifier_id character varying(255),
                                     short_description character varying(512) NOT NULL,
                                     keyword character varying(64) NOT NULL
);


ALTER TABLE public.tm_meta_test OWNER TO gazelle;

--
-- Name: tm_meta_test_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_meta_test_aud (
                                         id integer NOT NULL,
                                         rev integer NOT NULL,
                                         revtype smallint,
                                         short_description character varying(512),
                                         keyword character varying(64)
);


ALTER TABLE public.tm_meta_test_aud OWNER TO gazelle;

--
-- Name: tm_meta_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_meta_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_meta_test_id_seq OWNER TO gazelle;

--
-- Name: tm_meta_test_test_roles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_meta_test_test_roles (
                                                meta_test_id integer,
                                                test_roles_id integer NOT NULL
);


ALTER TABLE public.tm_meta_test_test_roles OWNER TO gazelle;

--
-- Name: tm_meta_test_test_roles_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_meta_test_test_roles_aud (
                                                    rev integer NOT NULL,
                                                    meta_test_id integer NOT NULL,
                                                    test_roles_id integer NOT NULL,
                                                    revtype smallint
);


ALTER TABLE public.tm_meta_test_test_roles_aud OWNER TO gazelle;

--
-- Name: tm_monitor_in_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_monitor_in_session (
                                              id integer NOT NULL,
                                              last_changed timestamp with time zone,
                                              last_modifier_id character varying(255),
                                              is_activated boolean,
                                              testing_session_id integer,
                                              user_id integer
);


ALTER TABLE public.tm_monitor_in_session OWNER TO gazelle;

--
-- Name: tm_monitor_in_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_monitor_in_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_monitor_in_session_id_seq OWNER TO gazelle;

--
-- Name: tm_monitor_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_monitor_test (
                                        monitor_id integer NOT NULL,
                                        test_id integer NOT NULL
);


ALTER TABLE public.tm_monitor_test OWNER TO gazelle;

--
-- Name: tm_network_config_for_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_network_config_for_session (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      dns_header character varying(20000),
                                                      domain_name character varying(255),
                                                      host_header character varying(20000),
                                                      dns_address character varying(255) NOT NULL,
                                                      ip_range_high character varying(255) NOT NULL,
                                                      ip_range_low character varying(255) NOT NULL,
                                                      reverse_header character varying(20000),
                                                      subnet_mask character varying(255) NOT NULL,
                                                      testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_network_config_for_session OWNER TO gazelle;

--
-- Name: tm_network_config_for_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_network_config_for_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_network_config_for_session_id_seq OWNER TO gazelle;

--
-- Name: tm_object_attribute; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_attribute (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            description character varying(255),
                                            keyword character varying(255) NOT NULL,
                                            object_id integer NOT NULL
);


ALTER TABLE public.tm_object_attribute OWNER TO gazelle;

--
-- Name: tm_object_attribute_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_attribute_aud (
                                                id integer NOT NULL,
                                                rev integer NOT NULL,
                                                revtype smallint,
                                                description character varying(255),
                                                keyword character varying(255),
                                                object_id integer
);


ALTER TABLE public.tm_object_attribute_aud OWNER TO gazelle;

--
-- Name: tm_object_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_attribute_id_seq OWNER TO gazelle;

--
-- Name: tm_object_creator; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_creator (
                                          id integer NOT NULL,
                                          last_changed timestamp with time zone,
                                          last_modifier_id character varying(255),
                                          description character varying(255),
                                          aipo_id integer NOT NULL,
                                          object_id integer NOT NULL
);


ALTER TABLE public.tm_object_creator OWNER TO gazelle;

--
-- Name: tm_object_creator_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_creator_aud (
                                              id integer NOT NULL,
                                              rev integer NOT NULL,
                                              revtype smallint,
                                              description character varying(255),
                                              aipo_id integer,
                                              object_id integer
);


ALTER TABLE public.tm_object_creator_aud OWNER TO gazelle;

--
-- Name: tm_object_creator_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_creator_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_creator_id_seq OWNER TO gazelle;

--
-- Name: tm_object_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_file (
                                       id integer NOT NULL,
                                       last_changed timestamp with time zone,
                                       last_modifier_id character varying(255),
                                       description character varying(255),
                                       max integer,
                                       min integer,
                                       uploader character varying(255),
                                       object_id integer NOT NULL,
                                       type_id integer NOT NULL
);


ALTER TABLE public.tm_object_file OWNER TO gazelle;

--
-- Name: tm_object_file_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_file_aud (
                                           id integer NOT NULL,
                                           rev integer NOT NULL,
                                           revtype smallint,
                                           description character varying(255),
                                           max integer,
                                           min integer,
                                           uploader character varying(255),
                                           object_id integer,
                                           type_id integer
);


ALTER TABLE public.tm_object_file_aud OWNER TO gazelle;

--
-- Name: tm_object_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_file_id_seq OWNER TO gazelle;

--
-- Name: tm_object_file_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_file_type (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            description character varying(255),
                                            extensions character varying(255),
                                            keyword character varying(255) NOT NULL,
                                            validate boolean,
                                            writable boolean
);


ALTER TABLE public.tm_object_file_type OWNER TO gazelle;

--
-- Name: tm_object_file_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_file_type_aud (
                                                id integer NOT NULL,
                                                rev integer NOT NULL,
                                                revtype smallint,
                                                description character varying(255),
                                                extensions character varying(255),
                                                keyword character varying(255),
                                                validate boolean,
                                                writable boolean
);


ALTER TABLE public.tm_object_file_type_aud OWNER TO gazelle;

--
-- Name: tm_object_file_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_file_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_file_type_id_seq OWNER TO gazelle;

--
-- Name: tm_object_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance (
                                           id integer NOT NULL,
                                           last_changed timestamp with time zone,
                                           last_modifier_id character varying(255),
                                           completed boolean,
                                           description text,
                                           name character varying(250),
                                           object_id integer NOT NULL,
                                           test_type_id integer,
                                           system_in_session_id integer NOT NULL,
                                           validation_id integer
);


ALTER TABLE public.tm_object_instance OWNER TO gazelle;

--
-- Name: tm_object_instance_annotation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance_annotation (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      annotation_id integer NOT NULL,
                                                      object_instance_id integer NOT NULL
);


ALTER TABLE public.tm_object_instance_annotation OWNER TO gazelle;

--
-- Name: tm_object_instance_annotation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_instance_annotation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_instance_annotation_id_seq OWNER TO gazelle;

--
-- Name: tm_object_instance_attribute; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance_attribute (
                                                     id integer NOT NULL,
                                                     last_changed timestamp with time zone,
                                                     last_modifier_id character varying(255),
                                                     description character varying(255),
                                                     attribute_id integer NOT NULL,
                                                     instance_id integer NOT NULL
);


ALTER TABLE public.tm_object_instance_attribute OWNER TO gazelle;

--
-- Name: tm_object_instance_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_instance_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_instance_attribute_id_seq OWNER TO gazelle;

--
-- Name: tm_object_instance_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance_file (
                                                id integer NOT NULL,
                                                last_changed timestamp with time zone,
                                                last_modifier_id character varying(255),
                                                result_oid bytea,
                                                url character varying(255),
                                                file_id integer NOT NULL,
                                                instance_id integer NOT NULL,
                                                system_in_session_id integer
);


ALTER TABLE public.tm_object_instance_file OWNER TO gazelle;

--
-- Name: tm_object_instance_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_instance_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_instance_file_id_seq OWNER TO gazelle;

--
-- Name: tm_object_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_instance_id_seq OWNER TO gazelle;

--
-- Name: tm_object_instance_validation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance_validation (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      description character varying(255),
                                                      value character varying(255) NOT NULL
);


ALTER TABLE public.tm_object_instance_validation OWNER TO gazelle;

--
-- Name: tm_object_instance_validation_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_instance_validation_aud (
                                                          id integer NOT NULL,
                                                          rev integer NOT NULL,
                                                          revtype smallint,
                                                          description character varying(255),
                                                          value character varying(255)
);


ALTER TABLE public.tm_object_instance_validation_aud OWNER TO gazelle;

--
-- Name: tm_object_instance_validation_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_instance_validation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_instance_validation_id_seq OWNER TO gazelle;

--
-- Name: tm_object_reader; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_reader (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         description character varying(255),
                                         aipo_id integer NOT NULL,
                                         object_id integer NOT NULL
);


ALTER TABLE public.tm_object_reader OWNER TO gazelle;

--
-- Name: tm_object_reader_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_reader_aud (
                                             id integer NOT NULL,
                                             rev integer NOT NULL,
                                             revtype smallint,
                                             description character varying(255),
                                             aipo_id integer,
                                             object_id integer
);


ALTER TABLE public.tm_object_reader_aud OWNER TO gazelle;

--
-- Name: tm_object_reader_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_reader_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_reader_id_seq OWNER TO gazelle;

--
-- Name: tm_object_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_type (
                                       id integer NOT NULL,
                                       last_changed timestamp with time zone,
                                       last_modifier_id character varying(255),
                                       default_desc character varying(500),
                                       description character varying(250),
                                       instructions text,
                                       keyword character varying(250) NOT NULL,
                                       object_type_status_id integer
);


ALTER TABLE public.tm_object_type OWNER TO gazelle;

--
-- Name: tm_object_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_type_aud (
                                           id integer NOT NULL,
                                           rev integer NOT NULL,
                                           revtype smallint,
                                           default_desc character varying(500),
                                           description character varying(250),
                                           instructions text,
                                           keyword character varying(250),
                                           object_type_status_id integer
);


ALTER TABLE public.tm_object_type_aud OWNER TO gazelle;

--
-- Name: tm_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_type_id_seq OWNER TO gazelle;

--
-- Name: tm_object_type_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_type_status (
                                              id integer NOT NULL,
                                              last_changed timestamp with time zone,
                                              last_modifier_id character varying(255),
                                              description character varying(255),
                                              keyword character varying(255),
                                              label_to_display character varying(255)
);


ALTER TABLE public.tm_object_type_status OWNER TO gazelle;

--
-- Name: tm_object_type_status_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_object_type_status_aud (
                                                  id integer NOT NULL,
                                                  rev integer NOT NULL,
                                                  revtype smallint
);


ALTER TABLE public.tm_object_type_status_aud OWNER TO gazelle;

--
-- Name: tm_object_type_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_object_type_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_object_type_status_id_seq OWNER TO gazelle;

--
-- Name: tm_oid_institution; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_institution (
                                           id integer NOT NULL,
                                           oid character varying(255),
                                           institution_id integer,
                                           testing_session_id integer
);


ALTER TABLE public.tm_oid_institution OWNER TO gazelle;

--
-- Name: tm_oid_institution_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_institution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_institution_id_seq OWNER TO gazelle;

--
-- Name: tm_oid_requirement; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_requirement (
                                           id integer NOT NULL,
                                           last_changed timestamp with time zone,
                                           last_modifier_id character varying(255),
                                           label character varying(255),
                                           prefix character varying(255),
                                           oid_root_definition_id integer
);


ALTER TABLE public.tm_oid_requirement OWNER TO gazelle;

--
-- Name: tm_oid_requirement_aipo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_requirement_aipo (
                                                oid_requirement_id integer NOT NULL,
                                                aipo_id integer NOT NULL
);


ALTER TABLE public.tm_oid_requirement_aipo OWNER TO gazelle;

--
-- Name: tm_oid_requirement_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_requirement_id_seq OWNER TO gazelle;

--
-- Name: tm_oid_root_definition; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_root_definition (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               last_value integer,
                                               root_oid character varying(255),
                                               label_id integer
);


ALTER TABLE public.tm_oid_root_definition OWNER TO gazelle;

--
-- Name: tm_oid_root_definition_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_root_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_root_definition_id_seq OWNER TO gazelle;

--
-- Name: tm_oid_root_definition_label; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_root_definition_label (
                                                     id integer NOT NULL,
                                                     last_changed timestamp with time zone,
                                                     last_modifier_id character varying(255),
                                                     label character varying(255) NOT NULL
);


ALTER TABLE public.tm_oid_root_definition_label OWNER TO gazelle;

--
-- Name: tm_oid_root_definition_label_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_root_definition_label_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_root_definition_label_id_seq OWNER TO gazelle;

--
-- Name: tm_oid_system_assignment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oid_system_assignment (
                                                 id integer NOT NULL,
                                                 last_changed timestamp with time zone,
                                                 last_modifier_id character varying(255),
                                                 comment character varying(255),
                                                 oid_value character varying(255),
                                                 oid_requirement_id integer,
                                                 system_in_session_id integer
);


ALTER TABLE public.tm_oid_system_assignment OWNER TO gazelle;

--
-- Name: tm_oid_system_assignment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oid_system_assignment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oid_system_assignment_id_seq OWNER TO gazelle;

--
-- Name: tm_oids_ip_config_param_for_session_for_hl7; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_oids_ip_config_param_for_session_for_hl7 (
                                                                    id integer NOT NULL,
                                                                    last_changed timestamp with time zone,
                                                                    last_modifier_id character varying(255),
                                                                    current_id_for_oid_hl7_v2 integer NOT NULL,
                                                                    current_id_for_oid_hl7_v3 integer NOT NULL,
                                                                    oid_for_hl7_v2 character varying(255),
                                                                    oid_for_hl7_v3 character varying(255),
                                                                    oid_for_session character varying(255),
                                                                    testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_oids_ip_config_param_for_session_for_hl7 OWNER TO gazelle;

--
-- Name: tm_oids_ip_config_param_for_session_for_hl7_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_oids_ip_config_param_for_session_for_hl7_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_oids_ip_config_param_for_session_for_hl7_id_seq OWNER TO gazelle;

--
-- Name: tm_path; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path (
                                id integer NOT NULL,
                                last_changed timestamp with time zone,
                                last_modifier_id character varying(255),
                                description character varying(255) NOT NULL,
                                keyword character varying(255) NOT NULL,
                                type character varying(255)
);


ALTER TABLE public.tm_path OWNER TO gazelle;

--
-- Name: tm_path_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_path_aud (
                                    id integer NOT NULL,
                                    rev integer NOT NULL,
                                    revtype smallint,
                                    description character varying(255),
                                    keyword character varying(255),
                                    type character varying(255)
);


ALTER TABLE public.tm_path_aud OWNER TO gazelle;

--
-- Name: tm_path_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_path_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_path_id_seq OWNER TO gazelle;

--
-- Name: tm_profiles_in_testing_sessions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_profiles_in_testing_sessions (
                                                        testing_session_id integer NOT NULL,
                                                        integration_profile_id integer NOT NULL
);


ALTER TABLE public.tm_profiles_in_testing_sessions OWNER TO gazelle;

--
-- Name: tm_proxy_configuration_for_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_proxy_configuration_for_session (
                                                           id integer NOT NULL,
                                                           last_changed timestamp with time zone,
                                                           last_modifier_id character varying(255),
                                                           ip_for_class character varying(255) NOT NULL,
                                                           port_range_high integer,
                                                           port_range_low integer,
                                                           configurationtype_id integer,
                                                           testing_session_id integer NOT NULL,
                                                           CONSTRAINT tm_proxy_configuration_for_session_port_range_high_check CHECK (((port_range_high <= 65634) AND (port_range_high >= 1024))),
                                                           CONSTRAINT tm_proxy_configuration_for_session_port_range_low_check CHECK (((port_range_low <= 65634) AND (port_range_low >= 1024)))
);


ALTER TABLE public.tm_proxy_configuration_for_session OWNER TO gazelle;

--
-- Name: tm_proxy_configuration_for_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_proxy_configuration_for_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_proxy_configuration_for_session_id_seq OWNER TO gazelle;

--
-- Name: tm_raw_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_raw_configuration (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             comments character varying(255),
                                             configuration_id integer NOT NULL,
                                             port integer,
                                             port_proxy integer,
                                             port_secured integer,
                                             transaction_description character varying(255),
                                             CONSTRAINT tm_raw_configuration_port_check CHECK (((port >= 0) AND (port <= 65635))),
                                             CONSTRAINT tm_raw_configuration_port_proxy_check CHECK (((port_proxy >= 0) AND (port_proxy <= 65635))),
                                             CONSTRAINT tm_raw_configuration_port_secured_check CHECK (((port_secured <= 65635) AND (port_secured >= 0)))
);


ALTER TABLE public.tm_raw_configuration OWNER TO gazelle;

--
-- Name: tm_role_in_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_role_in_test (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        is_role_played_by_a_tool boolean,
                                        keyword character varying(255)
);


ALTER TABLE public.tm_role_in_test OWNER TO gazelle;

--
-- Name: tm_role_in_test_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_role_in_test_aud (
                                            id integer NOT NULL,
                                            rev integer NOT NULL,
                                            revtype smallint,
                                            is_role_played_by_a_tool boolean,
                                            keyword character varying(255)
);


ALTER TABLE public.tm_role_in_test_aud OWNER TO gazelle;

--
-- Name: tm_role_in_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_role_in_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_role_in_test_id_seq OWNER TO gazelle;

--
-- Name: tm_role_in_test_test_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_role_in_test_test_participants (
                                                          role_in_test_id integer NOT NULL,
                                                          test_participants_id integer NOT NULL
);


ALTER TABLE public.tm_role_in_test_test_participants OWNER TO gazelle;

--
-- Name: tm_role_in_test_test_participants_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_role_in_test_test_participants_aud (
                                                              rev integer NOT NULL,
                                                              role_in_test_id integer NOT NULL,
                                                              test_participants_id integer NOT NULL,
                                                              revtype smallint
);


ALTER TABLE public.tm_role_in_test_test_participants_aud OWNER TO gazelle;

--
-- Name: tm_sap_result_tr_comment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_sap_result_tr_comment (
                                                 id integer NOT NULL,
                                                 last_changed timestamp with time zone,
                                                 last_modifier_id character varying(255),
                                                 comment character varying(255),
                                                 sap_id integer NOT NULL,
                                                 tr_id integer NOT NULL
);


ALTER TABLE public.tm_sap_result_tr_comment OWNER TO gazelle;

--
-- Name: tm_sap_result_tr_comment_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_sap_result_tr_comment_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_sap_result_tr_comment_seq OWNER TO gazelle;

--
-- Name: tm_section; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_section (
                                   id integer NOT NULL,
                                   content text,
                                   name character varying(255),
                                   rendered boolean,
                                   section_order integer,
                                   style_classes character varying(255),
                                   section_type_id integer
);


ALTER TABLE public.tm_section OWNER TO gazelle;

--
-- Name: tm_section_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_section_id_seq OWNER TO gazelle;

--
-- Name: tm_section_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_section_type (
                                        id integer NOT NULL,
                                        name character varying(255)
);


ALTER TABLE public.tm_section_type OWNER TO gazelle;

--
-- Name: tm_section_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_section_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_section_type_id_seq OWNER TO gazelle;

--
-- Name: tm_sop_class; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_sop_class (
                                     id integer NOT NULL,
                                     last_changed timestamp with time zone,
                                     last_modifier_id character varying(255),
                                     keyword character varying(255) NOT NULL,
                                     name character varying(255)
);


ALTER TABLE public.tm_sop_class OWNER TO gazelle;

--
-- Name: tm_sop_class_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_sop_class_aud (
                                         id integer NOT NULL,
                                         rev integer NOT NULL,
                                         revtype smallint,
                                         keyword character varying(255),
                                         name character varying(255)
);


ALTER TABLE public.tm_sop_class_aud OWNER TO gazelle;

--
-- Name: tm_sop_class_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_sop_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_sop_class_id_seq OWNER TO gazelle;

--
-- Name: tm_status_results; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_status_results (
                                          id integer NOT NULL,
                                          last_changed timestamp with time zone,
                                          last_modifier_id character varying(255),
                                          description character varying(255),
                                          keyword character varying(255),
                                          label_to_display character varying(255)
);


ALTER TABLE public.tm_status_results OWNER TO gazelle;

--
-- Name: tm_status_results_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_status_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_status_results_id_seq OWNER TO gazelle;

--
-- Name: tm_step_inst_msg_process_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_inst_msg_process_status (
                                                        id integer NOT NULL,
                                                        description character varying(1024) NOT NULL,
                                                        key character varying(255) NOT NULL,
                                                        label_key_for_display character varying(45) NOT NULL
);


ALTER TABLE public.tm_step_inst_msg_process_status OWNER TO gazelle;

--
-- Name: tm_step_inst_msg_process_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_step_inst_msg_process_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_step_inst_msg_process_status_id_seq OWNER TO gazelle;

--
-- Name: tm_step_instance_exec_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_instance_exec_status (
                                                     id integer NOT NULL,
                                                     description character varying(1024) NOT NULL,
                                                     key character varying(255) NOT NULL,
                                                     label_key_for_display character varying(45) NOT NULL
);


ALTER TABLE public.tm_step_instance_exec_status OWNER TO gazelle;

--
-- Name: tm_step_instance_exec_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_step_instance_exec_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_step_instance_exec_status_id_seq OWNER TO gazelle;

--
-- Name: tm_step_instance_message; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_instance_message (
                                                 id integer NOT NULL,
                                                 connection_id integer,
                                                 direction integer,
                                                 error_message character varying(255),
                                                 last_changed timestamp without time zone,
                                                 message_contents bytea,
                                                 proxy_host_name character varying(255),
                                                 proxy_msg_id integer,
                                                 proxy_port integer,
                                                 message_type_recieved character varying(255),
                                                 receiver_port integer,
                                                 sender_port integer,
                                                 process_status_id integer,
                                                 proxy_type_id integer,
                                                 receiver_host_id integer,
                                                 receiver_institution_id integer,
                                                 receiver_system_id integer,
                                                 receiver_tm_configuration_id integer,
                                                 relates_to integer,
                                                 sender_host_id integer,
                                                 sender_institution_id integer,
                                                 sender_system_id integer,
                                                 sender_tm_configuration_id integer,
                                                 tm_test_step_instance_id integer,
                                                 tm_test_step_message_profile_id integer
);


ALTER TABLE public.tm_step_instance_message OWNER TO gazelle;

--
-- Name: tm_step_instance_message_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_step_instance_message_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_step_instance_message_sequence OWNER TO gazelle;

--
-- Name: tm_step_instance_msg_validation; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_step_instance_msg_validation (
                                                        id integer NOT NULL,
                                                        last_changed timestamp without time zone,
                                                        validation_report bytea,
                                                        message_validation_service_id integer,
                                                        tm_step_instance_message_id integer,
                                                        validation_status_id integer
);


ALTER TABLE public.tm_step_instance_msg_validation OWNER TO gazelle;

--
-- Name: tm_step_instance_msg_validation_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_step_instance_msg_validation_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_step_instance_msg_validation_sequence OWNER TO gazelle;

--
-- Name: tm_subtypes_per_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_subtypes_per_system (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               system_id integer NOT NULL,
                                               system_subtypes_per_system_type_id integer NOT NULL
);


ALTER TABLE public.tm_subtypes_per_system OWNER TO gazelle;

--
-- Name: tm_subtypes_per_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_subtypes_per_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_subtypes_per_system_id_seq OWNER TO gazelle;

--
-- Name: tm_syslog_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_syslog_configuration (
                                                id integer NOT NULL,
                                                last_changed timestamp with time zone,
                                                last_modifier_id character varying(255),
                                                comments character varying(255),
                                                configuration_id integer NOT NULL,
                                                port integer,
                                                port_proxy integer,
                                                port_secured integer,
                                                protocol_version character varying(255),
                                                transport_layer_id integer NOT NULL,
                                                CONSTRAINT tm_syslog_configuration_port_check CHECK (((port <= 65635) AND (port >= 0))),
                                                CONSTRAINT tm_syslog_configuration_port_proxy_check CHECK (((port_proxy <= 65635) AND (port_proxy >= 0))),
                                                CONSTRAINT tm_syslog_configuration_port_secured_check CHECK (((port_secured >= 0) AND (port_secured <= 65635)))
);


ALTER TABLE public.tm_syslog_configuration OWNER TO gazelle;

--
-- Name: tm_system; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system (
                                  dtype character varying(31) NOT NULL,
                                  id integer NOT NULL,
                                  last_changed timestamp with time zone,
                                  last_modifier_id character varying(255),
                                  hl7_conformance_statement_url character varying(255),
                                  comment character varying(255),
                                  dicom_conformance_statement_url character varying(255),
                                  events_history character varying(92000),
                                  integration_statement_date timestamp without time zone,
                                  integration_statement_url character varying(255),
                                  is_dicom_system boolean,
                                  is_hl7_system boolean,
                                  is_tool boolean,
                                  keyword character varying(255),
                                  keyword_suffix character varying(60),
                                  name character varying(128) NOT NULL,
                                  integration_statement_generated_checksum character varying(255),
                                  integration_statement_validated_by_admin_checksum character varying(255),
                                  integration_statement_validated_by_admin_comment character varying(4096),
                                  counter_crawler_jobs_with_unmatching_hashcode integer,
                                  counter_crawler_jobs_with_unreachable_url integer,
                                  publication_request_date timestamp without time zone,
                                  integration_statement_generated_date timestamp without time zone,
                                  integration_statement_validated_by_admin_date timestamp without time zone,
                                  integration_statement_validated_last_time timestamp without time zone,
                                  is_email_sent_because_unmatching_hashcode boolean,
                                  is_email_sent_because_unreachable_url boolean,
                                  integration_statement_status integer,
                                  version character varying(255) NOT NULL,
                                  url character varying(255),
                                  owner_user_id integer NOT NULL,
                                  system_type_id integer
);


ALTER TABLE public.tm_system OWNER TO gazelle;

--
-- Name: tm_system_actor_profiles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_actor_profiles (
                                                 id integer NOT NULL,
                                                 last_changed timestamp with time zone,
                                                 last_modifier_id character varying(255),
                                                 testing_type_reviewed boolean,
                                                 actor_integration_profile_option_id integer,
                                                 system_id integer,
                                                 testing_type_id integer,
                                                 wanted_testing_type_id integer
);


ALTER TABLE public.tm_system_actor_profiles OWNER TO gazelle;

--
-- Name: tm_system_actor_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_actor_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_actor_profiles_id_seq OWNER TO gazelle;

--
-- Name: tm_system_aipo_result_for_a_testing_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_aipo_result_for_a_testing_session (
                                                                    id integer NOT NULL,
                                                                    last_changed timestamp with time zone,
                                                                    last_modifier_id character varying(255),
                                                                    comment character varying(255),
                                                                    enabled boolean,
                                                                    htmlcountfailed text,
                                                                    htmlcountprogress text,
                                                                    htmlcountverified text,
                                                                    htmlcountwaiting text,
                                                                    indicatorfailed integer,
                                                                    indicatorprogress integer,
                                                                    indicatorverified integer,
                                                                    indicatorwaiting integer,
                                                                    testrolescounto integer,
                                                                    testrolescountr integer,
                                                                    status_id integer,
                                                                    system_actor_profile_id integer NOT NULL,
                                                                    testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_system_aipo_result_for_a_testing_session OWNER TO gazelle;

--
-- Name: tm_system_aipo_result_for_a_testing_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_aipo_result_for_a_testing_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_aipo_result_for_a_testing_session_id_seq OWNER TO gazelle;

--
-- Name: tm_system_event; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_event (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        comment text,
                                        eventdate timestamp without time zone,
                                        type integer,
                                        username character varying(255)
);


ALTER TABLE public.tm_system_event OWNER TO gazelle;

--
-- Name: tm_system_event_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_event_id_seq OWNER TO gazelle;

--
-- Name: tm_system_events; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_events (
                                         tm_system_id integer NOT NULL,
                                         systemevents_id integer NOT NULL
);


ALTER TABLE public.tm_system_events OWNER TO gazelle;

--
-- Name: tm_system_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_id_seq OWNER TO gazelle;

--
-- Name: tm_system_in_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_in_session (
                                             dtype character varying(31) NOT NULL,
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             accepted_to_session boolean NOT NULL,
                                             ampere real,
                                             blog character varying(255),
                                             extra_ip_address integer,
                                             is_conf_stat_provided boolean,
                                             is_copy boolean,
                                             is_ihe_is_provided boolean,
                                             is_ihe_is_required boolean,
                                             noteslist bytea,
                                             power integer,
                                             registration_status integer,
                                             skype character varying(255),
                                             storage_volume real,
                                             table_label character varying(255),
                                             voltage integer,
                                             comment character varying(255),
                                             person_servicing integer,
                                             system_id integer,
                                             system_in_session_status_id integer,
                                             table_session_id integer,
                                             testing_session_id integer
);


ALTER TABLE public.tm_system_in_session OWNER TO gazelle;

--
-- Name: tm_system_in_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_in_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_in_session_id_seq OWNER TO gazelle;

--
-- Name: tm_system_in_session_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_in_session_status (
                                                    id integer NOT NULL,
                                                    last_changed timestamp with time zone,
                                                    last_modifier_id character varying(255),
                                                    description character varying(255),
                                                    keyword character varying(255),
                                                    label_to_display character varying(255)
);


ALTER TABLE public.tm_system_in_session_status OWNER TO gazelle;

--
-- Name: tm_system_in_session_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_in_session_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_in_session_status_id_seq OWNER TO gazelle;

--
-- Name: tm_system_in_session_user; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_system_in_session_user (
                                                  id integer NOT NULL,
                                                  system_in_session_id integer,
                                                  user_id integer
);


ALTER TABLE public.tm_system_in_session_user OWNER TO gazelle;

--
-- Name: tm_system_in_session_user_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_system_in_session_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_system_in_session_user_id_seq OWNER TO gazelle;

--
-- Name: tm_test; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test (
                                id integer NOT NULL,
                                last_changed timestamp with time zone,
                                last_modifier_id character varying(255),
                                author character varying(255),
                                auto_complete boolean,
                                child_node_update timestamp with time zone,
                                cttintegrated boolean,
                                keyword character varying(64) NOT NULL,
                                last_validator_id character varying(255),
                                name character varying(255) NOT NULL,
                                orchestrable boolean,
                                short_description character varying(512) NOT NULL,
                                validated boolean,
                                version character varying(255),
                                test_peer_type_id integer,
                                test_status_id integer,
                                test_type_id integer NOT NULL
);


ALTER TABLE public.tm_test OWNER TO gazelle;

--
-- Name: tm_test_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_aud (
                                    id integer NOT NULL,
                                    rev integer NOT NULL,
                                    revtype smallint,
                                    author character varying(255),
                                    child_node_update timestamp with time zone,
                                    cttintegrated boolean,
                                    keyword character varying(64),
                                    name character varying(255),
                                    orchestrable boolean,
                                    short_description character varying(512),
                                    validated boolean,
                                    version character varying(255),
                                    test_peer_type_id integer,
                                    test_status_id integer,
                                    test_type_id integer
);


ALTER TABLE public.tm_test_aud OWNER TO gazelle;

--
-- Name: tm_test_description; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_description (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            description text NOT NULL,
                                            gazelle_language_id integer NOT NULL
);


ALTER TABLE public.tm_test_description OWNER TO gazelle;

--
-- Name: tm_test_description_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_description_aud (
                                                id integer NOT NULL,
                                                rev integer NOT NULL,
                                                revtype smallint,
                                                description text,
                                                gazelle_language_id integer
);


ALTER TABLE public.tm_test_description_aud OWNER TO gazelle;

--
-- Name: tm_test_description_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_description_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_description_id_seq OWNER TO gazelle;

--
-- Name: tm_test_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         bpel_server_id character varying(255),
                                         comments text,
                                         contains_same_system_or_company boolean,
                                         description text,
                                         last_status_id integer,
                                         proxy_used boolean,
                                         test_version integer,
                                         execution_status_id integer,
                                         monitor_id integer,
                                         test_id integer,
                                         testing_session_id integer NOT NULL
);


ALTER TABLE public.tm_test_instance OWNER TO gazelle;

--
-- Name: tm_test_instance_event; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_event (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               comment text,
                                               date_of_event timestamp without time zone,
                                               description text,
                                               filename text,
                                               status_id integer,
                                               type integer,
                                               username character varying(250)
);


ALTER TABLE public.tm_test_instance_event OWNER TO gazelle;

--
-- Name: tm_test_instance_event_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_event_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_event_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_exec_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_exec_status (
                                                     id integer NOT NULL,
                                                     description character varying(1024) NOT NULL,
                                                     key character varying(255) NOT NULL,
                                                     label_key_for_display character varying(45) NOT NULL
);


ALTER TABLE public.tm_test_instance_exec_status OWNER TO gazelle;

--
-- Name: tm_test_instance_exec_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_exec_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_exec_status_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_participants (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      actor_integration_profile_option_id integer,
                                                      role_in_test_id integer,
                                                      status_id integer,
                                                      system_in_session_user_id integer,
                                                      test_instance_id integer
);


ALTER TABLE public.tm_test_instance_participants OWNER TO gazelle;

--
-- Name: tm_test_instance_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_participants_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_participants_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_participants_status (
                                                             id integer NOT NULL,
                                                             description character varying(255),
                                                             keyword character varying(255),
                                                             label_to_display character varying(255)
);


ALTER TABLE public.tm_test_instance_participants_status OWNER TO gazelle;

--
-- Name: tm_test_instance_participants_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_participants_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_participants_status_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_path_to_log_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_path_to_log_file (
                                                          id integer NOT NULL,
                                                          last_changed timestamp with time zone,
                                                          last_modifier_id character varying(255),
                                                          filename character varying(255),
                                                          path character varying(255) NOT NULL,
                                                          type character varying(255) NOT NULL,
                                                          withoutfilename boolean
);


ALTER TABLE public.tm_test_instance_path_to_log_file OWNER TO gazelle;

--
-- Name: tm_test_instance_path_to_log_file_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_instance_path_to_log_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_instance_path_to_log_file_id_seq OWNER TO gazelle;

--
-- Name: tm_test_instance_test_instance_path_to_log_file; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_test_instance_path_to_log_file (
                                                                        test_instance_id integer NOT NULL,
                                                                        test_instance_path_id integer NOT NULL
);


ALTER TABLE public.tm_test_instance_test_instance_path_to_log_file OWNER TO gazelle;

--
-- Name: tm_test_instance_test_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_test_status (
                                                     test_instance_id integer NOT NULL,
                                                     status_id integer NOT NULL
);


ALTER TABLE public.tm_test_instance_test_status OWNER TO gazelle;

--
-- Name: tm_test_instance_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_test_steps_instance (
                                                             test_instance_id integer,
                                                             test_steps_instance_id integer NOT NULL
);


ALTER TABLE public.tm_test_instance_test_steps_instance OWNER TO gazelle;

--
-- Name: tm_test_instance_tm_test_steps_data; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_tm_test_steps_data (
                                                            tm_test_instance_id integer NOT NULL,
                                                            teststepsdatalist_id integer NOT NULL
);


ALTER TABLE public.tm_test_instance_tm_test_steps_data OWNER TO gazelle;

--
-- Name: tm_test_instance_token; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_instance_token (
                                               test_instance_identifier integer NOT NULL,
                                               username character varying(255) NOT NULL,
                                               creation_date_time timestamp without time zone NOT NULL,
                                               value character varying(255) NOT NULL
);


ALTER TABLE public.tm_test_instance_token OWNER TO gazelle;

--
-- Name: tm_test_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_option (
                                       id integer NOT NULL,
                                       last_changed timestamp with time zone,
                                       last_modifier_id character varying(255),
                                       available boolean,
                                       description character varying(255),
                                       keyword character varying(255),
                                       label_to_display character varying(255)
);


ALTER TABLE public.tm_test_option OWNER TO gazelle;

--
-- Name: tm_test_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_option_aud (
                                           id integer NOT NULL,
                                           rev integer NOT NULL,
                                           revtype smallint,
                                           available boolean,
                                           description character varying(255),
                                           keyword character varying(255),
                                           label_to_display character varying(255)
);


ALTER TABLE public.tm_test_option_aud OWNER TO gazelle;

--
-- Name: tm_test_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_option_id_seq OWNER TO gazelle;

--
-- Name: tm_test_participants; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_participants (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             is_tested boolean NOT NULL,
                                             actor_integration_profile_option_id integer NOT NULL
);


ALTER TABLE public.tm_test_participants OWNER TO gazelle;

--
-- Name: tm_test_participants_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_participants_aud (
                                                 id integer NOT NULL,
                                                 rev integer NOT NULL,
                                                 revtype smallint,
                                                 is_tested boolean,
                                                 actor_integration_profile_option_id integer
);


ALTER TABLE public.tm_test_participants_aud OWNER TO gazelle;

--
-- Name: tm_test_participants_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_participants_id_seq OWNER TO gazelle;

--
-- Name: tm_test_peer_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_peer_type (
                                          id integer NOT NULL,
                                          last_changed timestamp with time zone,
                                          last_modifier_id character varying(255),
                                          description character varying(255),
                                          keyword character varying(255),
                                          label_to_display character varying(255)
);


ALTER TABLE public.tm_test_peer_type OWNER TO gazelle;

--
-- Name: tm_test_peer_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_peer_type_aud (
                                              id integer NOT NULL,
                                              rev integer NOT NULL,
                                              revtype smallint,
                                              description character varying(255),
                                              keyword character varying(255),
                                              label_to_display character varying(255)
);


ALTER TABLE public.tm_test_peer_type_aud OWNER TO gazelle;

--
-- Name: tm_test_peer_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_peer_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_peer_type_id_seq OWNER TO gazelle;

--
-- Name: tm_test_roles; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_roles (
                                      id integer NOT NULL,
                                      last_changed timestamp with time zone,
                                      last_modifier_id character varying(255),
                                      card_max integer,
                                      card_min integer,
                                      number_of_tests_to_realize integer,
                                      role_rank integer,
                                      url character varying(255),
                                      url_doc character varying(255),
                                      role_in_test_id integer,
                                      test_id integer NOT NULL,
                                      test_option_id integer NOT NULL,
                                      CONSTRAINT tm_test_roles_number_of_tests_to_realize_check CHECK ((number_of_tests_to_realize >= 1))
);


ALTER TABLE public.tm_test_roles OWNER TO gazelle;

--
-- Name: tm_test_roles_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_roles_aud (
                                          id integer NOT NULL,
                                          rev integer NOT NULL,
                                          revtype smallint,
                                          card_max integer,
                                          card_min integer,
                                          number_of_tests_to_realize integer,
                                          role_rank integer,
                                          url character varying(255),
                                          url_doc character varying(255),
                                          role_in_test_id integer,
                                          test_id integer,
                                          test_option_id integer
);


ALTER TABLE public.tm_test_roles_aud OWNER TO gazelle;

--
-- Name: tm_test_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_roles_id_seq OWNER TO gazelle;

--
-- Name: tm_test_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_status (
                                       id integer NOT NULL,
                                       last_changed timestamp with time zone,
                                       last_modifier_id character varying(255),
                                       description character varying(255),
                                       keyword character varying(255),
                                       label_to_display character varying(255)
);


ALTER TABLE public.tm_test_status OWNER TO gazelle;

--
-- Name: tm_test_status_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_status_aud (
                                           id integer NOT NULL,
                                           rev integer NOT NULL,
                                           revtype smallint,
                                           description character varying(255),
                                           keyword character varying(255),
                                           label_to_display character varying(255)
);


ALTER TABLE public.tm_test_status_aud OWNER TO gazelle;

--
-- Name: tm_test_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_status_id_seq OWNER TO gazelle;

--
-- Name: tm_test_step_message_profile; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_step_message_profile (
                                                     id integer NOT NULL,
                                                     auto_validate smallint,
                                                     direction integer,
                                                     example_msg_content character varying(2000000),
                                                     example_msg_file_name character varying(255),
                                                     validation_context_content character varying(2000000),
                                                     validation_context_file_name character varying(255),
                                                     tf_hl7_message_profile_id integer,
                                                     tm_test_steps_id integer
);


ALTER TABLE public.tm_test_step_message_profile OWNER TO gazelle;

--
-- Name: tm_test_step_message_profile_sequence; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_step_message_profile_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_step_message_profile_sequence OWNER TO gazelle;

--
-- Name: tm_test_steps; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps (
                                      id integer NOT NULL,
                                      last_changed timestamp with time zone,
                                      last_modifier_id character varying(255),
                                      auto_complete boolean,
                                      auto_triggered boolean,
                                      description character varying(1024) NOT NULL,
                                      expected_message_count integer,
                                      hl7_version character varying(255),
                                      message_type character varying(255),
                                      responder_message_type character varying(255),
                                      secured boolean,
                                      step_index integer NOT NULL,
                                      test_roles_initiator_id integer NOT NULL,
                                      test_roles_responder_id integer,
                                      test_steps_option_id integer,
                                      transaction_id integer,
                                      ws_transaction_usage_id integer
);


ALTER TABLE public.tm_test_steps OWNER TO gazelle;

--
-- Name: tm_test_steps_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_aud (
                                          id integer NOT NULL,
                                          rev integer NOT NULL,
                                          revtype smallint,
                                          description character varying(1024),
                                          message_type character varying(255),
                                          secured boolean,
                                          step_index integer,
                                          test_roles_initiator_id integer,
                                          test_roles_responder_id integer,
                                          test_steps_option_id integer,
                                          transaction_id integer,
                                          ws_transaction_usage_id integer
);


ALTER TABLE public.tm_test_steps_aud OWNER TO gazelle;

--
-- Name: tm_test_steps_data; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_data (
                                           id integer NOT NULL,
                                           last_changed timestamp with time zone,
                                           last_modifier_id character varying(255),
                                           comment text,
                                           value text,
                                           withoutfilename boolean,
                                           data_type_id integer
);


ALTER TABLE public.tm_test_steps_data OWNER TO gazelle;

--
-- Name: tm_test_steps_data_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_steps_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_steps_data_id_seq OWNER TO gazelle;

--
-- Name: tm_test_steps_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_steps_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_steps_id_seq OWNER TO gazelle;

--
-- Name: tm_test_steps_input_ci; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_input_ci (
                                               test_steps_id integer NOT NULL,
                                               contextual_information_id integer NOT NULL
);


ALTER TABLE public.tm_test_steps_input_ci OWNER TO gazelle;

--
-- Name: tm_test_steps_input_ci_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_input_ci_aud (
                                                   rev integer NOT NULL,
                                                   test_steps_id integer NOT NULL,
                                                   contextual_information_id integer NOT NULL,
                                                   revtype smallint
);


ALTER TABLE public.tm_test_steps_input_ci_aud OWNER TO gazelle;

--
-- Name: tm_test_steps_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_instance (
                                               id integer NOT NULL,
                                               last_changed timestamp with time zone,
                                               last_modifier_id character varying(255),
                                               comments text,
                                               status_id integer,
                                               test_steps_version integer,
                                               execution_status_id integer,
                                               system_in_session_initiator_id integer NOT NULL,
                                               system_in_session_reponder_id integer,
                                               test_steps_id integer NOT NULL,
                                               test_steps_instance_status_id integer
);


ALTER TABLE public.tm_test_steps_instance OWNER TO gazelle;

--
-- Name: tm_test_steps_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_steps_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_steps_instance_id_seq OWNER TO gazelle;

--
-- Name: tm_test_steps_instance_input_ci_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_instance_input_ci_instance (
                                                                 test_steps_instance_id integer NOT NULL,
                                                                 contextual_information_instance_id integer NOT NULL
);


ALTER TABLE public.tm_test_steps_instance_input_ci_instance OWNER TO gazelle;

--
-- Name: tm_test_steps_instance_output_ci_instance; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_instance_output_ci_instance (
                                                                  test_steps_instance_id integer,
                                                                  contextual_information_instance_id integer NOT NULL
);


ALTER TABLE public.tm_test_steps_instance_output_ci_instance OWNER TO gazelle;

--
-- Name: tm_test_steps_instance_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_instance_status (
                                                      id integer NOT NULL,
                                                      description character varying(255),
                                                      keyword character varying(255),
                                                      label_to_display character varying(255)
);


ALTER TABLE public.tm_test_steps_instance_status OWNER TO gazelle;

--
-- Name: tm_test_steps_instance_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_steps_instance_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_steps_instance_status_id_seq OWNER TO gazelle;

--
-- Name: tm_test_steps_option; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_option (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             description character varying(255),
                                             keyword character varying(255),
                                             label_to_display character varying(255)
);


ALTER TABLE public.tm_test_steps_option OWNER TO gazelle;

--
-- Name: tm_test_steps_option_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_option_aud (
                                                 id integer NOT NULL,
                                                 rev integer NOT NULL,
                                                 revtype smallint,
                                                 description character varying(255),
                                                 keyword character varying(255),
                                                 label_to_display character varying(255)
);


ALTER TABLE public.tm_test_steps_option_aud OWNER TO gazelle;

--
-- Name: tm_test_steps_option_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_steps_option_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_steps_option_id_seq OWNER TO gazelle;

--
-- Name: tm_test_steps_output_ci; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_output_ci (
                                                test_steps_id integer,
                                                contextual_information_id integer NOT NULL
);


ALTER TABLE public.tm_test_steps_output_ci OWNER TO gazelle;

--
-- Name: tm_test_steps_output_ci_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_output_ci_aud (
                                                    rev integer NOT NULL,
                                                    test_steps_id integer NOT NULL,
                                                    contextual_information_id integer NOT NULL,
                                                    revtype smallint
);


ALTER TABLE public.tm_test_steps_output_ci_aud OWNER TO gazelle;

--
-- Name: tm_test_steps_test_steps_data; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_steps_test_steps_data (
                                                      test_steps_id integer NOT NULL,
                                                      test_steps_data_id integer NOT NULL
);


ALTER TABLE public.tm_test_steps_test_steps_data OWNER TO gazelle;

--
-- Name: tm_test_test_description; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_test_description (
                                                 test_id integer,
                                                 test_description_id integer NOT NULL
);


ALTER TABLE public.tm_test_test_description OWNER TO gazelle;

--
-- Name: tm_test_test_description_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_test_description_aud (
                                                     rev integer NOT NULL,
                                                     test_id integer NOT NULL,
                                                     test_description_id integer NOT NULL,
                                                     revtype smallint
);


ALTER TABLE public.tm_test_test_description_aud OWNER TO gazelle;

--
-- Name: tm_test_test_steps; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_test_steps (
                                           test_id integer,
                                           test_steps_id integer NOT NULL
);


ALTER TABLE public.tm_test_test_steps OWNER TO gazelle;

--
-- Name: tm_test_test_steps_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_test_steps_aud (
                                               rev integer NOT NULL,
                                               test_id integer NOT NULL,
                                               test_steps_id integer NOT NULL,
                                               revtype smallint
);


ALTER TABLE public.tm_test_test_steps_aud OWNER TO gazelle;

--
-- Name: tm_test_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_type (
                                     id integer NOT NULL,
                                     last_changed timestamp with time zone,
                                     last_modifier_id character varying(255),
                                     description character varying(255),
                                     keyword character varying(255),
                                     label_to_display character varying(255)
);


ALTER TABLE public.tm_test_type OWNER TO gazelle;

--
-- Name: tm_test_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_type_aud (
                                         id integer NOT NULL,
                                         rev integer NOT NULL,
                                         revtype smallint,
                                         description character varying(255),
                                         keyword character varying(255),
                                         label_to_display character varying(255)
);


ALTER TABLE public.tm_test_type_aud OWNER TO gazelle;

--
-- Name: tm_test_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_test_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_test_type_id_seq OWNER TO gazelle;

--
-- Name: tm_test_types_sessions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_types_sessions (
                                               testing_session_id integer NOT NULL,
                                               test_type_id integer NOT NULL
);


ALTER TABLE public.tm_test_types_sessions OWNER TO gazelle;

--
-- Name: tm_test_user_comment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_test_user_comment (
                                             test_id integer NOT NULL,
                                             user_comment_id integer NOT NULL
);


ALTER TABLE public.tm_test_user_comment OWNER TO gazelle;

--
-- Name: tm_testing_depth; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_testing_depth (
                                         id integer NOT NULL,
                                         last_changed timestamp with time zone,
                                         last_modifier_id character varying(255),
                                         description character varying(255),
                                         image_url character varying(255),
                                         keyword character varying(255) NOT NULL,
                                         name character varying(128) NOT NULL
);


ALTER TABLE public.tm_testing_depth OWNER TO gazelle;

--
-- Name: tm_testing_depth_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_testing_depth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_testing_depth_id_seq OWNER TO gazelle;

--
-- Name: tm_testing_session; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_testing_session (
                                           id integer NOT NULL,
                                           last_changed timestamp with time zone,
                                           last_modifier_id character varying(255),
                                           active boolean,
                                           allow_one_company_play_several_role_in_group_tests boolean NOT NULL,
                                           allow_one_company_play_several_role_in_p2p_tests boolean NOT NULL,
                                           allowparticipantregistration boolean,
                                           allow_participants_start_group_tests boolean NOT NULL,
                                           beginning_date timestamp without time zone,
                                           certificates_rul character varying(255),
                                           configuration_overview character varying(20000),
                                           conformity_test_report_enabled boolean,
                                           contact_email character varying(255) NOT NULL,
                                           contact_firstname character varying(255) NOT NULL,
                                           contact_lastname character varying(255) NOT NULL,
                                           continuous_session boolean,
                                           required_contract boolean,
                                           default_testing_session boolean,
                                           description character varying(255),
                                           disable_auto_results boolean,
                                           disable_patient_generation_and_sharing boolean,
                                           display_certificates_menu boolean,
                                           domain_fee numeric(19,2),
                                           enable_precat_tests boolean,
                                           ending_date timestamp without time zone,
                                           fee_additional_system numeric(19,2),
                                           fee_first_system numeric(19,2),
                                           fee_participant numeric(19,2),
                                           hidden boolean,
                                           hide_advanced_sample_search_to_vendors boolean NOT NULL,
                                           internettesting boolean,
                                           invoice_payment_deadline_date timestamp without time zone,
                                           is_critical_status_enabled boolean,
                                           is_proxy_use_enabled boolean,
                                           iso3_vat_countries character varying(255),
                                           logo_link_url character varying(255),
                                           logo_url character varying(255),
                                           mailing_list_url character varying(255),
                                           ti_accessed_by_qr integer,
                                           ti_claimed_by_qr integer,
                                           ti_verified_on_smartphone integer,
                                           nb_participants_included_in_system_fees integer DEFAULT 2,
                                           next_invoice_number integer,
                                           order_in_gui integer,
                                           contract_template_path character varying(255),
                                           invoice_template_path character varying(255),
                                           registration_deadline_date timestamp without time zone,
                                           results_compute timestamp without time zone,
                                           send_notification_by_email boolean,
                                           session_closed boolean,
                                           session_without_monitors boolean NOT NULL,
                                           systemautoacceptance boolean NOT NULL,
                                           target_color character varying(255),
                                           type character varying(255) NOT NULL,
                                           vat_percent numeric(19,2),
                                           wiki_url character varying(255),
                                           year integer,
                                           zone character varying(255) NOT NULL,
                                           address_id integer,
                                           currency_id character varying(12),
                                           networkconfiguration_id integer
);


ALTER TABLE public.tm_testing_session OWNER TO gazelle;

--
-- Name: tm_testing_session_admin; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_testing_session_admin (
                                                 testing_session_id integer NOT NULL,
                                                 user_id integer NOT NULL
);


ALTER TABLE public.tm_testing_session_admin OWNER TO gazelle;

--
-- Name: tm_testing_session_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_testing_session_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_testing_session_id_seq OWNER TO gazelle;

--
-- Name: tm_transport_layer_for_config; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_transport_layer_for_config (
                                                      id integer NOT NULL,
                                                      last_changed timestamp with time zone,
                                                      last_modifier_id character varying(255),
                                                      keyword character varying(255) NOT NULL,
                                                      name character varying(255)
);


ALTER TABLE public.tm_transport_layer_for_config OWNER TO gazelle;

--
-- Name: tm_transport_layer_for_config_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_transport_layer_for_config_aud (
                                                          id integer NOT NULL,
                                                          rev integer NOT NULL,
                                                          revtype smallint,
                                                          keyword character varying(255),
                                                          name character varying(255)
);


ALTER TABLE public.tm_transport_layer_for_config_aud OWNER TO gazelle;

--
-- Name: tm_transport_layer_for_config_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_transport_layer_for_config_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_transport_layer_for_config_seq OWNER TO gazelle;

--
-- Name: tm_user_comment; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_user_comment (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        comment_content text NOT NULL,
                                        creation_date timestamp without time zone,
                                        user_id character varying(255) NOT NULL
);


ALTER TABLE public.tm_user_comment OWNER TO gazelle;

--
-- Name: tm_user_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_user_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_user_comment_id_seq OWNER TO gazelle;

--
-- Name: tm_user_photo; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_user_photo (
                                      id integer NOT NULL,
                                      photo_bytes bytea
);


ALTER TABLE public.tm_user_photo OWNER TO gazelle;

--
-- Name: tm_user_photo_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_user_photo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_user_photo_id_seq OWNER TO gazelle;

--
-- Name: tm_user_preferences; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_user_preferences (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            is_email_displayed boolean,
                                            is_tooltips_displayed boolean,
                                            number_of_results_per_page_id integer,
                                            connectathon_table character varying(255),
                                            email_notification boolean,
                                            show_sequence_diagram boolean,
                                            skype character varying(255),
                                            spoken_languages character varying(255),
                                            user_id integer NOT NULL,
                                            selected_testing_session_id integer,
                                            userphoto_id integer
);


ALTER TABLE public.tm_user_preferences OWNER TO gazelle;

--
-- Name: tm_user_preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_user_preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_user_preferences_id_seq OWNER TO gazelle;

--
-- Name: tm_web_service_configuration; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_web_service_configuration (
                                                     id integer NOT NULL,
                                                     last_changed timestamp with time zone,
                                                     last_modifier_id character varying(255),
                                                     comments character varying(255),
                                                     configuration_id integer NOT NULL,
                                                     assigning_authority character varying(255),
                                                     ass_auth_oid character varying(255),
                                                     port integer,
                                                     port_proxy integer,
                                                     port_secured integer,
                                                     url character varying(255) NOT NULL,
                                                     usage character varying(255),
                                                     web_service_detail character varying(255),
                                                     webservice_type integer,
                                                     ws_transaction_usage integer,
                                                     CONSTRAINT tm_web_service_configuration_port_check CHECK (((port >= 0) AND (port <= 65635))),
                                                     CONSTRAINT tm_web_service_configuration_port_proxy_check CHECK (((port_proxy >= 1024) AND (port_proxy <= 65635))),
                                                     CONSTRAINT tm_web_service_configuration_port_secured_check CHECK (((port_secured >= 0) AND (port_secured <= 65635)))
);


ALTER TABLE public.tm_web_service_configuration OWNER TO gazelle;

--
-- Name: tm_web_service_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_web_service_type (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            description character varying(255),
                                            needs_oid boolean,
                                            profile_id integer
);


ALTER TABLE public.tm_web_service_type OWNER TO gazelle;

--
-- Name: tm_web_service_type_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.tm_web_service_type_aud (
                                                id integer NOT NULL,
                                                rev integer NOT NULL,
                                                revtype smallint,
                                                description character varying(255),
                                                needs_oid boolean,
                                                profile_id integer
);


ALTER TABLE public.tm_web_service_type_aud OWNER TO gazelle;

--
-- Name: tm_web_service_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.tm_web_service_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tm_web_service_type_id_seq OWNER TO gazelle;

--
-- Name: usr_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_address (
                                    id integer NOT NULL,
                                    last_changed timestamp with time zone,
                                    last_modifier_id character varying(255),
                                    address character varying(512),
                                    address_line_2 character varying(512) DEFAULT ''::character varying NOT NULL,
                                    address_type character varying(255),
                                    city character varying(255),
                                    fax character varying(64),
                                    phone character varying(64),
                                    state character varying(255),
                                    zip_code character varying(20),
                                    country character varying(2)
);


ALTER TABLE public.usr_address OWNER TO gazelle;

--
-- Name: usr_address_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_address_id_seq OWNER TO gazelle;

--
-- Name: usr_currency; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_currency (
                                     keyword character varying(12) NOT NULL,
                                     last_changed timestamp with time zone,
                                     last_modifier_id character varying(255),
                                     comment character varying(255),
                                     name character varying(255)
);


ALTER TABLE public.usr_currency OWNER TO gazelle;

--
-- Name: usr_gazelle_language; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_gazelle_language (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             description character varying(255) NOT NULL,
                                             keyword character varying(255) NOT NULL
);


ALTER TABLE public.usr_gazelle_language OWNER TO gazelle;

--
-- Name: usr_gazelle_language_aud; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_gazelle_language_aud (
                                                 id integer NOT NULL,
                                                 rev integer NOT NULL,
                                                 revtype smallint,
                                                 description character varying(255),
                                                 keyword character varying(255)
);


ALTER TABLE public.usr_gazelle_language_aud OWNER TO gazelle;

--
-- Name: usr_gazelle_language_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_gazelle_language_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_gazelle_language_id_seq OWNER TO gazelle;

--
-- Name: usr_institution; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution (
                                        id integer NOT NULL,
                                        last_changed timestamp with time zone,
                                        last_modifier_id character varying(255),
                                        activated boolean,
                                        integration_statements_repository_url character varying(512),
                                        keyword character varying(16) NOT NULL,
                                        name character varying(255) NOT NULL,
                                        note character varying(1024),
                                        url character varying(512) NOT NULL,
                                        institution_type_id integer NOT NULL,
                                        mailing_address_id integer
);


ALTER TABLE public.usr_institution OWNER TO gazelle;

--
-- Name: usr_institution_address; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution_address (
                                                address_id integer NOT NULL,
                                                institution_id integer NOT NULL
);


ALTER TABLE public.usr_institution_address OWNER TO gazelle;

--
-- Name: usr_institution_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_institution_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_institution_id_seq OWNER TO gazelle;

--
-- Name: usr_institution_type; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_institution_type (
                                             id integer NOT NULL,
                                             last_changed timestamp with time zone,
                                             last_modifier_id character varying(255),
                                             description character varying(255),
                                             type character varying(32) NOT NULL
);


ALTER TABLE public.usr_institution_type OWNER TO gazelle;

--
-- Name: usr_institution_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_institution_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_institution_type_id_seq OWNER TO gazelle;

--
-- Name: usr_iso_3166_country_code; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_iso_3166_country_code (
                                                  iso character varying(2) NOT NULL,
                                                  ec boolean,
                                                  flag_url character varying(255),
                                                  iso3 character varying(3),
                                                  name character varying(80) NOT NULL,
                                                  numcode integer,
                                                  printable_name character varying(80) NOT NULL
);


ALTER TABLE public.usr_iso_3166_country_code OWNER TO gazelle;

--
-- Name: usr_message_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_message_parameters (
                                               id integer NOT NULL,
                                               messageparameter character varying(255)
);


ALTER TABLE public.usr_message_parameters OWNER TO gazelle;

--
-- Name: usr_message_parameters_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_message_parameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_message_parameters_id_seq OWNER TO gazelle;

--
-- Name: usr_messages; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_messages (
                                     id integer NOT NULL,
                                     author character varying(255),
                                     date timestamp without time zone,
                                     image character varying(255),
                                     link character varying(255),
                                     testingsession bytea,
                                     type character varying(255)
);


ALTER TABLE public.usr_messages OWNER TO gazelle;

--
-- Name: usr_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_messages_id_seq OWNER TO gazelle;

--
-- Name: usr_messages_usr_message_parameters; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_messages_usr_message_parameters (
                                                            usr_messages_id integer NOT NULL,
                                                            messageparameters_id integer NOT NULL
);


ALTER TABLE public.usr_messages_usr_message_parameters OWNER TO gazelle;

--
-- Name: usr_messages_usr_users; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_messages_usr_users (
                                               usr_messages_id integer NOT NULL,
                                               users_id integer NOT NULL
);


ALTER TABLE public.usr_messages_usr_users OWNER TO gazelle;

--
-- Name: usr_person; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_person (
                                   id integer NOT NULL,
                                   last_changed timestamp with time zone,
                                   last_modifier_id character varying(255),
                                   cell_phone character varying(64),
                                   email character varying(255),
                                   firstname character varying(255) NOT NULL,
                                   lastname character varying(255) NOT NULL,
                                   mailing_list boolean,
                                   personal_fax character varying(64),
                                   personal_phone character varying(64),
                                   photo character varying(512),
                                   title character varying(255),
                                   address_id integer,
                                   institution_id integer NOT NULL
);


ALTER TABLE public.usr_person OWNER TO gazelle;

--
-- Name: usr_person_function; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_person_function (
                                            id integer NOT NULL,
                                            last_changed timestamp with time zone,
                                            last_modifier_id character varying(255),
                                            description character varying(1024),
                                            is_billing boolean,
                                            keyword character varying(255) NOT NULL,
                                            name character varying(255) NOT NULL
);


ALTER TABLE public.usr_person_function OWNER TO gazelle;

--
-- Name: usr_person_function_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_person_function_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_person_function_id_seq OWNER TO gazelle;

--
-- Name: usr_person_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_person_id_seq OWNER TO gazelle;

--
-- Name: usr_persons_functions; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_persons_functions (
                                              person_id integer NOT NULL,
                                              person_function_id integer NOT NULL
);


ALTER TABLE public.usr_persons_functions OWNER TO gazelle;

--
-- Name: usr_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_role (
                                 id integer NOT NULL,
                                 last_changed timestamp with time zone,
                                 last_modifier_id character varying(255),
                                 description character varying(1024),
                                 name character varying(64) NOT NULL
);


ALTER TABLE public.usr_role OWNER TO gazelle;

--
-- Name: usr_role_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_role_id_seq OWNER TO gazelle;

--
-- Name: usr_user_role; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_user_role (
                                      user_id integer NOT NULL,
                                      role_id integer NOT NULL
);


ALTER TABLE public.usr_user_role OWNER TO gazelle;

--
-- Name: usr_users; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.usr_users (
                                  id integer NOT NULL,
                                  last_changed timestamp with time zone,
                                  last_modifier_id character varying(255),
                                  activated boolean,
                                  activation_code character varying(255),
                                  blocked boolean,
                                  change_password_code character varying(255),
                                  creation_date timestamp without time zone,
                                  email character varying(255) NOT NULL,
                                  counter_failed_login_attempts integer,
                                  firstname character varying(128),
                                  last_login timestamp without time zone,
                                  lastname character varying(128),
                                  counter_logins integer,
                                  password character varying(128) NOT NULL,
                                  username character varying(16) NOT NULL,
                                  institution_id integer NOT NULL
);


ALTER TABLE public.usr_users OWNER TO gazelle;

--
-- Name: usr_users_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.usr_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usr_users_id_seq OWNER TO gazelle;

--
-- Name: validation_service; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validation_service (
                                           id integer NOT NULL,
                                           base_url character varying(255),
                                           description character varying(1024) NOT NULL,
                                           key character varying(45) NOT NULL,
                                           label_key_for_display character varying(255),
                                           url_path character varying(255),
                                           xslt_url character varying(1024)
);


ALTER TABLE public.validation_service OWNER TO gazelle;

--
-- Name: validation_service_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.validation_service_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.validation_service_id_seq OWNER TO gazelle;

--
-- Name: validation_status; Type: TABLE; Schema: public; Owner: gazelle
--

CREATE TABLE public.validation_status (
                                          id integer NOT NULL,
                                          description character varying(1024) NOT NULL,
                                          key character varying(45) NOT NULL,
                                          label_key_for_display character varying(255)
);


ALTER TABLE public.validation_status OWNER TO gazelle;

--
-- Name: validation_status_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE public.validation_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.validation_status_id_seq OWNER TO gazelle;

--
-- Name: cmn_application_preference cmn_application_preference_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT cmn_application_preference_pkey PRIMARY KEY (id);


--
-- Name: cmn_path_linking_a_document cmn_path_linking_a_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_path_linking_a_document
    ADD CONSTRAINT cmn_path_linking_a_document_pkey PRIMARY KEY (id);


--
-- Name: message_validation_service message_validation_service_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.message_validation_service
    ADD CONSTRAINT message_validation_service_pkey PRIMARY KEY (id);


--
-- Name: pr_crawler_reporting pr_crawler_reporting_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_reporting
    ADD CONSTRAINT pr_crawler_reporting_pkey PRIMARY KEY (id);


--
-- Name: pr_integration_statement_download pr_integration_statement_download_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_integration_statement_download
    ADD CONSTRAINT pr_integration_statement_download_pkey PRIMARY KEY (id);


--
-- Name: pr_intro pr_intro_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_intro
    ADD CONSTRAINT pr_intro_pkey PRIMARY KEY (id);


--
-- Name: pr_mail pr_mail_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_mail
    ADD CONSTRAINT pr_mail_pkey PRIMARY KEY (id);


--
-- Name: pr_search_log_criterion pr_search_log_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_log_criterion
    ADD CONSTRAINT pr_search_log_criterion_pkey PRIMARY KEY (id);


--
-- Name: pr_search_log_report pr_search_log_report_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_log_report
    ADD CONSTRAINT pr_search_log_report_pkey PRIMARY KEY (id);


--
-- Name: proxy_type proxy_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.proxy_type
    ADD CONSTRAINT proxy_type_pkey PRIMARY KEY (id);


--
-- Name: revinfo revinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.revinfo
    ADD CONSTRAINT revinfo_pkey PRIMARY KEY (rev);


--
-- Name: sys_system_subtype sys_system_subtype_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtype
    ADD CONSTRAINT sys_system_subtype_pkey PRIMARY KEY (id);


--
-- Name: sys_system_subtypes_per_system_type sys_system_subtypes_per_system_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtypes_per_system_type
    ADD CONSTRAINT sys_system_subtypes_per_system_type_pkey PRIMARY KEY (id);


--
-- Name: sys_system_type sys_system_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_type
    ADD CONSTRAINT sys_system_type_pkey PRIMARY KEY (id);


--
-- Name: sys_table_session sys_table_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_table_session
    ADD CONSTRAINT sys_table_session_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_aud tf_actor_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT tf_actor_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_default_ports tf_actor_default_ports_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_default_ports
    ADD CONSTRAINT tf_actor_default_ports_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile_aud tf_actor_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT tf_actor_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option_aud tf_actor_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT tf_actor_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_actor_integration_profile_option tf_actor_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT tf_actor_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_actor_integration_profile tf_actor_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT tf_actor_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_actor tf_actor_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT tf_actor_pkey PRIMARY KEY (id);


--
-- Name: tf_affinity_domain_aud tf_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT tf_affinity_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_affinity_domain tf_affinity_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT tf_affinity_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_audit_message tf_audit_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT tf_audit_message_pkey PRIMARY KEY (id);


--
-- Name: tf_document_aud tf_document_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT tf_document_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document tf_document_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT tf_document_pkey PRIMARY KEY (id);


--
-- Name: tf_document_sections_aud tf_document_sections_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT tf_document_sections_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_document_sections tf_document_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT tf_document_sections_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_aud tf_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT tf_domain_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_domain tf_domain_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT tf_domain_pkey PRIMARY KEY (id);


--
-- Name: tf_domain_profile_aud tf_domain_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT tf_domain_profile_aud_pkey PRIMARY KEY (rev, domain_id, integration_profile_id);


--
-- Name: tf_domain_profile tf_domain_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT tf_domain_profile_pkey PRIMARY KEY (domain_id, integration_profile_id);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud tf_hl7_message_profile_affinity_domain_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT tf_hl7_message_profile_affinity_domain_aud_pkey PRIMARY KEY (rev, hl7_message_profile_id, affinity_domain_id);


--
-- Name: tf_hl7_message_profile_aud tf_hl7_message_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT tf_hl7_message_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_hl7_message_profile tf_hl7_message_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT tf_hl7_message_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_aud tf_integration_profile_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT tf_integration_profile_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option_aud tf_integration_profile_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT tf_integration_profile_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_option tf_integration_profile_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT tf_integration_profile_option_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile tf_integration_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT tf_integration_profile_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_status_type_aud tf_integration_profile_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT tf_integration_profile_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_status_type tf_integration_profile_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT tf_integration_profile_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_integration_profile_type_aud tf_integration_profile_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT tf_integration_profile_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_integration_profile_type_link_aud tf_integration_profile_type_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT tf_integration_profile_type_link_aud_pkey PRIMARY KEY (rev, integration_profile_id, integration_profile_type_id);


--
-- Name: tf_integration_profile_type tf_integration_profile_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT tf_integration_profile_type_pkey PRIMARY KEY (id);


--
-- Name: tf_profile_link_aud tf_profile_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT tf_profile_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_profile_link tf_profile_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT tf_profile_link_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_criterion tf_rule_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT tf_rule_criterion_pkey PRIMARY KEY (id);


--
-- Name: tf_rule_list_criterion tf_rule_list_criterion_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT tf_rule_list_criterion_pkey PRIMARY KEY (tf_rule_list_criterion_id);


--
-- Name: tf_rule tf_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT tf_rule_pkey PRIMARY KEY (id);


--
-- Name: tf_standard_aud tf_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT tf_standard_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_standard tf_standard_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT tf_standard_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_aud tf_transaction_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT tf_transaction_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link_aud tf_transaction_link_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT tf_transaction_link_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_link tf_transaction_link_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT tf_transaction_link_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_option_type_aud tf_transaction_option_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT tf_transaction_option_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_option_type tf_transaction_option_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT tf_transaction_option_type_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction tf_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT tf_transaction_pkey PRIMARY KEY (id);


--
-- Name: tf_transaction_standard_aud tf_transaction_standard_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT tf_transaction_standard_aud_pkey PRIMARY KEY (rev, transaction_id, standard_id);


--
-- Name: tf_transaction_status_type_aud tf_transaction_status_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT tf_transaction_status_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_transaction_status_type tf_transaction_status_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT tf_transaction_status_type_pkey PRIMARY KEY (id);


--
-- Name: tf_ws_transaction_usage_aud tf_ws_transaction_usage_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT tf_ws_transaction_usage_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tf_ws_transaction_usage tf_ws_transaction_usage_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT tf_ws_transaction_usage_pkey PRIMARY KEY (id);


--
-- Name: tm_abstract_configuration tm_abstract_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_abstract_configuration
    ADD CONSTRAINT tm_abstract_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_annotation tm_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_annotation
    ADD CONSTRAINT tm_annotation_pkey PRIMARY KEY (id);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes_aud tm_conf_mapping_w_aipo_w_conftypes_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes_aud
    ADD CONSTRAINT tm_conf_mapping_w_aipo_w_conftypes_aud_pkey PRIMARY KEY (rev, tm_conf_mapped_id, tm_conftypes_id);


--
-- Name: tm_configuration_mapped_with_aipo_aud tm_configuration_mapped_with_aipo_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_mapped_with_aipo_aud
    ADD CONSTRAINT tm_configuration_mapped_with_aipo_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_configuration_mapped_with_aipo tm_configuration_mapped_with_aipo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_mapped_with_aipo
    ADD CONSTRAINT tm_configuration_mapped_with_aipo_pkey PRIMARY KEY (id);


--
-- Name: tm_configuration tm_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT tm_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_configuration_type_aud tm_configuration_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_type_aud
    ADD CONSTRAINT tm_configuration_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_configuration_type tm_configuration_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_type
    ADD CONSTRAINT tm_configuration_type_pkey PRIMARY KEY (id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class_aud tm_conftype_w_ports_wstype_and_sop_class_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class_aud
    ADD CONSTRAINT tm_conftype_w_ports_wstype_and_sop_class_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class tm_conftype_w_ports_wstype_and_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT tm_conftype_w_ports_wstype_and_sop_class_pkey PRIMARY KEY (id);


--
-- Name: tm_connectathon_participant tm_connectathon_participant_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant
    ADD CONSTRAINT tm_connectathon_participant_pkey PRIMARY KEY (id);


--
-- Name: tm_connectathon_participant_status tm_connectathon_participant_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant_status
    ADD CONSTRAINT tm_connectathon_participant_status_pkey PRIMARY KEY (id);


--
-- Name: tm_contextual_information_aud tm_contextual_information_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information_aud
    ADD CONSTRAINT tm_contextual_information_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_contextual_information_instance tm_contextual_information_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information_instance
    ADD CONSTRAINT tm_contextual_information_instance_pkey PRIMARY KEY (id);


--
-- Name: tm_contextual_information tm_contextual_information_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information
    ADD CONSTRAINT tm_contextual_information_pkey PRIMARY KEY (id);


--
-- Name: tm_data_type tm_data_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_data_type
    ADD CONSTRAINT tm_data_type_pkey PRIMARY KEY (id);


--
-- Name: tm_demonstration tm_demonstration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration
    ADD CONSTRAINT tm_demonstration_pkey PRIMARY KEY (id);


--
-- Name: tm_demonstration_system_in_session tm_demonstration_system_in_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration_system_in_session
    ADD CONSTRAINT tm_demonstration_system_in_session_pkey PRIMARY KEY (demonstration_id, system_in_session_id);


--
-- Name: tm_dicom_scp_configuration tm_dicom_scp_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scp_configuration
    ADD CONSTRAINT tm_dicom_scp_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_dicom_scu_configuration tm_dicom_scu_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scu_configuration
    ADD CONSTRAINT tm_dicom_scu_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_generic_oids_institution_for_session tm_generic_oids_institution_for_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_institution_for_session
    ADD CONSTRAINT tm_generic_oids_institution_for_session_pkey PRIMARY KEY (id);


--
-- Name: tm_generic_oids_ip_config_param_for_session tm_generic_oids_ip_config_param_for_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_ip_config_param_for_session
    ADD CONSTRAINT tm_generic_oids_ip_config_param_for_session_pkey PRIMARY KEY (id);


--
-- Name: tm_hl7_initiator_configuration tm_hl7_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_initiator_configuration
    ADD CONSTRAINT tm_hl7_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_hl7_responder_configuration tm_hl7_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_responder_configuration
    ADD CONSTRAINT tm_hl7_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_hl7_v3_initiator_configuration tm_hl7_v3_initiator_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_initiator_configuration
    ADD CONSTRAINT tm_hl7_v3_initiator_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_hl7_v3_responder_configuration tm_hl7_v3_responder_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_responder_configuration
    ADD CONSTRAINT tm_hl7_v3_responder_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_home tm_home_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_home
    ADD CONSTRAINT tm_home_pkey PRIMARY KEY (id);


--
-- Name: tm_host tm_host_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_host
    ADD CONSTRAINT tm_host_pkey PRIMARY KEY (id);


--
-- Name: tm_institution_system tm_institution_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_institution_system
    ADD CONSTRAINT tm_institution_system_pkey PRIMARY KEY (id);


--
-- Name: tm_invoice tm_invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT tm_invoice_pkey PRIMARY KEY (id);


--
-- Name: tm_jira_issues_to_test_aud tm_jira_issues_to_test_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_jira_issues_to_test_aud
    ADD CONSTRAINT tm_jira_issues_to_test_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_jira_issues_to_test tm_jira_issues_to_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_jira_issues_to_test
    ADD CONSTRAINT tm_jira_issues_to_test_pkey PRIMARY KEY (id);


--
-- Name: tm_meta_test_aud tm_meta_test_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_aud
    ADD CONSTRAINT tm_meta_test_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_meta_test tm_meta_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test
    ADD CONSTRAINT tm_meta_test_pkey PRIMARY KEY (id);


--
-- Name: tm_meta_test_test_roles_aud tm_meta_test_test_roles_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles_aud
    ADD CONSTRAINT tm_meta_test_test_roles_aud_pkey PRIMARY KEY (rev, meta_test_id, test_roles_id);


--
-- Name: tm_meta_test_test_roles tm_meta_test_test_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles
    ADD CONSTRAINT tm_meta_test_test_roles_pkey PRIMARY KEY (test_roles_id);


--
-- Name: tm_monitor_in_session tm_monitor_in_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_in_session
    ADD CONSTRAINT tm_monitor_in_session_pkey PRIMARY KEY (id);


--
-- Name: tm_monitor_test tm_monitor_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_test
    ADD CONSTRAINT tm_monitor_test_pkey PRIMARY KEY (monitor_id, test_id);


--
-- Name: tm_network_config_for_session tm_network_config_for_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_network_config_for_session
    ADD CONSTRAINT tm_network_config_for_session_pkey PRIMARY KEY (id);


--
-- Name: tm_object_attribute_aud tm_object_attribute_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_attribute_aud
    ADD CONSTRAINT tm_object_attribute_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_attribute tm_object_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_attribute
    ADD CONSTRAINT tm_object_attribute_pkey PRIMARY KEY (id);


--
-- Name: tm_object_creator_aud tm_object_creator_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator_aud
    ADD CONSTRAINT tm_object_creator_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_creator tm_object_creator_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator
    ADD CONSTRAINT tm_object_creator_pkey PRIMARY KEY (id);


--
-- Name: tm_object_file_aud tm_object_file_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_aud
    ADD CONSTRAINT tm_object_file_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_file tm_object_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file
    ADD CONSTRAINT tm_object_file_pkey PRIMARY KEY (id);


--
-- Name: tm_object_file_type_aud tm_object_file_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_type_aud
    ADD CONSTRAINT tm_object_file_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_file_type tm_object_file_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_type
    ADD CONSTRAINT tm_object_file_type_pkey PRIMARY KEY (id);


--
-- Name: tm_object_instance_annotation tm_object_instance_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_annotation
    ADD CONSTRAINT tm_object_instance_annotation_pkey PRIMARY KEY (id);


--
-- Name: tm_object_instance_attribute tm_object_instance_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_attribute
    ADD CONSTRAINT tm_object_instance_attribute_pkey PRIMARY KEY (id);


--
-- Name: tm_object_instance_file tm_object_instance_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_file
    ADD CONSTRAINT tm_object_instance_file_pkey PRIMARY KEY (id);


--
-- Name: tm_object_instance tm_object_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance
    ADD CONSTRAINT tm_object_instance_pkey PRIMARY KEY (id);


--
-- Name: tm_object_instance_validation_aud tm_object_instance_validation_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_validation_aud
    ADD CONSTRAINT tm_object_instance_validation_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_instance_validation tm_object_instance_validation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_validation
    ADD CONSTRAINT tm_object_instance_validation_pkey PRIMARY KEY (id);


--
-- Name: tm_object_reader_aud tm_object_reader_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader_aud
    ADD CONSTRAINT tm_object_reader_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_reader tm_object_reader_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader
    ADD CONSTRAINT tm_object_reader_pkey PRIMARY KEY (id);


--
-- Name: tm_object_type_aud tm_object_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_aud
    ADD CONSTRAINT tm_object_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_type tm_object_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type
    ADD CONSTRAINT tm_object_type_pkey PRIMARY KEY (id);


--
-- Name: tm_object_type_status_aud tm_object_type_status_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_status_aud
    ADD CONSTRAINT tm_object_type_status_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_object_type_status tm_object_type_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_status
    ADD CONSTRAINT tm_object_type_status_pkey PRIMARY KEY (id);


--
-- Name: tm_oid_institution tm_oid_institution_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_institution
    ADD CONSTRAINT tm_oid_institution_pkey PRIMARY KEY (id);


--
-- Name: tm_oid_requirement tm_oid_requirement_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_requirement
    ADD CONSTRAINT tm_oid_requirement_pkey PRIMARY KEY (id);


--
-- Name: tm_oid_root_definition_label tm_oid_root_definition_label_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_root_definition_label
    ADD CONSTRAINT tm_oid_root_definition_label_pkey PRIMARY KEY (id);


--
-- Name: tm_oid_root_definition tm_oid_root_definition_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_root_definition
    ADD CONSTRAINT tm_oid_root_definition_pkey PRIMARY KEY (id);


--
-- Name: tm_oid_system_assignment tm_oid_system_assignment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_system_assignment
    ADD CONSTRAINT tm_oid_system_assignment_pkey PRIMARY KEY (id);


--
-- Name: tm_oids_ip_config_param_for_session_for_hl7 tm_oids_ip_config_param_for_session_for_hl7_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oids_ip_config_param_for_session_for_hl7
    ADD CONSTRAINT tm_oids_ip_config_param_for_session_for_hl7_pkey PRIMARY KEY (id);


--
-- Name: tm_path_aud tm_path_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path_aud
    ADD CONSTRAINT tm_path_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_path tm_path_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT tm_path_pkey PRIMARY KEY (id);


--
-- Name: tm_proxy_configuration_for_session tm_proxy_configuration_for_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_proxy_configuration_for_session
    ADD CONSTRAINT tm_proxy_configuration_for_session_pkey PRIMARY KEY (id);


--
-- Name: tm_raw_configuration tm_raw_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_raw_configuration
    ADD CONSTRAINT tm_raw_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_role_in_test_aud tm_role_in_test_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_aud
    ADD CONSTRAINT tm_role_in_test_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_role_in_test tm_role_in_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test
    ADD CONSTRAINT tm_role_in_test_pkey PRIMARY KEY (id);


--
-- Name: tm_role_in_test_test_participants_aud tm_role_in_test_test_participants_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_test_participants_aud
    ADD CONSTRAINT tm_role_in_test_test_participants_aud_pkey PRIMARY KEY (rev, role_in_test_id, test_participants_id);


--
-- Name: tm_sap_result_tr_comment tm_sap_result_tr_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sap_result_tr_comment
    ADD CONSTRAINT tm_sap_result_tr_comment_pkey PRIMARY KEY (id);


--
-- Name: tm_section tm_section_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_section
    ADD CONSTRAINT tm_section_pkey PRIMARY KEY (id);


--
-- Name: tm_section_type tm_section_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_section_type
    ADD CONSTRAINT tm_section_type_pkey PRIMARY KEY (id);


--
-- Name: tm_sop_class_aud tm_sop_class_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sop_class_aud
    ADD CONSTRAINT tm_sop_class_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_sop_class tm_sop_class_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sop_class
    ADD CONSTRAINT tm_sop_class_pkey PRIMARY KEY (id);


--
-- Name: tm_status_results tm_status_results_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_status_results
    ADD CONSTRAINT tm_status_results_pkey PRIMARY KEY (id);


--
-- Name: tm_step_inst_msg_process_status tm_step_inst_msg_process_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_inst_msg_process_status
    ADD CONSTRAINT tm_step_inst_msg_process_status_pkey PRIMARY KEY (id);


--
-- Name: tm_step_instance_exec_status tm_step_instance_exec_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_exec_status
    ADD CONSTRAINT tm_step_instance_exec_status_pkey PRIMARY KEY (id);


--
-- Name: tm_step_instance_message tm_step_instance_message_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT tm_step_instance_message_pkey PRIMARY KEY (id);


--
-- Name: tm_step_instance_msg_validation tm_step_instance_msg_validation_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_msg_validation
    ADD CONSTRAINT tm_step_instance_msg_validation_pkey PRIMARY KEY (id);


--
-- Name: tm_subtypes_per_system tm_subtypes_per_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_subtypes_per_system
    ADD CONSTRAINT tm_subtypes_per_system_pkey PRIMARY KEY (id);


--
-- Name: tm_syslog_configuration tm_syslog_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_syslog_configuration
    ADD CONSTRAINT tm_syslog_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_system_actor_profiles tm_system_actor_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT tm_system_actor_profiles_pkey PRIMARY KEY (id);


--
-- Name: tm_system_aipo_result_for_a_testing_session tm_system_aipo_result_for_a_testing_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_aipo_result_for_a_testing_session
    ADD CONSTRAINT tm_system_aipo_result_for_a_testing_session_pkey PRIMARY KEY (id);


--
-- Name: tm_system_event tm_system_event_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_event
    ADD CONSTRAINT tm_system_event_pkey PRIMARY KEY (id);


--
-- Name: tm_system_in_session tm_system_in_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT tm_system_in_session_pkey PRIMARY KEY (id);


--
-- Name: tm_system_in_session_status tm_system_in_session_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_status
    ADD CONSTRAINT tm_system_in_session_status_pkey PRIMARY KEY (id);


--
-- Name: tm_system_in_session_user tm_system_in_session_user_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_user
    ADD CONSTRAINT tm_system_in_session_user_pkey PRIMARY KEY (id);


--
-- Name: tm_system tm_system_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system
    ADD CONSTRAINT tm_system_pkey PRIMARY KEY (id);


--
-- Name: tm_test_aud tm_test_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_aud
    ADD CONSTRAINT tm_test_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_description_aud tm_test_description_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_description_aud
    ADD CONSTRAINT tm_test_description_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_description tm_test_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_description
    ADD CONSTRAINT tm_test_description_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_event tm_test_instance_event_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_event
    ADD CONSTRAINT tm_test_instance_event_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_exec_status tm_test_instance_exec_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_exec_status
    ADD CONSTRAINT tm_test_instance_exec_status_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_participants tm_test_instance_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT tm_test_instance_participants_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_participants_status tm_test_instance_participants_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants_status
    ADD CONSTRAINT tm_test_instance_participants_status_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_path_to_log_file tm_test_instance_path_to_log_file_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_path_to_log_file
    ADD CONSTRAINT tm_test_instance_path_to_log_file_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance tm_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance
    ADD CONSTRAINT tm_test_instance_pkey PRIMARY KEY (id);


--
-- Name: tm_test_instance_test_steps_instance tm_test_instance_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_steps_instance
    ADD CONSTRAINT tm_test_instance_test_steps_instance_pkey PRIMARY KEY (test_steps_instance_id);


--
-- Name: tm_test_instance_token tm_test_instance_token_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_token
    ADD CONSTRAINT tm_test_instance_token_pkey PRIMARY KEY (test_instance_identifier, username);


--
-- Name: tm_test_option_aud tm_test_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_option_aud
    ADD CONSTRAINT tm_test_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_option tm_test_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_option
    ADD CONSTRAINT tm_test_option_pkey PRIMARY KEY (id);


--
-- Name: tm_test_participants_aud tm_test_participants_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_participants_aud
    ADD CONSTRAINT tm_test_participants_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_participants tm_test_participants_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_participants
    ADD CONSTRAINT tm_test_participants_pkey PRIMARY KEY (id);


--
-- Name: tm_test_peer_type_aud tm_test_peer_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_peer_type_aud
    ADD CONSTRAINT tm_test_peer_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_peer_type tm_test_peer_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_peer_type
    ADD CONSTRAINT tm_test_peer_type_pkey PRIMARY KEY (id);


--
-- Name: tm_test tm_test_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test
    ADD CONSTRAINT tm_test_pkey PRIMARY KEY (id);


--
-- Name: tm_test_roles_aud tm_test_roles_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles_aud
    ADD CONSTRAINT tm_test_roles_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_roles tm_test_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles
    ADD CONSTRAINT tm_test_roles_pkey PRIMARY KEY (id);


--
-- Name: tm_test_status_aud tm_test_status_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_status_aud
    ADD CONSTRAINT tm_test_status_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_status tm_test_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_status
    ADD CONSTRAINT tm_test_status_pkey PRIMARY KEY (id);


--
-- Name: tm_test_step_message_profile tm_test_step_message_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_step_message_profile
    ADD CONSTRAINT tm_test_step_message_profile_pkey PRIMARY KEY (id);


--
-- Name: tm_test_steps_aud tm_test_steps_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_aud
    ADD CONSTRAINT tm_test_steps_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_steps_data tm_test_steps_data_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_data
    ADD CONSTRAINT tm_test_steps_data_pkey PRIMARY KEY (id);


--
-- Name: tm_test_steps_input_ci_aud tm_test_steps_input_ci_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_input_ci_aud
    ADD CONSTRAINT tm_test_steps_input_ci_aud_pkey PRIMARY KEY (rev, test_steps_id, contextual_information_id);


--
-- Name: tm_test_steps_instance_output_ci_instance tm_test_steps_instance_output_ci_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_output_ci_instance
    ADD CONSTRAINT tm_test_steps_instance_output_ci_instance_pkey PRIMARY KEY (contextual_information_instance_id);


--
-- Name: tm_test_steps_instance tm_test_steps_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT tm_test_steps_instance_pkey PRIMARY KEY (id);


--
-- Name: tm_test_steps_instance_status tm_test_steps_instance_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_status
    ADD CONSTRAINT tm_test_steps_instance_status_pkey PRIMARY KEY (id);


--
-- Name: tm_test_steps_option_aud tm_test_steps_option_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_option_aud
    ADD CONSTRAINT tm_test_steps_option_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_steps_option tm_test_steps_option_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_option
    ADD CONSTRAINT tm_test_steps_option_pkey PRIMARY KEY (id);


--
-- Name: tm_test_steps_output_ci_aud tm_test_steps_output_ci_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci_aud
    ADD CONSTRAINT tm_test_steps_output_ci_aud_pkey PRIMARY KEY (rev, test_steps_id, contextual_information_id);


--
-- Name: tm_test_steps_output_ci tm_test_steps_output_ci_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci
    ADD CONSTRAINT tm_test_steps_output_ci_pkey PRIMARY KEY (contextual_information_id);


--
-- Name: tm_test_steps tm_test_steps_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT tm_test_steps_pkey PRIMARY KEY (id);


--
-- Name: tm_test_test_description_aud tm_test_test_description_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description_aud
    ADD CONSTRAINT tm_test_test_description_aud_pkey PRIMARY KEY (rev, test_id, test_description_id);


--
-- Name: tm_test_test_description tm_test_test_description_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description
    ADD CONSTRAINT tm_test_test_description_pkey PRIMARY KEY (test_description_id);


--
-- Name: tm_test_test_steps_aud tm_test_test_steps_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps_aud
    ADD CONSTRAINT tm_test_test_steps_aud_pkey PRIMARY KEY (rev, test_id, test_steps_id);


--
-- Name: tm_test_test_steps tm_test_test_steps_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps
    ADD CONSTRAINT tm_test_test_steps_pkey PRIMARY KEY (test_steps_id);


--
-- Name: tm_test_type_aud tm_test_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_type_aud
    ADD CONSTRAINT tm_test_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_test_type tm_test_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_type
    ADD CONSTRAINT tm_test_type_pkey PRIMARY KEY (id);


--
-- Name: tm_testing_depth tm_testing_depth_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_depth
    ADD CONSTRAINT tm_testing_depth_pkey PRIMARY KEY (id);


--
-- Name: tm_testing_session tm_testing_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session
    ADD CONSTRAINT tm_testing_session_pkey PRIMARY KEY (id);


--
-- Name: tm_transport_layer_for_config_aud tm_transport_layer_for_config_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_transport_layer_for_config_aud
    ADD CONSTRAINT tm_transport_layer_for_config_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_transport_layer_for_config tm_transport_layer_for_config_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_transport_layer_for_config
    ADD CONSTRAINT tm_transport_layer_for_config_pkey PRIMARY KEY (id);


--
-- Name: tm_user_comment tm_user_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_comment
    ADD CONSTRAINT tm_user_comment_pkey PRIMARY KEY (id);


--
-- Name: tm_user_photo tm_user_photo_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_photo
    ADD CONSTRAINT tm_user_photo_pkey PRIMARY KEY (id);


--
-- Name: tm_user_preferences tm_user_preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_preferences
    ADD CONSTRAINT tm_user_preferences_pkey PRIMARY KEY (id);


--
-- Name: tm_web_service_configuration tm_web_service_configuration_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_configuration
    ADD CONSTRAINT tm_web_service_configuration_pkey PRIMARY KEY (id);


--
-- Name: tm_web_service_type_aud tm_web_service_type_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_type_aud
    ADD CONSTRAINT tm_web_service_type_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: tm_web_service_type tm_web_service_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_type
    ADD CONSTRAINT tm_web_service_type_pkey PRIMARY KEY (id);


--
-- Name: tm_hl7_v3_initiator_configuration uk_10buhf0s8g3nd52awmy9reqo8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_initiator_configuration
    ADD CONSTRAINT uk_10buhf0s8g3nd52awmy9reqo8 UNIQUE (id);


--
-- Name: tm_monitor_test uk_1hn1p5frtj81roa7jtx1b1bvx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_test
    ADD CONSTRAINT uk_1hn1p5frtj81roa7jtx1b1bvx UNIQUE (monitor_id, test_id);


--
-- Name: tm_oid_system_assignment uk_1vo5arg5con3rp0hyrytj2k2d; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_system_assignment
    ADD CONSTRAINT uk_1vo5arg5con3rp0hyrytj2k2d UNIQUE (oid_value);


--
-- Name: sys_dicom_documents_for_systems uk_28t1c79mdb8clmu3ilsb7bkuu; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_dicom_documents_for_systems
    ADD CONSTRAINT uk_28t1c79mdb8clmu3ilsb7bkuu UNIQUE (system_id, path_linking_a_document_id);


--
-- Name: tm_data_type uk_2cnamhxywvpywxgb2ieo825ow; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_data_type
    ADD CONSTRAINT uk_2cnamhxywvpywxgb2ieo825ow UNIQUE (keyword);


--
-- Name: usr_role uk_2mhium33qhmvjj0bup1ai22it; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_role
    ADD CONSTRAINT uk_2mhium33qhmvjj0bup1ai22it UNIQUE (name);


--
-- Name: tm_system_aipo_result_for_a_testing_session uk_2uns6kpga7ppk1fqley584ig2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_aipo_result_for_a_testing_session
    ADD CONSTRAINT uk_2uns6kpga7ppk1fqley584ig2 UNIQUE (system_actor_profile_id, testing_session_id);


--
-- Name: pr_crawler_changed_integration_statements uk_2voggjxoj0tlcovu29st15arr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_changed_integration_statements
    ADD CONSTRAINT uk_2voggjxoj0tlcovu29st15arr UNIQUE (system_id, crawler_report_id);


--
-- Name: tm_test_steps_test_steps_data uk_2vyce7ta80j2pwqf2vi7vcxte; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_test_steps_data
    ADD CONSTRAINT uk_2vyce7ta80j2pwqf2vi7vcxte UNIQUE (test_steps_id, test_steps_data_id);


--
-- Name: tm_object_file_type uk_3ib4ell78yk3j6j2wah4cob3f; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_type
    ADD CONSTRAINT uk_3ib4ell78yk3j6j2wah4cob3f UNIQUE (keyword);


--
-- Name: tm_oid_root_definition_label uk_3quk0wl72puff27mpk4aqcqei; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_root_definition_label
    ADD CONSTRAINT uk_3quk0wl72puff27mpk4aqcqei UNIQUE (label);


--
-- Name: tm_test_steps_option uk_3ty9i0xqmr3rqb3nxfa4mbn0l; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_option
    ADD CONSTRAINT uk_3ty9i0xqmr3rqb3nxfa4mbn0l UNIQUE (keyword);


--
-- Name: usr_user_role uk_40tx3h9q3qxciis823tkciy31; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT uk_40tx3h9q3qxciis823tkciy31 UNIQUE (user_id, role_id);


--
-- Name: tf_domain uk_436tct1jl8811q2xgd8bjth9q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain
    ADD CONSTRAINT uk_436tct1jl8811q2xgd8bjth9q UNIQUE (keyword);


--
-- Name: tm_subtypes_per_system uk_4jmwjcgdypwx6o98fbh57ygok; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_subtypes_per_system
    ADD CONSTRAINT uk_4jmwjcgdypwx6o98fbh57ygok UNIQUE (system_id, system_subtypes_per_system_type_id);


--
-- Name: tm_oid_requirement_aipo uk_4kq9fsdkrtitht5u00mq5g93x; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_requirement_aipo
    ADD CONSTRAINT uk_4kq9fsdkrtitht5u00mq5g93x UNIQUE (aipo_id);


--
-- Name: tf_integration_profile uk_4ojy2hqgxhoytlcrjkp9h9lr3; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_4ojy2hqgxhoytlcrjkp9h9lr3 UNIQUE (name);


--
-- Name: usr_institution_type uk_4x2rymgrys6gpojsuqcxq2prd; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_type
    ADD CONSTRAINT uk_4x2rymgrys6gpojsuqcxq2prd UNIQUE (type);


--
-- Name: tm_connectathon_participant uk_4x3q251c75oj0p9vf0jwfr3x; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant
    ADD CONSTRAINT uk_4x3q251c75oj0p9vf0jwfr3x UNIQUE (email, testing_session_id);


--
-- Name: tm_test_status uk_50ng6rcuqw7gsnlfwuagdeqk4; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_status
    ADD CONSTRAINT uk_50ng6rcuqw7gsnlfwuagdeqk4 UNIQUE (keyword);


--
-- Name: pr_search_log_systems_found uk_52rsrc6xwgmj26w2rm947w5b9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_log_systems_found
    ADD CONSTRAINT uk_52rsrc6xwgmj26w2rm947w5b9 UNIQUE (system_id, search_report_id);


--
-- Name: cmn_application_preference uk_535ry4wkukk8xivd9vqvu06hp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.cmn_application_preference
    ADD CONSTRAINT uk_535ry4wkukk8xivd9vqvu06hp UNIQUE (preference_name);


--
-- Name: tm_test_steps_instance_input_ci_instance uk_59rxha39jxmugs9x3jyaj2djn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_input_ci_instance
    ADD CONSTRAINT uk_59rxha39jxmugs9x3jyaj2djn UNIQUE (test_steps_instance_id, contextual_information_instance_id);


--
-- Name: tm_test_steps_input_ci uk_5rdivjd19b0n5nqwy3bkb7u9c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_input_ci
    ADD CONSTRAINT uk_5rdivjd19b0n5nqwy3bkb7u9c UNIQUE (test_steps_id, contextual_information_id);


--
-- Name: tm_meta_test uk_5ugit0h6hjiskoajpn6i0arvg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test
    ADD CONSTRAINT uk_5ugit0h6hjiskoajpn6i0arvg UNIQUE (keyword);


--
-- Name: tm_system_in_session_status uk_5vy8n9xfqxajbmgvmm584227p; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_status
    ADD CONSTRAINT uk_5vy8n9xfqxajbmgvmm584227p UNIQUE (keyword);


--
-- Name: tm_generic_oids_ip_config_param_for_session uk_63nd7mueot21l0ar33r2fhjp5; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_ip_config_param_for_session
    ADD CONSTRAINT uk_63nd7mueot21l0ar33r2fhjp5 UNIQUE (oid_root_value, testing_session_id);


--
-- Name: tm_user_preferences uk_6e83kvu2r93s4hafcs6gcwmah; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_preferences
    ADD CONSTRAINT uk_6e83kvu2r93s4hafcs6gcwmah UNIQUE (user_id);


--
-- Name: tf_transaction uk_6nt35dm69gbrrj0aegkkqalr2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT uk_6nt35dm69gbrrj0aegkkqalr2 UNIQUE (keyword);


--
-- Name: tm_object_type uk_6oykhsfjjc39awfljdf6fxrd9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type
    ADD CONSTRAINT uk_6oykhsfjjc39awfljdf6fxrd9 UNIQUE (keyword);


--
-- Name: pr_search_criteria_per_report uk_6pk7s91q24wh36ggpq3lmufp7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_criteria_per_report
    ADD CONSTRAINT uk_6pk7s91q24wh36ggpq3lmufp7 UNIQUE (search_log_criterion_id, search_log_report_id);


--
-- Name: tm_test_type uk_6q0ah7pcfjbqk2hr0a3aoa2t6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_type
    ADD CONSTRAINT uk_6q0ah7pcfjbqk2hr0a3aoa2t6 UNIQUE (keyword);


--
-- Name: tf_transaction_status_type uk_712597f0v980ai4l7v0cg1rrh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_712597f0v980ai4l7v0cg1rrh UNIQUE (keyword);


--
-- Name: tf_actor_default_ports uk_75d73bul1n175ugk6o8v2xgsn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_default_ports
    ADD CONSTRAINT uk_75d73bul1n175ugk6o8v2xgsn UNIQUE (actor_id, config_name, type_of_config);


--
-- Name: tm_syslog_configuration uk_7940buif4fubyah9eb9g2ua7h; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_syslog_configuration
    ADD CONSTRAINT uk_7940buif4fubyah9eb9g2ua7h UNIQUE (id);


--
-- Name: tm_test_peer_type uk_7bj58xq0vovkqdxe37lgqfcqm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_peer_type
    ADD CONSTRAINT uk_7bj58xq0vovkqdxe37lgqfcqm UNIQUE (keyword);


--
-- Name: tm_test_steps_test_steps_data uk_7bla4jq9vrgr2ke17oq45ku5d; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_test_steps_data
    ADD CONSTRAINT uk_7bla4jq9vrgr2ke17oq45ku5d UNIQUE (test_steps_data_id);


--
-- Name: pr_intro uk_7g5gypis7rsyevuml55t9hxvi; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_intro
    ADD CONSTRAINT uk_7g5gypis7rsyevuml55t9hxvi UNIQUE (language);


--
-- Name: tm_jira_issues_to_test uk_7ix8bprdgihbs5esk9osv6phc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_jira_issues_to_test
    ADD CONSTRAINT uk_7ix8bprdgihbs5esk9osv6phc UNIQUE (jira_key, test_id);


--
-- Name: tm_test_participants uk_7k7xjvok2fkhv090gf6k8n6ua; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_participants
    ADD CONSTRAINT uk_7k7xjvok2fkhv090gf6k8n6ua UNIQUE (actor_integration_profile_option_id, is_tested);


--
-- Name: tm_configuration_type uk_80ce4xdl64cyo038ls8pfsdka; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_type
    ADD CONSTRAINT uk_80ce4xdl64cyo038ls8pfsdka UNIQUE (class_name);


--
-- Name: tf_rule_list_criterion uk_81shykw7emr3q41anx9ksqdb6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT uk_81shykw7emr3q41anx9ksqdb6 UNIQUE (aipocriterions_id);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes uk_81wqciwyf0gnhx01h4jl9ix9m; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes
    ADD CONSTRAINT uk_81wqciwyf0gnhx01h4jl9ix9m UNIQUE (tm_conftypes_id);


--
-- Name: tf_standard uk_862df7ewvip5ajs8fpnnx9hm6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard
    ADD CONSTRAINT uk_862df7ewvip5ajs8fpnnx9hm6 UNIQUE (keyword);


--
-- Name: sys_system_type uk_86l8p9bukdmfhqqt9nln4o18q; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_type
    ADD CONSTRAINT uk_86l8p9bukdmfhqqt9nln4o18q UNIQUE (keyword);


--
-- Name: tf_actor_integration_profile_option uk_8b4tb6bcp3eh7mtsucokgh7fx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT uk_8b4tb6bcp3eh7mtsucokgh7fx UNIQUE (actor_integration_profile_id, integration_profile_option_id);


--
-- Name: tm_transport_layer_for_config uk_8njjtyf6jc3xjhji9diq0ab8t; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_transport_layer_for_config
    ADD CONSTRAINT uk_8njjtyf6jc3xjhji9diq0ab8t UNIQUE (keyword);


--
-- Name: tf_transaction_status_type uk_8ojkw0me2bv4tsm5vyrmwb6gu; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type
    ADD CONSTRAINT uk_8ojkw0me2bv4tsm5vyrmwb6gu UNIQUE (name);


--
-- Name: tm_path uk_8ufv5mgjsoifbtbq28b64r87s; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path
    ADD CONSTRAINT uk_8ufv5mgjsoifbtbq28b64r87s UNIQUE (keyword);


--
-- Name: tf_transaction_link uk_97iyim1a054rgjkpa32j9ksxb; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT uk_97iyim1a054rgjkpa32j9ksxb UNIQUE (from_actor_id, to_actor_id, transaction_id);


--
-- Name: tf_affinity_domain uk_9lriehprhbeo91cxqn7rqwjsy; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain
    ADD CONSTRAINT uk_9lriehprhbeo91cxqn7rqwjsy UNIQUE (keyword);


--
-- Name: tf_transaction_standard uk_9xw54omnofi1bn7px13kr6jyk; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT uk_9xw54omnofi1bn7px13kr6jyk UNIQUE (standard_id, transaction_id);


--
-- Name: tm_generic_oids_institution_for_session uk_9yvucm5rkx6qynt97lv8th990; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_institution_for_session
    ADD CONSTRAINT uk_9yvucm5rkx6qynt97lv8th990 UNIQUE (testing_session_id);


--
-- Name: usr_gazelle_language uk_aca60tc5yxx6qnicu68m40cxa; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT uk_aca60tc5yxx6qnicu68m40cxa UNIQUE (description);


--
-- Name: tm_demonstration uk_ajsabrf510pmh8fxks3rqwa8l; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration
    ADD CONSTRAINT uk_ajsabrf510pmh8fxks3rqwa8l UNIQUE (name);


--
-- Name: usr_institution uk_an3krxejvje497n5bqhw3pyew; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT uk_an3krxejvje497n5bqhw3pyew UNIQUE (name);


--
-- Name: tm_dicom_scp_configuration uk_anse5k9jxcnd3r5if3utd0ia0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scp_configuration
    ADD CONSTRAINT uk_anse5k9jxcnd3r5if3utd0ia0 UNIQUE (id);


--
-- Name: tm_generic_oids_ip_config_param_for_session uk_arx4mnq8dccmdtn9m4187dr3j; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_ip_config_param_for_session
    ADD CONSTRAINT uk_arx4mnq8dccmdtn9m4187dr3j UNIQUE (profile_for_id, testing_session_id);


--
-- Name: tm_object_instance_validation uk_b59w2rthu7pymkce0yvpbfjqj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_validation
    ADD CONSTRAINT uk_b59w2rthu7pymkce0yvpbfjqj UNIQUE (description);


--
-- Name: sys_hl7_documents_for_systems uk_b62n69tgo7jmvs85yon701g5p; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_hl7_documents_for_systems
    ADD CONSTRAINT uk_b62n69tgo7jmvs85yon701g5p UNIQUE (system_id, path_linking_a_document_id);


--
-- Name: tf_actor_integration_profile uk_b6ejd87o8v27xinqw27nn1hss; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT uk_b6ejd87o8v27xinqw27nn1hss UNIQUE (actor_id, integration_profile_id);


--
-- Name: tm_web_service_configuration uk_ba2hfrlmlwirsk0uoyj2sjqfx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_configuration
    ADD CONSTRAINT uk_ba2hfrlmlwirsk0uoyj2sjqfx UNIQUE (id);


--
-- Name: tm_step_instance_exec_status uk_c5puybaipxbybbe67le7axrvg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_exec_status
    ADD CONSTRAINT uk_c5puybaipxbybbe67le7axrvg UNIQUE (key);


--
-- Name: tm_testing_depth uk_cv6fl2rldk3kq3w48w7j06oog; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_depth
    ADD CONSTRAINT uk_cv6fl2rldk3kq3w48w7j06oog UNIQUE (keyword);


--
-- Name: usr_institution_address uk_cwvldv0unpwmi4m472u9ai8ga; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT uk_cwvldv0unpwmi4m472u9ai8ga UNIQUE (institution_id, address_id);


--
-- Name: usr_gazelle_language uk_cxj0vux0flmxfcwvqwuk893w8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT uk_cxj0vux0flmxfcwvqwuk893w8 UNIQUE (keyword);


--
-- Name: tf_integration_profile_type_link uk_d8f4vv4juh946m3kbs3o0w7p2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT uk_d8f4vv4juh946m3kbs3o0w7p2 UNIQUE (integration_profile_id, integration_profile_type_id);


--
-- Name: usr_messages_usr_message_parameters uk_dgnn9iuua4q2tytrlkkhitjf1; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages_usr_message_parameters
    ADD CONSTRAINT uk_dgnn9iuua4q2tytrlkkhitjf1 UNIQUE (messageparameters_id);


--
-- Name: tm_test_instance_test_status uk_dt4ios5pp48n7weh8rxx4n9rf; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_status
    ADD CONSTRAINT uk_dt4ios5pp48n7weh8rxx4n9rf UNIQUE (status_id);


--
-- Name: tm_web_service_type uk_dwst1ggdqkkmtdox3rumvf2jr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_type
    ADD CONSTRAINT uk_dwst1ggdqkkmtdox3rumvf2jr UNIQUE (profile_id);


--
-- Name: validation_status uk_e2mry5ar3m094h9uucxjlyd17; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_status
    ADD CONSTRAINT uk_e2mry5ar3m094h9uucxjlyd17 UNIQUE (key);


--
-- Name: tm_oids_ip_config_param_for_session_for_hl7 uk_elms3go7hjd6st7hd3shhp14b; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oids_ip_config_param_for_session_for_hl7
    ADD CONSTRAINT uk_elms3go7hjd6st7hd3shhp14b UNIQUE (testing_session_id);


--
-- Name: usr_persons_functions uk_en9ho8ougnovio1aktpuc7jj4; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT uk_en9ho8ougnovio1aktpuc7jj4 UNIQUE (person_function_id, person_id);


--
-- Name: tm_oid_system_assignment uk_eqafflltc0vi5wuspeeb58bmn; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_system_assignment
    ADD CONSTRAINT uk_eqafflltc0vi5wuspeeb58bmn UNIQUE (system_in_session_id, oid_requirement_id);


--
-- Name: tm_institution_system uk_f8opps24svl0h6lf93vngtuhy; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_institution_system
    ADD CONSTRAINT uk_f8opps24svl0h6lf93vngtuhy UNIQUE (system_id, institution_id);


--
-- Name: tm_test_instance_tm_test_steps_data uk_fcvt06p9ogywqpi7wv390hdd4; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_tm_test_steps_data
    ADD CONSTRAINT uk_fcvt06p9ogywqpi7wv390hdd4 UNIQUE (teststepsdatalist_id);


--
-- Name: validation_service uk_fkv6lchsjlx740y0f34kgt4ng; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_service
    ADD CONSTRAINT uk_fkv6lchsjlx740y0f34kgt4ng UNIQUE (key);


--
-- Name: tm_object_file uk_fpb1spo4lb16jwvv2om0lv4e8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file
    ADD CONSTRAINT uk_fpb1spo4lb16jwvv2om0lv4e8 UNIQUE (description, object_id, type_id, uploader);


--
-- Name: tf_actor uk_fpb6vpa9bnw6cjsb1pucuuivw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor
    ADD CONSTRAINT uk_fpb6vpa9bnw6cjsb1pucuuivw UNIQUE (keyword);


--
-- Name: tf_transaction_option_type uk_fv74hod7rl73bhyerub82q67c; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_fv74hod7rl73bhyerub82q67c UNIQUE (name);


--
-- Name: usr_users uk_g0jloiasku8a7gat4lu7866r6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT uk_g0jloiasku8a7gat4lu7866r6 UNIQUE (email);


--
-- Name: tm_object_type_status uk_g1w5apucxfq82xcvs0pj4l0df; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_status
    ADD CONSTRAINT uk_g1w5apucxfq82xcvs0pj4l0df UNIQUE (keyword);


--
-- Name: sys_table_session uk_gitay4shg3ajgue6udvvw4xqf; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_table_session
    ADD CONSTRAINT uk_gitay4shg3ajgue6udvvw4xqf UNIQUE (table_keyword);


--
-- Name: tm_test_instance_exec_status uk_gjrtpg6gttlmrcotbpebkcf4v; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_exec_status
    ADD CONSTRAINT uk_gjrtpg6gttlmrcotbpebkcf4v UNIQUE (key);


--
-- Name: tm_test_user_comment uk_gq3etdcefp2fj86m8gag3x7xd; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_user_comment
    ADD CONSTRAINT uk_gq3etdcefp2fj86m8gag3x7xd UNIQUE (user_comment_id);


--
-- Name: tf_document_sections uk_gqqt5kk1xa5npiaptbwwdu9bt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT uk_gqqt5kk1xa5npiaptbwwdu9bt UNIQUE (section, document_id);


--
-- Name: tf_integration_profile_status_type uk_gs2vk7vmy1ggt3p34n1f5da6; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_gs2vk7vmy1ggt3p34n1f5da6 UNIQUE (keyword);


--
-- Name: tm_testing_session_admin uk_gxleq7xkksv2l4l162wu37u2h; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session_admin
    ADD CONSTRAINT uk_gxleq7xkksv2l4l162wu37u2h UNIQUE (testing_session_id, user_id);


--
-- Name: tm_step_inst_msg_process_status uk_gyprxsvlm5if4h2nssk5svomp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_inst_msg_process_status
    ADD CONSTRAINT uk_gyprxsvlm5if4h2nssk5svomp UNIQUE (key);


--
-- Name: tm_step_instance_message uk_h1ssaepfongpr260b2fea8tkm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT uk_h1ssaepfongpr260b2fea8tkm UNIQUE (proxy_msg_id, proxy_type_id, proxy_host_name);


--
-- Name: tm_test_test_steps uk_hf9i4p02djpa5xscdy3rmneuc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps
    ADD CONSTRAINT uk_hf9i4p02djpa5xscdy3rmneuc UNIQUE (test_id, test_steps_id);


--
-- Name: tm_raw_configuration uk_hjk5484nmdehnlukgop7o1sd7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_raw_configuration
    ADD CONSTRAINT uk_hjk5484nmdehnlukgop7o1sd7 UNIQUE (id);


--
-- Name: tm_system uk_hs3ywttp7yc6q6mwg8wjm03qw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system
    ADD CONSTRAINT uk_hs3ywttp7yc6q6mwg8wjm03qw UNIQUE (keyword);


--
-- Name: tm_test_steps_instance_status uk_hsrkj8eco4ucxdlas3t2x7diy; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_status
    ADD CONSTRAINT uk_hsrkj8eco4ucxdlas3t2x7diy UNIQUE (keyword);


--
-- Name: tm_dicom_scu_configuration uk_i8es6i39yycwgntwmxa8f042w; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scu_configuration
    ADD CONSTRAINT uk_i8es6i39yycwgntwmxa8f042w UNIQUE (id);


--
-- Name: tm_monitor_in_session uk_icit9xkh4kbs16pco83ekfs6x; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_in_session
    ADD CONSTRAINT uk_icit9xkh4kbs16pco83ekfs6x UNIQUE (user_id, testing_session_id);


--
-- Name: tm_sop_class uk_ifoqgpxeg97br8yaj8g79nu35; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sop_class
    ADD CONSTRAINT uk_ifoqgpxeg97br8yaj8g79nu35 UNIQUE (keyword);


--
-- Name: tm_status_results uk_ior9tfnc6f6yaxoqrue238b2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_status_results
    ADD CONSTRAINT uk_ior9tfnc6f6yaxoqrue238b2 UNIQUE (keyword);


--
-- Name: tm_demonstration_system_in_session uk_irmi59xhdk6eun3kol3sogx6t; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration_system_in_session
    ADD CONSTRAINT uk_irmi59xhdk6eun3kol3sogx6t UNIQUE (demonstration_id, system_in_session_id);


--
-- Name: usr_person_function uk_j0g40k0is2g0gw7kiilqnh1nr; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person_function
    ADD CONSTRAINT uk_j0g40k0is2g0gw7kiilqnh1nr UNIQUE (keyword);


--
-- Name: tm_test_option uk_j5qq9td6cf9ufae7r0qx726jw; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_option
    ADD CONSTRAINT uk_j5qq9td6cf9ufae7r0qx726jw UNIQUE (keyword);


--
-- Name: tm_testing_depth uk_jebdbhol6320vvx1dcdr4aand; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_depth
    ADD CONSTRAINT uk_jebdbhol6320vvx1dcdr4aand UNIQUE (name);


--
-- Name: tm_hl7_v3_responder_configuration uk_jgxhbd5te98139auc4i2ka4vs; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_responder_configuration
    ADD CONSTRAINT uk_jgxhbd5te98139auc4i2ka4vs UNIQUE (id);


--
-- Name: usr_institution uk_jqjrsonmmlfnnp3ta4aaqu3ut; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT uk_jqjrsonmmlfnnp3ta4aaqu3ut UNIQUE (keyword);


--
-- Name: tf_integration_profile_type uk_jx75w0mlp0ptnjsbiv8576nl0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type
    ADD CONSTRAINT uk_jx75w0mlp0ptnjsbiv8576nl0 UNIQUE (keyword);


--
-- Name: tm_object_reader uk_k0v85ygfvl81vmjbt2nc8g6eb; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader
    ADD CONSTRAINT uk_k0v85ygfvl81vmjbt2nc8g6eb UNIQUE (aipo_id, object_id);


--
-- Name: tf_hl7_message_profile uk_k6ujf0i2mjoaf4w15wk7fq89y; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT uk_k6ujf0i2mjoaf4w15wk7fq89y UNIQUE (profile_oid);


--
-- Name: tm_object_creator uk_kvm154f5tanjhm3yaiint0rdl; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator
    ADD CONSTRAINT uk_kvm154f5tanjhm3yaiint0rdl UNIQUE (aipo_id, object_id);


--
-- Name: tm_test_steps_instance_output_ci_instance uk_l6rhgq95skiwdornands5xr45; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_output_ci_instance
    ADD CONSTRAINT uk_l6rhgq95skiwdornands5xr45 UNIQUE (test_steps_instance_id, contextual_information_instance_id);


--
-- Name: tm_system_events uk_lqcivp5bh2olqqdmy9jah4gor; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_events
    ADD CONSTRAINT uk_lqcivp5bh2olqqdmy9jah4gor UNIQUE (systemevents_id);


--
-- Name: pr_crawler_unreachable_integration_statements uk_lr6gr27b3pf1onfp0jaf63ucx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unreachable_integration_statements
    ADD CONSTRAINT uk_lr6gr27b3pf1onfp0jaf63ucx UNIQUE (system_id, crawler_report_id);


--
-- Name: tf_domain_profile uk_luennoioveseb7edrlppqaum2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT uk_luennoioveseb7edrlppqaum2 UNIQUE (domain_id, integration_profile_id);


--
-- Name: tm_demonstrations_in_testing_sessions uk_lv3evdty6fk3whjwtmqn6ygs7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstrations_in_testing_sessions
    ADD CONSTRAINT uk_lv3evdty6fk3whjwtmqn6ygs7 UNIQUE (demonstration_id, testing_session_id);


--
-- Name: tm_home uk_m6xa64ak77qn64i52f94o0aui; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_home
    ADD CONSTRAINT uk_m6xa64ak77qn64i52f94o0aui UNIQUE (language);


--
-- Name: tm_invoice uk_m8wwkgy5y1mtt2vn9nsiv143g; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT uk_m8wwkgy5y1mtt2vn9nsiv143g UNIQUE (invoice_number);


--
-- Name: sys_system_subtype uk_m93upfoi8eoru26w6wa71b9w3; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtype
    ADD CONSTRAINT uk_m93upfoi8eoru26w6wa71b9w3 UNIQUE (keyword);


--
-- Name: tf_profile_link uk_mei0k3s7jd4oyc3000w96e3oi; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT uk_mei0k3s7jd4oyc3000w96e3oi UNIQUE (actor_integration_profile_id, transaction_id, transaction_option_type_id);


--
-- Name: tm_role_in_test_test_participants uk_mqxhfodh8n194l0vsaqoafdej; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_test_participants
    ADD CONSTRAINT uk_mqxhfodh8n194l0vsaqoafdej UNIQUE (role_in_test_id, test_participants_id);


--
-- Name: usr_currency uk_n1f3sbcxe00m8d0p6vhva0x96; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_currency
    ADD CONSTRAINT uk_n1f3sbcxe00m8d0p6vhva0x96 UNIQUE (name);


--
-- Name: tf_hl7_message_profile_affinity_domain uk_n5trtb5a0bt27vlsad418w4rj; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT uk_n5trtb5a0bt27vlsad418w4rj UNIQUE (hl7_message_profile_id, affinity_domain_id);


--
-- Name: tm_test_steps_output_ci uk_ncouo4jyhdge5n0lc7tae3ole; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci
    ADD CONSTRAINT uk_ncouo4jyhdge5n0lc7tae3ole UNIQUE (test_steps_id, contextual_information_id);


--
-- Name: tm_proxy_configuration_for_session uk_nfn91ua4n1fuo6655qopnveb9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_proxy_configuration_for_session
    ADD CONSTRAINT uk_nfn91ua4n1fuo6655qopnveb9 UNIQUE (configurationtype_id, testing_session_id);


--
-- Name: tf_transaction_option_type uk_nix1duwegwr24dpbqa0u7mgm9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type
    ADD CONSTRAINT uk_nix1duwegwr24dpbqa0u7mgm9 UNIQUE (keyword);


--
-- Name: tm_configuration_mapped_with_aipo uk_nlrdxrud9haf3aoqtwbv20mf0; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_mapped_with_aipo
    ADD CONSTRAINT uk_nlrdxrud9haf3aoqtwbv20mf0 UNIQUE (aipo_id);


--
-- Name: tf_integration_profile uk_ny4glgtyovt2w045nwct19arx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT uk_ny4glgtyovt2w045nwct19arx UNIQUE (keyword);


--
-- Name: tm_network_config_for_session uk_ollwjxfh9b0krgxki6lumkejv; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_network_config_for_session
    ADD CONSTRAINT uk_ollwjxfh9b0krgxki6lumkejv UNIQUE (testing_session_id);


--
-- Name: tm_system_in_session_user uk_p0jgiqxpcw7m6f363ek0pax50; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_user
    ADD CONSTRAINT uk_p0jgiqxpcw7m6f363ek0pax50 UNIQUE (user_id, system_in_session_id);


--
-- Name: tm_test_instance_token uk_p3327pp6vib0t9qyrn24b57v3; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_token
    ADD CONSTRAINT uk_p3327pp6vib0t9qyrn24b57v3 UNIQUE (value);


--
-- Name: sys_system_subtypes_per_system_type uk_p54his4fl04yygccw4fdhcips; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtypes_per_system_type
    ADD CONSTRAINT uk_p54his4fl04yygccw4fdhcips UNIQUE (system_type_id, system_subtype_id);


--
-- Name: tm_hl7_responder_configuration uk_pcmv1trmyn2fm7wxoqfgrh2s9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_responder_configuration
    ADD CONSTRAINT uk_pcmv1trmyn2fm7wxoqfgrh2s9 UNIQUE (id);


--
-- Name: tm_system_in_session uk_pd70pawc4iq8l7afq8mclgabg; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT uk_pd70pawc4iq8l7afq8mclgabg UNIQUE (system_id, testing_session_id);


--
-- Name: tf_integration_profile_status_type uk_pejuyujydm4t4ylimltqlogfc; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type
    ADD CONSTRAINT uk_pejuyujydm4t4ylimltqlogfc UNIQUE (name);


--
-- Name: tm_hl7_initiator_configuration uk_pipx90x9f6lv8wc7bauawo88p; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_initiator_configuration
    ADD CONSTRAINT uk_pipx90x9f6lv8wc7bauawo88p UNIQUE (id);


--
-- Name: tf_document uk_q7ne4eu5ts5lgfl3aces2ikin; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT uk_q7ne4eu5ts5lgfl3aces2ikin UNIQUE (md5_hash_code);


--
-- Name: tm_system_actor_profiles uk_qd01r6y0s49jd12l0m462ayo8; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT uk_qd01r6y0s49jd12l0m462ayo8 UNIQUE (system_id, actor_integration_profile_option_id);


--
-- Name: tm_system uk_qhpcvotilafl949f1g3do63tt; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system
    ADD CONSTRAINT uk_qhpcvotilafl949f1g3do63tt UNIQUE (name, version);


--
-- Name: tm_test_test_description uk_qo6uflwoy4qa3qokcqrt996bm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description
    ADD CONSTRAINT uk_qo6uflwoy4qa3qokcqrt996bm UNIQUE (test_id, test_description_id);


--
-- Name: tm_contextual_information uk_qq8qo8sirf841okjdfjyw7ycx; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information
    ADD CONSTRAINT uk_qq8qo8sirf841okjdfjyw7ycx UNIQUE (label, value);


--
-- Name: tm_role_in_test uk_qw7b4tx9g3di4wo95mpoip1c9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test
    ADD CONSTRAINT uk_qw7b4tx9g3di4wo95mpoip1c9 UNIQUE (keyword);


--
-- Name: tm_test uk_rvv55pf2itvv4lrg71ad0pg4s; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test
    ADD CONSTRAINT uk_rvv55pf2itvv4lrg71ad0pg4s UNIQUE (keyword);


--
-- Name: tm_sap_result_tr_comment uk_rxj81ybsh4wf86h4wc9buyo25; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sap_result_tr_comment
    ADD CONSTRAINT uk_rxj81ybsh4wf86h4wc9buyo25 UNIQUE (sap_id, tr_id);


--
-- Name: pr_crawler_unmatching_hascode_for_integration_statements uk_s4yfodw20na8a0njfk5wvspx9; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unmatching_hascode_for_integration_statements
    ADD CONSTRAINT uk_s4yfodw20na8a0njfk5wvspx9 UNIQUE (system_id, crawler_report_id);


--
-- Name: tf_ws_transaction_usage uk_s65caasaiq9j4f8f99hikh9qm; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT uk_s65caasaiq9j4f8f99hikh9qm UNIQUE (usage);


--
-- Name: usr_users uk_s6erk9upsmv5muuvcynby6mm7; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT uk_s6erk9upsmv5muuvcynby6mm7 UNIQUE (username);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes uk_sgdk7xcq7relwxufplvfm62b2; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes
    ADD CONSTRAINT uk_sgdk7xcq7relwxufplvfm62b2 UNIQUE (tm_conf_mapped_id, tm_conftypes_id);


--
-- Name: tm_test_roles uk_si09xlggxxmbda6b2ablydara; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles
    ADD CONSTRAINT uk_si09xlggxxmbda6b2ablydara UNIQUE (test_id, role_in_test_id);


--
-- Name: tm_test_instance_path_to_log_file uk_sndyxmwxaou29u0jdyq5jxl95; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_path_to_log_file
    ADD CONSTRAINT uk_sndyxmwxaou29u0jdyq5jxl95 UNIQUE (path);


--
-- Name: tm_meta_test_test_roles uk_sspau5tor4bss0vn9s4sj0mtl; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles
    ADD CONSTRAINT uk_sspau5tor4bss0vn9s4sj0mtl UNIQUE (test_roles_id, meta_test_id);


--
-- Name: tm_invoice uk_tfqb9a3h09ycwkwj0dcsx81yp; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT uk_tfqb9a3h09ycwkwj0dcsx81yp UNIQUE (testing_session_id, institution_id);


--
-- Name: tf_integration_profile_option uk_thqc1vykug4qhn8jdij04d1bh; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option
    ADD CONSTRAINT uk_thqc1vykug4qhn8jdij04d1bh UNIQUE (keyword);


--
-- Name: usr_address usr_address_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_address
    ADD CONSTRAINT usr_address_pkey PRIMARY KEY (id);


--
-- Name: usr_currency usr_currency_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_currency
    ADD CONSTRAINT usr_currency_pkey PRIMARY KEY (keyword);


--
-- Name: usr_gazelle_language_aud usr_gazelle_language_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language_aud
    ADD CONSTRAINT usr_gazelle_language_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: usr_gazelle_language usr_gazelle_language_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language
    ADD CONSTRAINT usr_gazelle_language_pkey PRIMARY KEY (id);


--
-- Name: usr_institution usr_institution_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT usr_institution_pkey PRIMARY KEY (id);


--
-- Name: usr_institution_type usr_institution_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_type
    ADD CONSTRAINT usr_institution_type_pkey PRIMARY KEY (id);


--
-- Name: usr_iso_3166_country_code usr_iso_3166_country_code_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_iso_3166_country_code
    ADD CONSTRAINT usr_iso_3166_country_code_pkey PRIMARY KEY (iso);


--
-- Name: usr_message_parameters usr_message_parameters_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_message_parameters
    ADD CONSTRAINT usr_message_parameters_pkey PRIMARY KEY (id);


--
-- Name: usr_messages usr_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages
    ADD CONSTRAINT usr_messages_pkey PRIMARY KEY (id);


--
-- Name: usr_person_function usr_person_function_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person_function
    ADD CONSTRAINT usr_person_function_pkey PRIMARY KEY (id);


--
-- Name: usr_person usr_person_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT usr_person_pkey PRIMARY KEY (id);


--
-- Name: usr_role usr_role_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_role
    ADD CONSTRAINT usr_role_pkey PRIMARY KEY (id);


--
-- Name: usr_users usr_users_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT usr_users_pkey PRIMARY KEY (id);


--
-- Name: validation_service validation_service_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_service
    ADD CONSTRAINT validation_service_pkey PRIMARY KEY (id);


--
-- Name: validation_status validation_status_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.validation_status
    ADD CONSTRAINT validation_status_pkey PRIMARY KEY (id);


--
-- Name: usr_persons_functions fk_16hc2h0f9ovodr85lrom22x3i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT fk_16hc2h0f9ovodr85lrom22x3i FOREIGN KEY (person_function_id) REFERENCES public.usr_person_function(id);


--
-- Name: message_validation_service fk_17y447lpjkoio62cfhcp3h1h5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.message_validation_service
    ADD CONSTRAINT fk_17y447lpjkoio62cfhcp3h1h5 FOREIGN KEY (tm_test_step_message_profile_id) REFERENCES public.tm_test_step_message_profile(id);


--
-- Name: tm_test_aud fk_18e31b5912a7owpnriag3ywfq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_aud
    ADD CONSTRAINT fk_18e31b5912a7owpnriag3ywfq FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_integration_profile fk_1cdstaxi6v493qidfghxv24ry; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_1cdstaxi6v493qidfghxv24ry FOREIGN KEY (integration_profile_status_type_id) REFERENCES public.tf_integration_profile_status_type(id);


--
-- Name: usr_address fk_1d1y1m6snweppg6484hil729f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_address
    ADD CONSTRAINT fk_1d1y1m6snweppg6484hil729f FOREIGN KEY (country) REFERENCES public.usr_iso_3166_country_code(iso);


--
-- Name: tm_object_instance_file fk_1g15jkkjc04vhpphm0ekoouyi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_file
    ADD CONSTRAINT fk_1g15jkkjc04vhpphm0ekoouyi FOREIGN KEY (instance_id) REFERENCES public.tm_object_instance(id);


--
-- Name: tm_domains_in_testing_sessions fk_1gqpdhsf8nqvvu78f3xaleah1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_domains_in_testing_sessions
    ADD CONSTRAINT fk_1gqpdhsf8nqvvu78f3xaleah1 FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: pr_integration_statement_download fk_1lnibcrxff9bekskqh8oavdmv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_integration_statement_download
    ADD CONSTRAINT fk_1lnibcrxff9bekskqh8oavdmv FOREIGN KEY (search_log_report_id) REFERENCES public.pr_search_log_report(id);


--
-- Name: tf_rule fk_1nipbe194qol7ts0abf705jv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_1nipbe194qol7ts0abf705jv4 FOREIGN KEY (consequence_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tm_step_instance_msg_validation fk_1rsj7pba6u7f799vdf48m87fc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_msg_validation
    ADD CONSTRAINT fk_1rsj7pba6u7f799vdf48m87fc FOREIGN KEY (validation_status_id) REFERENCES public.validation_status(id);


--
-- Name: tm_oid_system_assignment fk_1s86g9lwepdr73wyckk97bai0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_system_assignment
    ADD CONSTRAINT fk_1s86g9lwepdr73wyckk97bai0 FOREIGN KEY (oid_requirement_id) REFERENCES public.tm_oid_requirement(id);


--
-- Name: tm_test_steps_option_aud fk_1ue9aipf3ohgkfqquytiido20; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_option_aud
    ADD CONSTRAINT fk_1ue9aipf3ohgkfqquytiido20 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_steps_input_ci fk_1x5t8tcbs9c6a3056aya7niiv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_input_ci
    ADD CONSTRAINT fk_1x5t8tcbs9c6a3056aya7niiv FOREIGN KEY (test_steps_id) REFERENCES public.tm_test_steps(id);


--
-- Name: tm_test_instance_test_instance_path_to_log_file fk_1y3oux0erpkodkhrntpfsxcd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_instance_path_to_log_file
    ADD CONSTRAINT fk_1y3oux0erpkodkhrntpfsxcd FOREIGN KEY (test_instance_path_id) REFERENCES public.tm_test_instance_path_to_log_file(id);


--
-- Name: tm_raw_configuration fk_20plq4300e5jtag03lyiepdy8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_raw_configuration
    ADD CONSTRAINT fk_20plq4300e5jtag03lyiepdy8 FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_transport_layer_for_config_aud fk_23vbm9sy2cn26qx3uuau8f8vm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_transport_layer_for_config_aud
    ADD CONSTRAINT fk_23vbm9sy2cn26qx3uuau8f8vm FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_steps_instance fk_251qrp06vb0kqxoiovm55h7wo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT fk_251qrp06vb0kqxoiovm55h7wo FOREIGN KEY (test_steps_instance_status_id) REFERENCES public.tm_test_steps_instance_status(id);


--
-- Name: tm_profiles_in_testing_sessions fk_25f9a2a2aeltibe0rgrq6thrt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_profiles_in_testing_sessions
    ADD CONSTRAINT fk_25f9a2a2aeltibe0rgrq6thrt FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_test_steps_instance fk_29wk9d5upvxvh7c4h2en7y4q6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT fk_29wk9d5upvxvh7c4h2en7y4q6 FOREIGN KEY (system_in_session_reponder_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_test_status_aud fk_2f9kmcx71utv1fsr9fruhefbq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_status_aud
    ADD CONSTRAINT fk_2f9kmcx71utv1fsr9fruhefbq FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_object_reader fk_2khsqk2eae145oxm0axll5e74; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader
    ADD CONSTRAINT fk_2khsqk2eae145oxm0axll5e74 FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tm_test_steps_aud fk_2mr95d6g5mox0g2xyokwfisgr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_aud
    ADD CONSTRAINT fk_2mr95d6g5mox0g2xyokwfisgr FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_hl7_v3_responder_configuration fk_2nujr7yyrd79s3mgqcyhetba1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_2nujr7yyrd79s3mgqcyhetba1 FOREIGN KEY (ws_transaction_usage) REFERENCES public.tf_ws_transaction_usage(id);


--
-- Name: tm_test_peer_type_aud fk_2wifot5ui1jvkaw8hg4p0414n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_peer_type_aud
    ADD CONSTRAINT fk_2wifot5ui1jvkaw8hg4p0414n FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_37u9uwxammh3m9bg9jfueia3i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_37u9uwxammh3m9bg9jfueia3i FOREIGN KEY (affinity_domain_id) REFERENCES public.tf_affinity_domain(id);


--
-- Name: tf_transaction_standard_aud fk_384id91wu9ujfmxhkbj25hfl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard_aud
    ADD CONSTRAINT fk_384id91wu9ujfmxhkbj25hfl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_in_session fk_3a6r9hfs65l9s1capg656gfe; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT fk_3a6r9hfs65l9s1capg656gfe FOREIGN KEY (table_session_id) REFERENCES public.sys_table_session(id);


--
-- Name: pr_crawler_changed_integration_statements fk_3a85t5qlgngd7474q63kh9km8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_changed_integration_statements
    ADD CONSTRAINT fk_3a85t5qlgngd7474q63kh9km8 FOREIGN KEY (crawler_report_id) REFERENCES public.pr_crawler_reporting(id);


--
-- Name: tm_system_actor_profiles fk_3b2gi4l03hndwbeh6e34glkus; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT fk_3b2gi4l03hndwbeh6e34glkus FOREIGN KEY (actor_integration_profile_option_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tm_test fk_3fu2cc480624phjiydcet3phw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test
    ADD CONSTRAINT fk_3fu2cc480624phjiydcet3phw FOREIGN KEY (test_peer_type_id) REFERENCES public.tm_test_peer_type(id);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes fk_3h1hfdqnd3lh81w9l7qdmldv9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes
    ADD CONSTRAINT fk_3h1hfdqnd3lh81w9l7qdmldv9 FOREIGN KEY (tm_conf_mapped_id) REFERENCES public.tm_configuration_mapped_with_aipo(id);


--
-- Name: tm_testing_session_admin fk_3iy6socfnp2sr6mrru70yb39a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session_admin
    ADD CONSTRAINT fk_3iy6socfnp2sr6mrru70yb39a FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_invoice fk_3kre8coxc1wbp441saqht8ptp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT fk_3kre8coxc1wbp441saqht8ptp FOREIGN KEY (currency) REFERENCES public.usr_currency(keyword);


--
-- Name: tf_hl7_message_profile_affinity_domain_aud fk_3kwlpb038de40g5jrhoa83fab; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain_aud
    ADD CONSTRAINT fk_3kwlpb038de40g5jrhoa83fab FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_actor_profiles fk_3nhtpl5h54jxmrlxh40vjy3qq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT fk_3nhtpl5h54jxmrlxh40vjy3qq FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_hl7_v3_initiator_configuration fk_3rdh5aw7ih75hbe7tkyleqb0o; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_initiator_configuration
    ADD CONSTRAINT fk_3rdh5aw7ih75hbe7tkyleqb0o FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_invoice fk_3w3idlwl4lo8gbdk5whoqqgmv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT fk_3w3idlwl4lo8gbdk5whoqqgmv FOREIGN KEY (vat_country) REFERENCES public.usr_iso_3166_country_code(iso);


--
-- Name: tm_contextual_information fk_42coovf2ewk9rw4wh44xxc7s4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information
    ADD CONSTRAINT fk_42coovf2ewk9rw4wh44xxc7s4 FOREIGN KEY (path_id) REFERENCES public.tm_path(id);


--
-- Name: usr_gazelle_language_aud fk_43ntbbwffjv2tx97jvndnnyx1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_gazelle_language_aud
    ADD CONSTRAINT fk_43ntbbwffjv2tx97jvndnnyx1 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_description_aud fk_452cadasl8l8ii63bvtqu2xah; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_description_aud
    ADD CONSTRAINT fk_452cadasl8l8ii63bvtqu2xah FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_steps_output_ci fk_4598bvemjin9b99l7kwypultu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci
    ADD CONSTRAINT fk_4598bvemjin9b99l7kwypultu FOREIGN KEY (test_steps_id) REFERENCES public.tm_test_steps(id);


--
-- Name: tm_object_file fk_469c0firrof2dyo9op3kaa65n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file
    ADD CONSTRAINT fk_469c0firrof2dyo9op3kaa65n FOREIGN KEY (object_id) REFERENCES public.tm_object_type(id);


--
-- Name: tm_test_steps fk_4ab1t5jak4owpbwihdod7w56r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT fk_4ab1t5jak4owpbwihdod7w56r FOREIGN KEY (test_roles_initiator_id) REFERENCES public.tm_test_roles(id);


--
-- Name: tm_proxy_configuration_for_session fk_4bu9pwippknkm08oxe0s8kd7k; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_proxy_configuration_for_session
    ADD CONSTRAINT fk_4bu9pwippknkm08oxe0s8kd7k FOREIGN KEY (configurationtype_id) REFERENCES public.tm_configuration_type(id);


--
-- Name: tm_test_steps fk_4f1iga2b9r6uv6qwc6e5ta6rq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT fk_4f1iga2b9r6uv6qwc6e5ta6rq FOREIGN KEY (ws_transaction_usage_id) REFERENCES public.tf_ws_transaction_usage(id);


--
-- Name: tm_oid_requirement_aipo fk_4j2bfylfqbrtgqnb5kd9ect2x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_requirement_aipo
    ADD CONSTRAINT fk_4j2bfylfqbrtgqnb5kd9ect2x FOREIGN KEY (oid_requirement_id) REFERENCES public.tm_oid_requirement(id);


--
-- Name: tf_affinity_domain_aud fk_4ji8wqgg51x3x2k4jgl52y07b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_affinity_domain_aud
    ADD CONSTRAINT fk_4ji8wqgg51x3x2k4jgl52y07b FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_oid_requirement_aipo fk_4kq9fsdkrtitht5u00mq5g93x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_requirement_aipo
    ADD CONSTRAINT fk_4kq9fsdkrtitht5u00mq5g93x FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: pr_search_criteria_per_report fk_4v31a5t9k7kygo34dr7s2tga; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_criteria_per_report
    ADD CONSTRAINT fk_4v31a5t9k7kygo34dr7s2tga FOREIGN KEY (search_log_report_id) REFERENCES public.pr_search_log_report(id);


--
-- Name: tm_object_creator fk_4vov78p17loja71x69s3dpewr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator
    ADD CONSTRAINT fk_4vov78p17loja71x69s3dpewr FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tm_object_creator fk_4y6fyu1vf252a4onwo1mgfucw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator
    ADD CONSTRAINT fk_4y6fyu1vf252a4onwo1mgfucw FOREIGN KEY (object_id) REFERENCES public.tm_object_type(id);


--
-- Name: usr_persons_functions fk_50qu55jx1koo5alhsgtomyct6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_persons_functions
    ADD CONSTRAINT fk_50qu55jx1koo5alhsgtomyct6 FOREIGN KEY (person_id) REFERENCES public.usr_person(id);


--
-- Name: tm_object_type_status_aud fk_56965fu5cb2vv5mvl7qiq1vwc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_status_aud
    ADD CONSTRAINT fk_56965fu5cb2vv5mvl7qiq1vwc FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_in_session fk_596e0vcheor4cc24a3dhdnr5j; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT fk_596e0vcheor4cc24a3dhdnr5j FOREIGN KEY (person_servicing) REFERENCES public.usr_person(id);


--
-- Name: tm_system_aipo_result_for_a_testing_session fk_5c2i15tw4psyh9m8ko6ri0q9a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_aipo_result_for_a_testing_session
    ADD CONSTRAINT fk_5c2i15tw4psyh9m8ko6ri0q9a FOREIGN KEY (status_id) REFERENCES public.tm_status_results(id);


--
-- Name: tf_actor_integration_profile fk_5ilf7sfvvixf9f51lmphnwyoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5ilf7sfvvixf9f51lmphnwyoa FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tm_test_instance fk_5j9f5ymdapc16dog5x9q64qip; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance
    ADD CONSTRAINT fk_5j9f5ymdapc16dog5x9q64qip FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tf_transaction_link fk_5lra5ub3x8tyhweuxa4wfv1bi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_5lra5ub3x8tyhweuxa4wfv1bi FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_step_instance_message fk_5vbvipkdoubrau8o7cerkdrho; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_5vbvipkdoubrau8o7cerkdrho FOREIGN KEY (receiver_system_id) REFERENCES public.tm_system(id);


--
-- Name: tf_domain_profile fk_5xrcq9irc0lfqkd1hfwfggh7q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_5xrcq9irc0lfqkd1hfwfggh7q FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tm_generic_oids_ip_config_param_for_session fk_5yp3fkpqthmntq2dgtty7uoon; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_ip_config_param_for_session
    ADD CONSTRAINT fk_5yp3fkpqthmntq2dgtty7uoon FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tf_actor_integration_profile fk_5yubxkv7cw5mqpv5u0axd4f1x; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile
    ADD CONSTRAINT fk_5yubxkv7cw5mqpv5u0axd4f1x FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tm_system fk_60phploi6or05gwfh426rvwhd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system
    ADD CONSTRAINT fk_60phploi6or05gwfh426rvwhd FOREIGN KEY (owner_user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_test fk_61ug0sy9g9qkyk614sru9pg2q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test
    ADD CONSTRAINT fk_61ug0sy9g9qkyk614sru9pg2q FOREIGN KEY (test_status_id) REFERENCES public.tm_test_status(id);


--
-- Name: tm_hl7_v3_responder_configuration fk_6480vkp55qlr3nrisbuj1ijrj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_v3_responder_configuration
    ADD CONSTRAINT fk_6480vkp55qlr3nrisbuj1ijrj FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tf_rule_list_criterion fk_65ptid9agnv6s0kkvttoxmtqx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_65ptid9agnv6s0kkvttoxmtqx FOREIGN KEY (tf_rule_criterion_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tm_step_instance_msg_validation fk_66f43ltpyniif8n3j5eka8062; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_msg_validation
    ADD CONSTRAINT fk_66f43ltpyniif8n3j5eka8062 FOREIGN KEY (message_validation_service_id) REFERENCES public.message_validation_service(id);


--
-- Name: tm_test_option_aud fk_672kjf00ps3ue31mqbc582nnj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_option_aud
    ADD CONSTRAINT fk_672kjf00ps3ue31mqbc582nnj FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_user_preferences fk_6e83kvu2r93s4hafcs6gcwmah; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_preferences
    ADD CONSTRAINT fk_6e83kvu2r93s4hafcs6gcwmah FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_step_instance_message fk_6gd10isc1i0tkdk1hmje46kvf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_6gd10isc1i0tkdk1hmje46kvf FOREIGN KEY (receiver_institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tf_hl7_message_profile fk_6ok2phqeqflrttiroorpowqhb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_6ok2phqeqflrttiroorpowqhb FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_integration_profile_type_link_aud fk_6qrcdc6ud9tdwmr0h3vc85un2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link_aud
    ADD CONSTRAINT fk_6qrcdc6ud9tdwmr0h3vc85un2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_test_description_aud fk_6souldavhsnbubb6tu492mmrf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description_aud
    ADD CONSTRAINT fk_6souldavhsnbubb6tu492mmrf FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_object_reader fk_6ttu9p6wiqbg5mm020nnenulr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader
    ADD CONSTRAINT fk_6ttu9p6wiqbg5mm020nnenulr FOREIGN KEY (object_id) REFERENCES public.tm_object_type(id);


--
-- Name: tf_rule_criterion fk_6ybxwerfkie3n814ffvgjghvk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_6ybxwerfkie3n814ffvgjghvk FOREIGN KEY (single_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: pr_crawler_unreachable_integration_statements fk_6yry7okjeu3cbrnkulc9wf8c4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unreachable_integration_statements
    ADD CONSTRAINT fk_6yry7okjeu3cbrnkulc9wf8c4 FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_system_events fk_73t8uikw49mu4kfwnnpv17i6y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_events
    ADD CONSTRAINT fk_73t8uikw49mu4kfwnnpv17i6y FOREIGN KEY (tm_system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_testing_session fk_77qbxfqy7ljaee92gcspcmhoq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session
    ADD CONSTRAINT fk_77qbxfqy7ljaee92gcspcmhoq FOREIGN KEY (currency_id) REFERENCES public.usr_currency(keyword);


--
-- Name: tm_test_steps_test_steps_data fk_7bla4jq9vrgr2ke17oq45ku5d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_test_steps_data
    ADD CONSTRAINT fk_7bla4jq9vrgr2ke17oq45ku5d FOREIGN KEY (test_steps_data_id) REFERENCES public.tm_test_steps_data(id);


--
-- Name: tm_test_steps_output_ci fk_7d4l8ajyfxxugo8oj6j0uu5b5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci
    ADD CONSTRAINT fk_7d4l8ajyfxxugo8oj6j0uu5b5 FOREIGN KEY (contextual_information_id) REFERENCES public.tm_contextual_information(id);


--
-- Name: message_validation_service fk_7d9pbjm9eodlai3hgsteqgcsa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.message_validation_service
    ADD CONSTRAINT fk_7d9pbjm9eodlai3hgsteqgcsa FOREIGN KEY (validation_service_id) REFERENCES public.validation_service(id);


--
-- Name: tf_profile_link fk_7gn0cpke6kjpbcps2iy70rvxx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_7gn0cpke6kjpbcps2iy70rvxx FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: sys_system_subtypes_per_system_type fk_7isdxbdhbu2kroog9j52fo2cm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtypes_per_system_type
    ADD CONSTRAINT fk_7isdxbdhbu2kroog9j52fo2cm FOREIGN KEY (system_subtype_id) REFERENCES public.sys_system_subtype(id);


--
-- Name: pr_mail fk_7j3ya8wlct6u72a3lqayjn3r0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_mail
    ADD CONSTRAINT fk_7j3ya8wlct6u72a3lqayjn3r0 FOREIGN KEY (crawlerreporting_id) REFERENCES public.pr_crawler_reporting(id);


--
-- Name: tf_integration_profile_aud fk_7r7ffh8bhd4yj0gsn6o900j72; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_aud
    ADD CONSTRAINT fk_7r7ffh8bhd4yj0gsn6o900j72 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_instance_test_steps_instance fk_7ve0rah5fu0runtd15gavbrsg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_steps_instance
    ADD CONSTRAINT fk_7ve0rah5fu0runtd15gavbrsg FOREIGN KEY (test_instance_id) REFERENCES public.tm_test_instance(id);


--
-- Name: pr_crawler_unreachable_integration_statements fk_7wwbm3mp5ujf2xqprtylnrn4u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unreachable_integration_statements
    ADD CONSTRAINT fk_7wwbm3mp5ujf2xqprtylnrn4u FOREIGN KEY (crawler_report_id) REFERENCES public.pr_crawler_reporting(id);


--
-- Name: tf_rule_list_criterion fk_81shykw7emr3q41anx9ksqdb6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_list_criterion
    ADD CONSTRAINT fk_81shykw7emr3q41anx9ksqdb6 FOREIGN KEY (aipocriterions_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes fk_81wqciwyf0gnhx01h4jl9ix9m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes
    ADD CONSTRAINT fk_81wqciwyf0gnhx01h4jl9ix9m FOREIGN KEY (tm_conftypes_id) REFERENCES public.tm_conftype_w_ports_wstype_and_sop_class(id);


--
-- Name: tm_connectathon_participant fk_82k4rbv805g3mhcw8isg06q5s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant
    ADD CONSTRAINT fk_82k4rbv805g3mhcw8isg06q5s FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_test_instance_participants fk_84pq2j8gjdyqd0vjgpw2gw93l; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT fk_84pq2j8gjdyqd0vjgpw2gw93l FOREIGN KEY (test_instance_id) REFERENCES public.tm_test_instance(id);


--
-- Name: tm_test fk_8761taset675qit1uaqo78up4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test
    ADD CONSTRAINT fk_8761taset675qit1uaqo78up4 FOREIGN KEY (test_type_id) REFERENCES public.tm_test_type(id);


--
-- Name: tf_audit_message fk_8i38nhkvxfbynrf6n14x74sni; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_8i38nhkvxfbynrf6n14x74sni FOREIGN KEY (audited_transaction) REFERENCES public.tf_transaction(id);


--
-- Name: tm_step_instance_message fk_8i6tlgj01rhehcacmbui9kci8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_8i6tlgj01rhehcacmbui9kci8 FOREIGN KEY (sender_system_id) REFERENCES public.tm_system(id);


--
-- Name: pr_crawler_unmatching_hascode_for_integration_statements fk_8ivvmdbjtvxcl6dp8cx0hgwqm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unmatching_hascode_for_integration_statements
    ADD CONSTRAINT fk_8ivvmdbjtvxcl6dp8cx0hgwqm FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_abstract_configuration fk_8lpw88g6cvs9wolc1rn7pk8ur; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_abstract_configuration
    ADD CONSTRAINT fk_8lpw88g6cvs9wolc1rn7pk8ur FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: sys_dicom_documents_for_systems fk_8oi0rclvqbh67h9yvjpv6s3g1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_dicom_documents_for_systems
    ADD CONSTRAINT fk_8oi0rclvqbh67h9yvjpv6s3g1 FOREIGN KEY (path_linking_a_document_id) REFERENCES public.cmn_path_linking_a_document(id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class fk_8quekfveaqrk2656esn2n5gki; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT fk_8quekfveaqrk2656esn2n5gki FOREIGN KEY (sop_class_id) REFERENCES public.tm_sop_class(id);


--
-- Name: tf_transaction fk_8u4sa8axnsn6v8jy23skc3qty; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_8u4sa8axnsn6v8jy23skc3qty FOREIGN KEY (transaction_status_type_id) REFERENCES public.tf_transaction_status_type(id);


--
-- Name: tm_role_in_test_test_participants fk_8x0ui7bq7hbkky6tr1kpou8pu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_test_participants
    ADD CONSTRAINT fk_8x0ui7bq7hbkky6tr1kpou8pu FOREIGN KEY (test_participants_id) REFERENCES public.tm_test_participants(id);


--
-- Name: usr_messages_usr_message_parameters fk_90m8qkysomsmf1wugt7tcbldc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages_usr_message_parameters
    ADD CONSTRAINT fk_90m8qkysomsmf1wugt7tcbldc FOREIGN KEY (usr_messages_id) REFERENCES public.usr_messages(id);


--
-- Name: tf_profile_link fk_92x366ed20bldob29khteawg4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_92x366ed20bldob29khteawg4 FOREIGN KEY (transaction_option_type_id) REFERENCES public.tf_transaction_option_type(id);


--
-- Name: tm_configuration_mapped_with_aipo_aud fk_97mdmhttpg4975g987qnqw57w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_mapped_with_aipo_aud
    ADD CONSTRAINT fk_97mdmhttpg4975g987qnqw57w FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_object_instance fk_9bf01jcy9se723quqv9r8m6my; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance
    ADD CONSTRAINT fk_9bf01jcy9se723quqv9r8m6my FOREIGN KEY (object_id) REFERENCES public.tm_object_type(id);


--
-- Name: tm_object_instance fk_9bl1r40nl549cql07w201jg9a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance
    ADD CONSTRAINT fk_9bl1r40nl549cql07w201jg9a FOREIGN KEY (validation_id) REFERENCES public.tm_object_instance_validation(id);


--
-- Name: tf_hl7_message_profile fk_9by8jwr94eeb0bme1bsql5hdb; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_9by8jwr94eeb0bme1bsql5hdb FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_rule fk_9cynyuydbfmqhresmj8n1uuqo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule
    ADD CONSTRAINT fk_9cynyuydbfmqhresmj8n1uuqo FOREIGN KEY (cause_id) REFERENCES public.tf_rule_criterion(id);


--
-- Name: tm_monitor_in_session fk_9dmu3nvkx0blel6oqbqaph93r; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_in_session
    ADD CONSTRAINT fk_9dmu3nvkx0blel6oqbqaph93r FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_meta_test_test_roles_aud fk_9ewqpyspbek8mft0xncf8n7k9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles_aud
    ADD CONSTRAINT fk_9ewqpyspbek8mft0xncf8n7k9 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_roles fk_9f12falm1uj6f0lk47jsrwsfy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles
    ADD CONSTRAINT fk_9f12falm1uj6f0lk47jsrwsfy FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tf_transaction_link_aud fk_9gvvnh0aqrll2mp1u89a5msy0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link_aud
    ADD CONSTRAINT fk_9gvvnh0aqrll2mp1u89a5msy0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_host fk_9i4co1kbpfx461xh1bfyn7wsn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_host
    ADD CONSTRAINT fk_9i4co1kbpfx461xh1bfyn7wsn FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_jira_issues_to_test fk_9rsj4l7yw7aui134br9omsjbg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_jira_issues_to_test
    ADD CONSTRAINT fk_9rsj4l7yw7aui134br9omsjbg FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tm_system_actor_profiles fk_9we4tovckv3jng4lp9fajwiq9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT fk_9we4tovckv3jng4lp9fajwiq9 FOREIGN KEY (wanted_testing_type_id) REFERENCES public.tm_testing_depth(id);


--
-- Name: tm_test_instance_participants fk_9xrh9nfs8iwy5neeuufug9upg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT fk_9xrh9nfs8iwy5neeuufug9upg FOREIGN KEY (role_in_test_id) REFERENCES public.tm_role_in_test(id);


--
-- Name: tm_generic_oids_institution_for_session fk_9yvucm5rkx6qynt97lv8th990; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_institution_for_session
    ADD CONSTRAINT fk_9yvucm5rkx6qynt97lv8th990 FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_generic_oids_ip_config_param_for_session fk_a1933k1wr5jkm61gbkhk2nebm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_generic_oids_ip_config_param_for_session
    ADD CONSTRAINT fk_a1933k1wr5jkm61gbkhk2nebm FOREIGN KEY (profile_for_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tm_test_instance_test_status fk_a6trmbiln5cochmwt257er79p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_status
    ADD CONSTRAINT fk_a6trmbiln5cochmwt257er79p FOREIGN KEY (test_instance_id) REFERENCES public.tm_test_instance(id);


--
-- Name: tf_rule_criterion fk_ac8qpll9fw2bdtemmbkeos5pd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_ac8qpll9fw2bdtemmbkeos5pd FOREIGN KEY (single_integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_aud fk_ae8odst29dbdua80lcyqyckjp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_aud
    ADD CONSTRAINT fk_ae8odst29dbdua80lcyqyckjp FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_dicom_scu_configuration fk_ak3uddu14f6tn77f2mn5x6wt1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scu_configuration
    ADD CONSTRAINT fk_ak3uddu14f6tn77f2mn5x6wt1 FOREIGN KEY (sop_class_id) REFERENCES public.tm_sop_class(id);


--
-- Name: tf_transaction_link fk_apnhy27aykxqfd0exsrxlnns7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_apnhy27aykxqfd0exsrxlnns7 FOREIGN KEY (from_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tm_test_roles fk_asi2qiq1o9avipr6vn60uk2x7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles
    ADD CONSTRAINT fk_asi2qiq1o9avipr6vn60uk2x7 FOREIGN KEY (test_option_id) REFERENCES public.tm_test_option(id);


--
-- Name: tf_transaction_standard fk_atvilp88b07p05km224t7n5l4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_atvilp88b07p05km224t7n5l4 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_test_steps_input_ci fk_axey7iu60lyufxqcx6y6xltnn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_input_ci
    ADD CONSTRAINT fk_axey7iu60lyufxqcx6y6xltnn FOREIGN KEY (contextual_information_id) REFERENCES public.tm_contextual_information(id);


--
-- Name: tm_test_steps fk_b02auaivauew0nc2tklikbb48; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT fk_b02auaivauew0nc2tklikbb48 FOREIGN KEY (test_roles_responder_id) REFERENCES public.tm_test_roles(id);


--
-- Name: tf_integration_profile_type_link fk_b1w97il0injkp7gasy79nqj2q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_b1w97il0injkp7gasy79nqj2q FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tm_configuration fk_b1yww2wk8mkgfkk50veqq0as4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT fk_b1yww2wk8mkgfkk50veqq0as4 FOREIGN KEY (configurationtype_id) REFERENCES public.tm_configuration_type(id);


--
-- Name: tm_test_test_steps_aud fk_b6jh7e7hcko01wvsmxphgelwt; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps_aud
    ADD CONSTRAINT fk_b6jh7e7hcko01wvsmxphgelwt FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_instance_tm_test_steps_data fk_bbfrx9dk4cpehghwnxne7qkb9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_tm_test_steps_data
    ADD CONSTRAINT fk_bbfrx9dk4cpehghwnxne7qkb9 FOREIGN KEY (tm_test_instance_id) REFERENCES public.tm_test_instance(id);


--
-- Name: usr_messages_usr_users fk_bbjkuhm6mjmedyk955jbkhd87; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages_usr_users
    ADD CONSTRAINT fk_bbjkuhm6mjmedyk955jbkhd87 FOREIGN KEY (usr_messages_id) REFERENCES public.usr_messages(id);


--
-- Name: tm_test_instance_participants fk_bcti5cw4go4gkhmn1y9bbvgsd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT fk_bcti5cw4go4gkhmn1y9bbvgsd FOREIGN KEY (actor_integration_profile_option_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tm_system_in_session fk_bedyn367cnbhooj7aki6jlefu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT fk_bedyn367cnbhooj7aki6jlefu FOREIGN KEY (system_in_session_status_id) REFERENCES public.tm_system_in_session_status(id);


--
-- Name: tm_system_aipo_result_for_a_testing_session fk_bev0orrc4ed69h3oy5nu28sil; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_aipo_result_for_a_testing_session
    ADD CONSTRAINT fk_bev0orrc4ed69h3oy5nu28sil FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_object_instance_attribute fk_bga3ydulvsyioghxv1h16rnua; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_attribute
    ADD CONSTRAINT fk_bga3ydulvsyioghxv1h16rnua FOREIGN KEY (instance_id) REFERENCES public.tm_object_instance(id);


--
-- Name: tm_proxy_configuration_for_session fk_bge0hm829gjm3byxr1i7lhm0q; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_proxy_configuration_for_session
    ADD CONSTRAINT fk_bge0hm829gjm3byxr1i7lhm0q FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: usr_institution fk_bhdg36towjg9r5bf95mo6tsr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT fk_bhdg36towjg9r5bf95mo6tsr FOREIGN KEY (institution_type_id) REFERENCES public.usr_institution_type(id);


--
-- Name: tm_oid_institution fk_bl3ildc532146un3vad7o1fkm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_institution
    ADD CONSTRAINT fk_bl3ildc532146un3vad7o1fkm FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_object_file fk_bnyoo31f1o3od2a1a3b439du4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file
    ADD CONSTRAINT fk_bnyoo31f1o3od2a1a3b439du4 FOREIGN KEY (type_id) REFERENCES public.tm_object_file_type(id);


--
-- Name: tm_step_instance_message fk_bryd2q1eyk25suq9qa3bo34d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_bryd2q1eyk25suq9qa3bo34d FOREIGN KEY (sender_institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_contextual_information_instance fk_bu7a4yusvnwgx4nthfx5a5rl9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information_instance
    ADD CONSTRAINT fk_bu7a4yusvnwgx4nthfx5a5rl9 FOREIGN KEY (contextual_information) REFERENCES public.tm_contextual_information(id);


--
-- Name: sys_hl7_documents_for_systems fk_c0rd6ehgs21vy7fetdg0dfkve; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_hl7_documents_for_systems
    ADD CONSTRAINT fk_c0rd6ehgs21vy7fetdg0dfkve FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: usr_person fk_c36bomcig5ndglvu7cgo9mwh0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT fk_c36bomcig5ndglvu7cgo9mwh0 FOREIGN KEY (address_id) REFERENCES public.usr_address(id);


--
-- Name: tf_document_aud fk_c8up7729eltevk3c03jfnxd8f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_aud
    ADD CONSTRAINT fk_c8up7729eltevk3c03jfnxd8f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_rule_criterion fk_cguvfq0yml0ppij05b9549s2e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_cguvfq0yml0ppij05b9549s2e FOREIGN KEY (single_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tf_integration_profile_option_aud fk_cju6k73ktqmuwinim0g8h12e0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_option_aud
    ADD CONSTRAINT fk_cju6k73ktqmuwinim0g8h12e0 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_step_instance_message fk_cldl281ivxvujdgkx7vd9156d; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_cldl281ivxvujdgkx7vd9156d FOREIGN KEY (relates_to) REFERENCES public.tm_step_instance_message(id);


--
-- Name: tm_hl7_responder_configuration fk_clhb4e1s4dpmmlryaue5ljtxw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_responder_configuration
    ADD CONSTRAINT fk_clhb4e1s4dpmmlryaue5ljtxw FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_connectathon_participant fk_cxkj5xc9kc8o3h5090jny6hvl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant
    ADD CONSTRAINT fk_cxkj5xc9kc8o3h5090jny6hvl FOREIGN KEY (status_id) REFERENCES public.tm_connectathon_participant_status(id);


--
-- Name: tm_configuration fk_cypxrdhvupv7f6fsyvw6h9gyk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT fk_cypxrdhvupv7f6fsyvw6h9gyk FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_domain_aud fk_d3mp0skbxicng7w2tdumqnuel; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_aud
    ADD CONSTRAINT fk_d3mp0skbxicng7w2tdumqnuel FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_actor_aud fk_d66h3bo446ibhr2oql90bmq9h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_aud
    ADD CONSTRAINT fk_d66h3bo446ibhr2oql90bmq9h FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_instance fk_d860srimmj6dhuvdkeg5cq4yn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance
    ADD CONSTRAINT fk_d860srimmj6dhuvdkeg5cq4yn FOREIGN KEY (monitor_id) REFERENCES public.tm_monitor_in_session(id);


--
-- Name: tm_monitor_test fk_d8wj5komgpei9n2u5pkw4p0yo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_test
    ADD CONSTRAINT fk_d8wj5komgpei9n2u5pkw4p0yo FOREIGN KEY (monitor_id) REFERENCES public.tm_monitor_in_session(id);


--
-- Name: tf_actor_integration_profile_option fk_dd4cllr6xpdtoxp5wnjv4i2jn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_dd4cllr6xpdtoxp5wnjv4i2jn FOREIGN KEY (actor_integration_profile_id) REFERENCES public.tf_actor_integration_profile(id);


--
-- Name: usr_messages_usr_message_parameters fk_dgnn9iuua4q2tytrlkkhitjf1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages_usr_message_parameters
    ADD CONSTRAINT fk_dgnn9iuua4q2tytrlkkhitjf1 FOREIGN KEY (messageparameters_id) REFERENCES public.usr_message_parameters(id);


--
-- Name: tm_monitor_in_session fk_dgpeqm405ex4g8syplum8d0tg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_in_session
    ADD CONSTRAINT fk_dgpeqm405ex4g8syplum8d0tg FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_object_type fk_dod8ducq6ft6y43i6owvt0vms; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type
    ADD CONSTRAINT fk_dod8ducq6ft6y43i6owvt0vms FOREIGN KEY (object_type_status_id) REFERENCES public.tm_object_type_status(id);


--
-- Name: tm_object_instance_validation_aud fk_drkmngr75abmv18a83cppf7fe; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_validation_aud
    ADD CONSTRAINT fk_drkmngr75abmv18a83cppf7fe FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_instance_test_status fk_dt4ios5pp48n7weh8rxx4n9rf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_status
    ADD CONSTRAINT fk_dt4ios5pp48n7weh8rxx4n9rf FOREIGN KEY (status_id) REFERENCES public.tm_test_instance_event(id);


--
-- Name: tm_test_steps_test_steps_data fk_dti1os088k5gxknwhw7r867sx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_test_steps_data
    ADD CONSTRAINT fk_dti1os088k5gxknwhw7r867sx FOREIGN KEY (test_steps_id) REFERENCES public.tm_test_steps_instance(id);


--
-- Name: tm_test_steps_instance_output_ci_instance fk_dtjpfh844tx6t1af9blr7gog1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_output_ci_instance
    ADD CONSTRAINT fk_dtjpfh844tx6t1af9blr7gog1 FOREIGN KEY (test_steps_instance_id) REFERENCES public.tm_test_steps_instance(id);


--
-- Name: tf_domain_profile fk_du7myrlv6npj0053efkqu9rpg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile
    ADD CONSTRAINT fk_du7myrlv6npj0053efkqu9rpg FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tm_web_service_type fk_dwst1ggdqkkmtdox3rumvf2jr; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_type
    ADD CONSTRAINT fk_dwst1ggdqkkmtdox3rumvf2jr FOREIGN KEY (profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tf_transaction_link fk_dy6dg8etr367irrt8xqoh3q9g; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_link
    ADD CONSTRAINT fk_dy6dg8etr367irrt8xqoh3q9g FOREIGN KEY (to_actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tf_integration_profile_type_link fk_e1ifbmgpje5wcfsg34tuvvq83; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_link
    ADD CONSTRAINT fk_e1ifbmgpje5wcfsg34tuvvq83 FOREIGN KEY (integration_profile_type_id) REFERENCES public.tf_integration_profile_type(id);


--
-- Name: tm_test_test_steps fk_e38qm2pcggj6o8a2p5h1o3dly; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps
    ADD CONSTRAINT fk_e38qm2pcggj6o8a2p5h1o3dly FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tm_domains_in_testing_sessions fk_e4q14pg4h4kkh92m01noox84p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_domains_in_testing_sessions
    ADD CONSTRAINT fk_e4q14pg4h4kkh92m01noox84p FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tm_dicom_scp_configuration fk_e6cw93j0py0o3k0vi82j7how9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scp_configuration
    ADD CONSTRAINT fk_e6cw93j0py0o3k0vi82j7how9 FOREIGN KEY (sop_class_id) REFERENCES public.tm_sop_class(id);


--
-- Name: tm_object_file_aud fk_e7c5xa4wgmaoe5ucil05r33nl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_aud
    ADD CONSTRAINT fk_e7c5xa4wgmaoe5ucil05r33nl FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: pr_integration_statement_download fk_e95e5xco3dvl9bhx55012x5f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_integration_statement_download
    ADD CONSTRAINT fk_e95e5xco3dvl9bhx55012x5f FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_section fk_e9bkp20lx0m592rfpvl5gwo6l; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_section
    ADD CONSTRAINT fk_e9bkp20lx0m592rfpvl5gwo6l FOREIGN KEY (section_type_id) REFERENCES public.tm_section_type(id);


--
-- Name: tm_step_instance_message fk_ehqat0eb9haobllpkt8r36ryh; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_ehqat0eb9haobllpkt8r36ryh FOREIGN KEY (proxy_type_id) REFERENCES public.proxy_type(id);


--
-- Name: tm_oids_ip_config_param_for_session_for_hl7 fk_elms3go7hjd6st7hd3shhp14b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oids_ip_config_param_for_session_for_hl7
    ADD CONSTRAINT fk_elms3go7hjd6st7hd3shhp14b FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_web_service_configuration fk_em2vyp1rtn5q8tknrfa51ot8c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_configuration
    ADD CONSTRAINT fk_em2vyp1rtn5q8tknrfa51ot8c FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_test_test_description fk_emta3mmfgpy98t49uuw85ytj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description
    ADD CONSTRAINT fk_emta3mmfgpy98t49uuw85ytj FOREIGN KEY (test_description_id) REFERENCES public.tm_test_description(id);


--
-- Name: tm_object_attribute fk_enqnurnmnq8mqkn9ttx4p5qxw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_attribute
    ADD CONSTRAINT fk_enqnurnmnq8mqkn9ttx4p5qxw FOREIGN KEY (object_id) REFERENCES public.tm_object_type(id);


--
-- Name: tm_test_type_aud fk_ep2i9t4xodcjcpt4fca492evq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_type_aud
    ADD CONSTRAINT fk_ep2i9t4xodcjcpt4fca492evq FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_object_type_aud fk_eucoxj7yo1x2bsm7hscu1y01u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_type_aud
    ADD CONSTRAINT fk_eucoxj7yo1x2bsm7hscu1y01u FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_institution_system fk_ey64j4qj13nxoqrva3x0dfo8h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_institution_system
    ADD CONSTRAINT fk_ey64j4qj13nxoqrva3x0dfo8h FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_system_in_session_user fk_f95746m5seu092ncc6je8dp3b; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_user
    ADD CONSTRAINT fk_f95746m5seu092ncc6je8dp3b FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_test_instance_tm_test_steps_data fk_fcvt06p9ogywqpi7wv390hdd4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_tm_test_steps_data
    ADD CONSTRAINT fk_fcvt06p9ogywqpi7wv390hdd4 FOREIGN KEY (teststepsdatalist_id) REFERENCES public.tm_test_steps_data(id);


--
-- Name: tf_actor_integration_profile_aud fk_fffhdfnac6f1ry2ijq1jsehya; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_aud
    ADD CONSTRAINT fk_fffhdfnac6f1ry2ijq1jsehya FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_step_instance_message fk_fnh02vdlv1576p1dsi35eh0rn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_fnh02vdlv1576p1dsi35eh0rn FOREIGN KEY (process_status_id) REFERENCES public.tm_step_inst_msg_process_status(id);


--
-- Name: tf_standard_aud fk_g07iv8pl5kcogd8i5e0vp5dpc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_standard_aud
    ADD CONSTRAINT fk_g07iv8pl5kcogd8i5e0vp5dpc FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: usr_institution_address fk_g0l9chva61medkx6l14frkaoa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT fk_g0l9chva61medkx6l14frkaoa FOREIGN KEY (address_id) REFERENCES public.usr_address(id);


--
-- Name: tm_test_steps_instance_output_ci_instance fk_g1wg5f58l1pkjqnw416b306xk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_output_ci_instance
    ADD CONSTRAINT fk_g1wg5f58l1pkjqnw416b306xk FOREIGN KEY (contextual_information_instance_id) REFERENCES public.tm_contextual_information_instance(id);


--
-- Name: tm_connectathon_participant fk_g2xo6ac8ltupkc936681qpi5p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_connectathon_participant
    ADD CONSTRAINT fk_g2xo6ac8ltupkc936681qpi5p FOREIGN KEY (institution_ok_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_test_step_message_profile fk_gal7d8bf3yjmvg2l46g95bl1a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_step_message_profile
    ADD CONSTRAINT fk_gal7d8bf3yjmvg2l46g95bl1a FOREIGN KEY (tf_hl7_message_profile_id) REFERENCES public.tf_hl7_message_profile(id);


--
-- Name: tf_profile_link fk_gfv3euo4c19tpch111m3859t2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link
    ADD CONSTRAINT fk_gfv3euo4c19tpch111m3859t2 FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tf_hl7_message_profile fk_gmenyb86ceqff9ujrud4p4elf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile
    ADD CONSTRAINT fk_gmenyb86ceqff9ujrud4p4elf FOREIGN KEY (domain_id) REFERENCES public.tf_domain(id);


--
-- Name: tm_test_user_comment fk_gq3etdcefp2fj86m8gag3x7xd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_user_comment
    ADD CONSTRAINT fk_gq3etdcefp2fj86m8gag3x7xd FOREIGN KEY (user_comment_id) REFERENCES public.tm_user_comment(id);


--
-- Name: tm_step_instance_message fk_gqw41vxqo9df6sksde0m2580p; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_gqw41vxqo9df6sksde0m2580p FOREIGN KEY (sender_tm_configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_sop_class_aud fk_guvi4au5vqhjmf3i3q502aewu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sop_class_aud
    ADD CONSTRAINT fk_guvi4au5vqhjmf3i3q502aewu FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class_aud fk_gv71mtgelt71x3sognxfss1ru; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class_aud
    ADD CONSTRAINT fk_gv71mtgelt71x3sognxfss1ru FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_configuration fk_h1msohwij9xn53aetuae4iy9m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT fk_h1msohwij9xn53aetuae4iy9m FOREIGN KEY (host_id) REFERENCES public.tm_host(id);


--
-- Name: tm_test_steps fk_h1wtq3vlwty6105ibw2pjyjwk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT fk_h1wtq3vlwty6105ibw2pjyjwk FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_test_roles fk_h2pvtclpmgme2brs7wk0w3c5e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles
    ADD CONSTRAINT fk_h2pvtclpmgme2brs7wk0w3c5e FOREIGN KEY (role_in_test_id) REFERENCES public.tm_role_in_test(id);


--
-- Name: pr_search_criteria_per_report fk_h2w5wameru1m43upruylnw8xu; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_criteria_per_report
    ADD CONSTRAINT fk_h2w5wameru1m43upruylnw8xu FOREIGN KEY (search_log_criterion_id) REFERENCES public.pr_search_log_criterion(id);


--
-- Name: tm_configuration fk_h3cpscb0ttmpqnfyn39syg1f4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration
    ADD CONSTRAINT fk_h3cpscb0ttmpqnfyn39syg1f4 FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_test_steps_instance_input_ci_instance fk_h63ttx91v2t1h0564hsmptwg9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_input_ci_instance
    ADD CONSTRAINT fk_h63ttx91v2t1h0564hsmptwg9 FOREIGN KEY (test_steps_instance_id) REFERENCES public.tm_test_steps_instance(id);


--
-- Name: tm_oid_root_definition fk_h8ym6m2xbpfk5esjbnayppw7e; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_root_definition
    ADD CONSTRAINT fk_h8ym6m2xbpfk5esjbnayppw7e FOREIGN KEY (label_id) REFERENCES public.tm_oid_root_definition_label(id);


--
-- Name: tm_syslog_configuration fk_hbe4iiuec1lcf6d09jr4ny1j7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_syslog_configuration
    ADD CONSTRAINT fk_hbe4iiuec1lcf6d09jr4ny1j7 FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class fk_hi7lqy7855wnca9dxx7aietjg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT fk_hi7lqy7855wnca9dxx7aietjg FOREIGN KEY (ws_transaction_usage_id) REFERENCES public.tf_ws_transaction_usage(id);


--
-- Name: tm_test_instance_participants fk_hls7j25qlhs6c5ex4kd0y8wx4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT fk_hls7j25qlhs6c5ex4kd0y8wx4 FOREIGN KEY (status_id) REFERENCES public.tm_test_instance_participants_status(id);


--
-- Name: sys_dicom_documents_for_systems fk_hrfsiaetnyws8x0c66376hp1t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_dicom_documents_for_systems
    ADD CONSTRAINT fk_hrfsiaetnyws8x0c66376hp1t FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_test_steps fk_htntijiqqt0ec2nuxfnohijrd; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps
    ADD CONSTRAINT fk_htntijiqqt0ec2nuxfnohijrd FOREIGN KEY (test_steps_option_id) REFERENCES public.tm_test_steps_option(id);


--
-- Name: tm_web_service_configuration fk_hw6hksbj3pn9g025bxbsgl6i7; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_configuration
    ADD CONSTRAINT fk_hw6hksbj3pn9g025bxbsgl6i7 FOREIGN KEY (ws_transaction_usage) REFERENCES public.tf_ws_transaction_usage(id);


--
-- Name: tm_object_reader_aud fk_i17aunwkunfm26o22uqob58iy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_reader_aud
    ADD CONSTRAINT fk_i17aunwkunfm26o22uqob58iy FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_user_preferences fk_i5d14il8ihrkj9n1yjontwuux; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_preferences
    ADD CONSTRAINT fk_i5d14il8ihrkj9n1yjontwuux FOREIGN KEY (selected_testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_oid_requirement fk_i94kdks47uoatdc26pf3y2y9y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_requirement
    ADD CONSTRAINT fk_i94kdks47uoatdc26pf3y2y9y FOREIGN KEY (oid_root_definition_id) REFERENCES public.tm_oid_root_definition(id);


--
-- Name: tf_actor_integration_profile_option fk_ichumu56164vfu1j44qb0uos3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_ichumu56164vfu1j44qb0uos3 FOREIGN KEY (integration_profile_option_id) REFERENCES public.tf_integration_profile_option(id);


--
-- Name: tm_step_instance_message fk_ifx28c1u92apr606e2naos3ki; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_ifx28c1u92apr606e2naos3ki FOREIGN KEY (tm_test_step_message_profile_id) REFERENCES public.tm_test_step_message_profile(id);


--
-- Name: tf_document_sections fk_iqelub40m4bwli6jc4p1gahw0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections
    ADD CONSTRAINT fk_iqelub40m4bwli6jc4p1gahw0 FOREIGN KEY (document_id) REFERENCES public.tf_document(id);


--
-- Name: tf_rule_criterion fk_j241r3eqgmslvierka38b8g0n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_rule_criterion
    ADD CONSTRAINT fk_j241r3eqgmslvierka38b8g0n FOREIGN KEY (aiporule_id) REFERENCES public.tf_rule(id);


--
-- Name: tm_meta_test_test_roles fk_j44487m84na9g0oiiu4wq43u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles
    ADD CONSTRAINT fk_j44487m84na9g0oiiu4wq43u FOREIGN KEY (meta_test_id) REFERENCES public.tm_meta_test(id);


--
-- Name: tf_domain_profile_aud fk_j86yg7l48lurifx013ijn01hj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_domain_profile_aud
    ADD CONSTRAINT fk_j86yg7l48lurifx013ijn01hj FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_transaction_option_type_aud fk_j92xo8oyqun96s7ct4dxoqj21; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_option_type_aud
    ADD CONSTRAINT fk_j92xo8oyqun96s7ct4dxoqj21 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_object_instance_annotation fk_jegpl0w8wjrfvliavg9g9k0xy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_annotation
    ADD CONSTRAINT fk_jegpl0w8wjrfvliavg9g9k0xy FOREIGN KEY (object_instance_id) REFERENCES public.tm_object_instance(id);


--
-- Name: tm_test_types_sessions fk_jeticaxprq8ctgjwjrl4upanv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_types_sessions
    ADD CONSTRAINT fk_jeticaxprq8ctgjwjrl4upanv FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_configuration_type_aud fk_k2nts9kv5ltrg3is0tuk2nc87; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_type_aud
    ADD CONSTRAINT fk_k2nts9kv5ltrg3is0tuk2nc87 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: pr_search_log_systems_found fk_k4w3vge0p53q44ef9kwq9lesw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_log_systems_found
    ADD CONSTRAINT fk_k4w3vge0p53q44ef9kwq9lesw FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_test_instance fk_k9841xy3mx4t4oe0j0gba42i4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance
    ADD CONSTRAINT fk_k9841xy3mx4t4oe0j0gba42i4 FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tf_document fk_kghw5fo7w5wbq5ayl4bo1hwok; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document
    ADD CONSTRAINT fk_kghw5fo7w5wbq5ayl4bo1hwok FOREIGN KEY (document_domain_id) REFERENCES public.tf_domain(id);


--
-- Name: pr_mail fk_khmdmvjary6ia8u4oxvpb672y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_mail
    ADD CONSTRAINT fk_khmdmvjary6ia8u4oxvpb672y FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tf_actor_default_ports fk_kirnjsxowiurs62dcw84g9sy8; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_default_ports
    ADD CONSTRAINT fk_kirnjsxowiurs62dcw84g9sy8 FOREIGN KEY (actor_id) REFERENCES public.tf_actor(id);


--
-- Name: tm_testing_session fk_kmghkdnf8gxqp5l5w656qikdc; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session
    ADD CONSTRAINT fk_kmghkdnf8gxqp5l5w656qikdc FOREIGN KEY (networkconfiguration_id) REFERENCES public.tm_network_config_for_session(id);


--
-- Name: tm_oid_institution fk_kngpo6n5gql6s9si9g4jyy1r9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_institution
    ADD CONSTRAINT fk_kngpo6n5gql6s9si9g4jyy1r9 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tf_audit_message fk_kqshjsm4nxkadcwjw3d3csiny; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_kqshjsm4nxkadcwjw3d3csiny FOREIGN KEY (issuing_actor) REFERENCES public.tf_actor(id);


--
-- Name: tm_user_preferences fk_ks4okv9ariahgb5n0k3d3esw9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_user_preferences
    ADD CONSTRAINT fk_ks4okv9ariahgb5n0k3d3esw9 FOREIGN KEY (userphoto_id) REFERENCES public.tm_user_photo(id);


--
-- Name: tf_integration_profile_status_type_aud fk_kuduyri7glq93006crkrnufkw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_status_type_aud
    ADD CONSTRAINT fk_kuduyri7glq93006crkrnufkw FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_actor_integration_profile_option fk_l1fdaa86cbvmpqmp1q3f0hwcm; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option
    ADD CONSTRAINT fk_l1fdaa86cbvmpqmp1q3f0hwcm FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- Name: tf_actor_integration_profile_option_aud fk_l3r8wknk5xpfwpagfodbrevi2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_actor_integration_profile_option_aud
    ADD CONSTRAINT fk_l3r8wknk5xpfwpagfodbrevi2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_monitor_test fk_la7va5x4pm97bbtpuseg85eb2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_monitor_test
    ADD CONSTRAINT fk_la7va5x4pm97bbtpuseg85eb2 FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tm_test_participants_aud fk_lal4j6ndlklnfbtg5pebpvixq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_participants_aud
    ADD CONSTRAINT fk_lal4j6ndlklnfbtg5pebpvixq FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_aipo_result_for_a_testing_session fk_lh5niyx5y4tsk7p9pfbetqcwa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_aipo_result_for_a_testing_session
    ADD CONSTRAINT fk_lh5niyx5y4tsk7p9pfbetqcwa FOREIGN KEY (system_actor_profile_id) REFERENCES public.tm_system_actor_profiles(id);


--
-- Name: tm_demonstrations_in_testing_sessions fk_lieh7w7rgtbbv3fsajnokw4wa; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstrations_in_testing_sessions
    ADD CONSTRAINT fk_lieh7w7rgtbbv3fsajnokw4wa FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_object_instance_attribute fk_liwpi61feu2ytqxrgqcyyg859; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_attribute
    ADD CONSTRAINT fk_liwpi61feu2ytqxrgqcyyg859 FOREIGN KEY (attribute_id) REFERENCES public.tm_object_attribute(id);


--
-- Name: tm_system_events fk_lqcivp5bh2olqqdmy9jah4gor; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_events
    ADD CONSTRAINT fk_lqcivp5bh2olqqdmy9jah4gor FOREIGN KEY (systemevents_id) REFERENCES public.tm_system_event(id);


--
-- Name: tm_object_attribute_aud fk_ls8c5t68q91vgsmp6h52mu9vg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_attribute_aud
    ADD CONSTRAINT fk_ls8c5t68q91vgsmp6h52mu9vg FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_instance_test_instance_path_to_log_file fk_lvqtq11ve10tm8ibq8d24a6n; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_instance_path_to_log_file
    ADD CONSTRAINT fk_lvqtq11ve10tm8ibq8d24a6n FOREIGN KEY (test_instance_id) REFERENCES public.tm_test_instance(id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class fk_lw5wptdonu1q59obxe8ooswv0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT fk_lw5wptdonu1q59obxe8ooswv0 FOREIGN KEY (configurationtype_id) REFERENCES public.tm_configuration_type(id);


--
-- Name: tm_test_test_steps fk_m1ecsldv3cwqsm4j4olutynva; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_steps
    ADD CONSTRAINT fk_m1ecsldv3cwqsm4j4olutynva FOREIGN KEY (test_steps_id) REFERENCES public.tm_test_steps(id);


--
-- Name: tm_testing_session_admin fk_m3o27nlw1nlnan9tb9avku02i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session_admin
    ADD CONSTRAINT fk_m3o27nlw1nlnan9tb9avku02i FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_test_instance_participants fk_m7xtyf7tm5d1jsy9jk5oauc9a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_participants
    ADD CONSTRAINT fk_m7xtyf7tm5d1jsy9jk5oauc9a FOREIGN KEY (system_in_session_user_id) REFERENCES public.tm_system_in_session_user(id);


--
-- Name: tm_test_steps_instance fk_ma8wa4g8sn4i7qetctmwhlnvj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT fk_ma8wa4g8sn4i7qetctmwhlnvj FOREIGN KEY (execution_status_id) REFERENCES public.tm_step_instance_exec_status(id);


--
-- Name: sys_system_subtypes_per_system_type fk_mfo2jk254s1f9b5t2cwp44yei; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_system_subtypes_per_system_type
    ADD CONSTRAINT fk_mfo2jk254s1f9b5t2cwp44yei FOREIGN KEY (system_type_id) REFERENCES public.sys_system_type(id);


--
-- Name: tm_object_instance_annotation fk_mr9lu379jopp7l22k5lw790ax; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_annotation
    ADD CONSTRAINT fk_mr9lu379jopp7l22k5lw790ax FOREIGN KEY (annotation_id) REFERENCES public.tm_annotation(id);


--
-- Name: tm_object_instance fk_msmtjuieytyw7mkr6e29cus55; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance
    ADD CONSTRAINT fk_msmtjuieytyw7mkr6e29cus55 FOREIGN KEY (test_type_id) REFERENCES public.tm_test_type(id);


--
-- Name: tm_host fk_mu0t4y2aclf72oeviw4h7t5xg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_host
    ADD CONSTRAINT fk_mu0t4y2aclf72oeviw4h7t5xg FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_object_instance_file fk_muco5r9r47comq5pdejgyq0v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_file
    ADD CONSTRAINT fk_muco5r9r47comq5pdejgyq0v FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_test_steps_instance fk_n0kvbowvki1af3a6ku0lq2ixq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT fk_n0kvbowvki1af3a6ku0lq2ixq FOREIGN KEY (test_steps_id) REFERENCES public.tm_test_steps(id);


--
-- Name: tm_test_steps_instance_input_ci_instance fk_n59hqaegg354gmwaw6waqv92l; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance_input_ci_instance
    ADD CONSTRAINT fk_n59hqaegg354gmwaw6waqv92l FOREIGN KEY (contextual_information_instance_id) REFERENCES public.tm_contextual_information_instance(id);


--
-- Name: usr_user_role fk_n7gxjfdul9jid245847muudoy; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT fk_n7gxjfdul9jid245847muudoy FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_test_test_description fk_n8mdra84dv22qsgmkefw8megq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_test_description
    ADD CONSTRAINT fk_n8mdra84dv22qsgmkefw8megq FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: pr_search_log_systems_found fk_n9rqmdq9h6s5wcwen16gnnt2y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_search_log_systems_found
    ADD CONSTRAINT fk_n9rqmdq9h6s5wcwen16gnnt2y FOREIGN KEY (search_report_id) REFERENCES public.pr_search_log_report(id);


--
-- Name: tm_path_aud fk_nennvs171bi7s7op3n7gmnlbi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_path_aud
    ADD CONSTRAINT fk_nennvs171bi7s7op3n7gmnlbi FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_subtypes_per_system fk_nhn135cn73i6ok4dks41yvx4s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_subtypes_per_system
    ADD CONSTRAINT fk_nhn135cn73i6ok4dks41yvx4s FOREIGN KEY (system_subtypes_per_system_type_id) REFERENCES public.sys_system_subtypes_per_system_type(id);


--
-- Name: tm_meta_test_aud fk_nihnqthjjtmy4tuum1oatjyw5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_aud
    ADD CONSTRAINT fk_nihnqthjjtmy4tuum1oatjyw5 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_configuration_mapped_with_aipo fk_nlrdxrud9haf3aoqtwbv20mf0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_configuration_mapped_with_aipo
    ADD CONSTRAINT fk_nlrdxrud9haf3aoqtwbv20mf0 FOREIGN KEY (aipo_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tf_transaction fk_nmsbssdcd0ll5g9dvehic1cdg; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction
    ADD CONSTRAINT fk_nmsbssdcd0ll5g9dvehic1cdg FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: tm_test_steps_data fk_nmscf6j85yxg8byy7o6fxj4n9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_data
    ADD CONSTRAINT fk_nmscf6j85yxg8byy7o6fxj4n9 FOREIGN KEY (data_type_id) REFERENCES public.tm_data_type(id);


--
-- Name: tm_system fk_noppdjafjokxk0exqc0p6mgsw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system
    ADD CONSTRAINT fk_noppdjafjokxk0exqc0p6mgsw FOREIGN KEY (system_type_id) REFERENCES public.sys_system_type(id);


--
-- Name: tf_integration_profile_type_aud fk_np3xu3ya9brlrhx2jxadk1gl6; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile_type_aud
    ADD CONSTRAINT fk_np3xu3ya9brlrhx2jxadk1gl6 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_step_instance_message fk_nuq0tu6poiuwa2m8we4iucmi; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_nuq0tu6poiuwa2m8we4iucmi FOREIGN KEY (sender_host_id) REFERENCES public.tm_host(id);


--
-- Name: tm_institution_system fk_nwmrmlv2n1tb6evu96fenqydq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_institution_system
    ADD CONSTRAINT fk_nwmrmlv2n1tb6evu96fenqydq FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_test_steps_instance fk_nxratne8p854xc4y96ce27igj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_instance
    ADD CONSTRAINT fk_nxratne8p854xc4y96ce27igj FOREIGN KEY (system_in_session_initiator_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: usr_users fk_nyqv47jtq7254b9y0kq55g0p4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_users
    ADD CONSTRAINT fk_nyqv47jtq7254b9y0kq55g0p4 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_step_instance_message fk_o0rwou91h3807kd2y7pmokfcf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_o0rwou91h3807kd2y7pmokfcf FOREIGN KEY (tm_test_step_instance_id) REFERENCES public.tm_test_steps_instance(id);


--
-- Name: tm_jira_issues_to_test_aud fk_ocbykg3yewg11r6e3ax39ap62; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_jira_issues_to_test_aud
    ADD CONSTRAINT fk_ocbykg3yewg11r6e3ax39ap62 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_syslog_configuration fk_ocvgk0pa31pbq5j48jnpkjr2j; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_syslog_configuration
    ADD CONSTRAINT fk_ocvgk0pa31pbq5j48jnpkjr2j FOREIGN KEY (transport_layer_id) REFERENCES public.tm_transport_layer_for_config(id);


--
-- Name: tm_invoice fk_ohx2b3k5dy7dk6srai3xxe7i2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT fk_ohx2b3k5dy7dk6srai3xxe7i2 FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_sap_result_tr_comment fk_ohydmmym8xeemdov67b3if3kp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sap_result_tr_comment
    ADD CONSTRAINT fk_ohydmmym8xeemdov67b3if3kp FOREIGN KEY (tr_id) REFERENCES public.tm_test_roles(id);


--
-- Name: tm_object_creator_aud fk_ojka8mg54bum99vrds2wn4nar; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_creator_aud
    ADD CONSTRAINT fk_ojka8mg54bum99vrds2wn4nar FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_network_config_for_session fk_ollwjxfh9b0krgxki6lumkejv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_network_config_for_session
    ADD CONSTRAINT fk_ollwjxfh9b0krgxki6lumkejv FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tm_contextual_information_aud fk_ox51ucd6916jemwf3macupb1v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_contextual_information_aud
    ADD CONSTRAINT fk_ox51ucd6916jemwf3macupb1v FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_subtypes_per_system fk_oyeqw9rxxe4ijvvvb90ht0yte; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_subtypes_per_system
    ADD CONSTRAINT fk_oyeqw9rxxe4ijvvvb90ht0yte FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_test_step_message_profile fk_p7arm8awsahr6cmr4csbqlsfp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_step_message_profile
    ADD CONSTRAINT fk_p7arm8awsahr6cmr4csbqlsfp FOREIGN KEY (tm_test_steps_id) REFERENCES public.tm_test_steps(id);


--
-- Name: tm_test_description fk_pbtvrjyr45ojwi5j2na8y6mkq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_description
    ADD CONSTRAINT fk_pbtvrjyr45ojwi5j2na8y6mkq FOREIGN KEY (gazelle_language_id) REFERENCES public.usr_gazelle_language(id);


--
-- Name: tm_test_roles_aud fk_pc4iiwm2b903iwx7oogc9cd0i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_roles_aud
    ADD CONSTRAINT fk_pc4iiwm2b903iwx7oogc9cd0i FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_steps_output_ci_aud fk_peh518reooej1bgy0uq1yrqw1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_output_ci_aud
    ADD CONSTRAINT fk_peh518reooej1bgy0uq1yrqw1 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_testing_session fk_peyxbcqtu2y8scuc96mdpbxow; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_testing_session
    ADD CONSTRAINT fk_peyxbcqtu2y8scuc96mdpbxow FOREIGN KEY (address_id) REFERENCES public.usr_address(id);


--
-- Name: tf_hl7_message_profile_affinity_domain fk_pfco9j3tuok8p2pxb7jrqh47y; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_affinity_domain
    ADD CONSTRAINT fk_pfco9j3tuok8p2pxb7jrqh47y FOREIGN KEY (hl7_message_profile_id) REFERENCES public.tf_hl7_message_profile(id);


--
-- Name: tm_role_in_test_test_participants_aud fk_phxy9nfj6wts9kcrm9nqkotdw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_test_participants_aud
    ADD CONSTRAINT fk_phxy9nfj6wts9kcrm9nqkotdw FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_step_instance_message fk_pjja9bj6ln7n6f1iy7350jjjh; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_pjja9bj6ln7n6f1iy7350jjjh FOREIGN KEY (receiver_host_id) REFERENCES public.tm_host(id);


--
-- Name: usr_institution_address fk_pkeq7ywr9281p26l7fgkom3df; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution_address
    ADD CONSTRAINT fk_pkeq7ywr9281p26l7fgkom3df FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_object_file_type_aud fk_pn9t12u2cf3g4vto7u89y9dth; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_file_type_aud
    ADD CONSTRAINT fk_pn9t12u2cf3g4vto7u89y9dth FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_in_session_user fk_pqb7g2f0lw5upbumnaauvopwv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session_user
    ADD CONSTRAINT fk_pqb7g2f0lw5upbumnaauvopwv FOREIGN KEY (user_id) REFERENCES public.usr_users(id);


--
-- Name: tm_demonstrations_in_testing_sessions fk_q268jj2ritmaqhn5wgc88b3q0; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstrations_in_testing_sessions
    ADD CONSTRAINT fk_q268jj2ritmaqhn5wgc88b3q0 FOREIGN KEY (demonstration_id) REFERENCES public.tm_demonstration(id);


--
-- Name: tm_test_instance_test_steps_instance fk_q27fxsv78duwivajsy79pwmd5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance_test_steps_instance
    ADD CONSTRAINT fk_q27fxsv78duwivajsy79pwmd5 FOREIGN KEY (test_steps_instance_id) REFERENCES public.tm_test_steps_instance(id);


--
-- Name: tm_demonstration_system_in_session fk_q2r1knr42gwluam0uta8cyud1; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration_system_in_session
    ADD CONSTRAINT fk_q2r1knr42gwluam0uta8cyud1 FOREIGN KEY (demonstration_id) REFERENCES public.tm_demonstration(id);


--
-- Name: sys_hl7_documents_for_systems fk_qdguie6pvgkifk5o09gxte21v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.sys_hl7_documents_for_systems
    ADD CONSTRAINT fk_qdguie6pvgkifk5o09gxte21v FOREIGN KEY (path_linking_a_document_id) REFERENCES public.cmn_path_linking_a_document(id);


--
-- Name: tm_test_instance fk_qeijgpdnuwptql64jtakwvssl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_instance
    ADD CONSTRAINT fk_qeijgpdnuwptql64jtakwvssl FOREIGN KEY (execution_status_id) REFERENCES public.tm_test_instance_exec_status(id);


--
-- Name: tm_role_in_test_test_participants fk_qj6unnr6faycfvkjxk68sqt9; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_test_participants
    ADD CONSTRAINT fk_qj6unnr6faycfvkjxk68sqt9 FOREIGN KEY (role_in_test_id) REFERENCES public.tm_role_in_test(id);


--
-- Name: tm_oid_system_assignment fk_qn46egrt4hjpopxfqaer41141; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_oid_system_assignment
    ADD CONSTRAINT fk_qn46egrt4hjpopxfqaer41141 FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_test_steps_input_ci_aud fk_qngdwbmhr0utolrvcd2y9yoe2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_steps_input_ci_aud
    ADD CONSTRAINT fk_qngdwbmhr0utolrvcd2y9yoe2 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_conf_mapping_w_aipo_w_conftypes_aud fk_qp2aih0y4eqjckklxop8ali7w; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conf_mapping_w_aipo_w_conftypes_aud
    ADD CONSTRAINT fk_qp2aih0y4eqjckklxop8ali7w FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_hl7_message_profile_aud fk_qx4njevahh67dplugi2iqgn9f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_hl7_message_profile_aud
    ADD CONSTRAINT fk_qx4njevahh67dplugi2iqgn9f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage_aud fk_qxy2usxp9p277j5m1gyjgrlxv; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage_aud
    ADD CONSTRAINT fk_qxy2usxp9p277j5m1gyjgrlxv FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_step_instance_message fk_r2riual1pmeubppuln4sds7tn; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_message
    ADD CONSTRAINT fk_r2riual1pmeubppuln4sds7tn FOREIGN KEY (receiver_tm_configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class fk_r6ge0q8jxhcc0tpihadhqwqyf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT fk_r6ge0q8jxhcc0tpihadhqwqyf FOREIGN KEY (transport_layer_id) REFERENCES public.tm_transport_layer_for_config(id);


--
-- Name: usr_user_role fk_r98ip7hgg258gxm6jk9oj0v6t; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_user_role
    ADD CONSTRAINT fk_r98ip7hgg258gxm6jk9oj0v6t FOREIGN KEY (role_id) REFERENCES public.usr_role(id);


--
-- Name: tf_document_sections_aud fk_rfgwvh14e22kxi49wt8rtus68; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_document_sections_aud
    ADD CONSTRAINT fk_rfgwvh14e22kxi49wt8rtus68 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_system_in_session fk_rpn1pj7pohlr5nuwpb4e5exp3; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT fk_rpn1pj7pohlr5nuwpb4e5exp3 FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_test_participants fk_rq8l80icaxhf7nia0ae4eyyor; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_participants
    ADD CONSTRAINT fk_rq8l80icaxhf7nia0ae4eyyor FOREIGN KEY (actor_integration_profile_option_id) REFERENCES public.tf_actor_integration_profile_option(id);


--
-- Name: tm_role_in_test_aud fk_rqwjjlu9fa5ugx3p08ku3mdga; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_role_in_test_aud
    ADD CONSTRAINT fk_rqwjjlu9fa5ugx3p08ku3mdga FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_web_service_configuration fk_rr1cirqxrhgbtlwvrnypyumum; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_configuration
    ADD CONSTRAINT fk_rr1cirqxrhgbtlwvrnypyumum FOREIGN KEY (webservice_type) REFERENCES public.tm_web_service_type(id);


--
-- Name: tm_dicom_scp_configuration fk_rshfckfpohwrc46smcjaf5ccl; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scp_configuration
    ADD CONSTRAINT fk_rshfckfpohwrc46smcjaf5ccl FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_meta_test_test_roles fk_rttbxqqdc0g4p8i9wp4ady0m5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_meta_test_test_roles
    ADD CONSTRAINT fk_rttbxqqdc0g4p8i9wp4ady0m5 FOREIGN KEY (test_roles_id) REFERENCES public.tm_test_roles(id);


--
-- Name: tf_transaction_standard fk_ru5fqa00qtwrerdt1oqqsjlv4; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_standard
    ADD CONSTRAINT fk_ru5fqa00qtwrerdt1oqqsjlv4 FOREIGN KEY (standard_id) REFERENCES public.tf_standard(id);


--
-- Name: tm_system_actor_profiles fk_rxho3xai0u6jtei53ujvoncrj; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_actor_profiles
    ADD CONSTRAINT fk_rxho3xai0u6jtei53ujvoncrj FOREIGN KEY (testing_type_id) REFERENCES public.tm_testing_depth(id);


--
-- Name: tm_profiles_in_testing_sessions fk_ryh6739nw0ym9un3g7wjj7agp; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_profiles_in_testing_sessions
    ADD CONSTRAINT fk_ryh6739nw0ym9un3g7wjj7agp FOREIGN KEY (integration_profile_id) REFERENCES public.tf_integration_profile(id);


--
-- Name: tm_conftype_w_ports_wstype_and_sop_class fk_ryuag4lisoh65iqs1qwu1d8u5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_conftype_w_ports_wstype_and_sop_class
    ADD CONSTRAINT fk_ryuag4lisoh65iqs1qwu1d8u5 FOREIGN KEY (web_service_type_id) REFERENCES public.tm_web_service_type(id);


--
-- Name: pr_crawler_changed_integration_statements fk_s296674hwdluo6scebjkp712u; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_changed_integration_statements
    ADD CONSTRAINT fk_s296674hwdluo6scebjkp712u FOREIGN KEY (system_id) REFERENCES public.tm_system(id);


--
-- Name: tm_object_instance fk_s42g4w0ccbhobtpeg9vytc3wf; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance
    ADD CONSTRAINT fk_s42g4w0ccbhobtpeg9vytc3wf FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tf_integration_profile fk_s62lad7mgha6mk7y8d1j4ww6a; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_integration_profile
    ADD CONSTRAINT fk_s62lad7mgha6mk7y8d1j4ww6a FOREIGN KEY (documentsection) REFERENCES public.tf_document_sections(id);


--
-- Name: tm_sap_result_tr_comment fk_s6tj5xdwhrgk9fq1f8ewoa0ul; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_sap_result_tr_comment
    ADD CONSTRAINT fk_s6tj5xdwhrgk9fq1f8ewoa0ul FOREIGN KEY (sap_id) REFERENCES public.tm_system_aipo_result_for_a_testing_session(id);


--
-- Name: usr_messages_usr_users fk_s9eac53wxfkvq1gk2wjdurn38; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_messages_usr_users
    ADD CONSTRAINT fk_s9eac53wxfkvq1gk2wjdurn38 FOREIGN KEY (users_id) REFERENCES public.usr_users(id);


--
-- Name: tm_dicom_scu_configuration fk_sbi4lm2rfpgoo61sr7bi7635s; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_dicom_scu_configuration
    ADD CONSTRAINT fk_sbi4lm2rfpgoo61sr7bi7635s FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: pr_crawler_unmatching_hascode_for_integration_statements fk_sciat1llx9q7c392k7a3ea77c; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.pr_crawler_unmatching_hascode_for_integration_statements
    ADD CONSTRAINT fk_sciat1llx9q7c392k7a3ea77c FOREIGN KEY (crawler_report_id) REFERENCES public.pr_crawler_reporting(id);


--
-- Name: usr_institution fk_se17muri3hx8svymngo25ope5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_institution
    ADD CONSTRAINT fk_se17muri3hx8svymngo25ope5 FOREIGN KEY (mailing_address_id) REFERENCES public.usr_address(id);


--
-- Name: tm_web_service_type_aud fk_ssmkb6wdvl992bxc62brooh51; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_web_service_type_aud
    ADD CONSTRAINT fk_ssmkb6wdvl992bxc62brooh51 FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_demonstration fk_t0rumxjr2ogajp7htl30wb9ky; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration
    ADD CONSTRAINT fk_t0rumxjr2ogajp7htl30wb9ky FOREIGN KEY (country) REFERENCES public.usr_iso_3166_country_code(iso);


--
-- Name: usr_person fk_t0uqd6j2ihsc1rksmbtxso3qx; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.usr_person
    ADD CONSTRAINT fk_t0uqd6j2ihsc1rksmbtxso3qx FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_object_instance_file fk_t1r20yd6nvitdg4avapkq5lgk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_object_instance_file
    ADD CONSTRAINT fk_t1r20yd6nvitdg4avapkq5lgk FOREIGN KEY (file_id) REFERENCES public.tm_object_file(id);


--
-- Name: tm_system_in_session fk_t4l59pbfpfvikgmtw0f1oscuq; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_system_in_session
    ADD CONSTRAINT fk_t4l59pbfpfvikgmtw0f1oscuq FOREIGN KEY (testing_session_id) REFERENCES public.tm_testing_session(id);


--
-- Name: tf_profile_link_aud fk_tbg8e3v0117w55eah8dspnd4f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_profile_link_aud
    ADD CONSTRAINT fk_tbg8e3v0117w55eah8dspnd4f FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tm_test_user_comment fk_tkfw03p8c3spmpu3gwf5yimn5; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_user_comment
    ADD CONSTRAINT fk_tkfw03p8c3spmpu3gwf5yimn5 FOREIGN KEY (test_id) REFERENCES public.tm_test(id);


--
-- Name: tm_demonstration_system_in_session fk_tkgpwjs8907htf8yw3a511y4i; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_demonstration_system_in_session
    ADD CONSTRAINT fk_tkgpwjs8907htf8yw3a511y4i FOREIGN KEY (system_in_session_id) REFERENCES public.tm_system_in_session(id);


--
-- Name: tm_invoice fk_tkuxf07nj6onh9ewonb4b1ey2; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_invoice
    ADD CONSTRAINT fk_tkuxf07nj6onh9ewonb4b1ey2 FOREIGN KEY (institution_id) REFERENCES public.usr_institution(id);


--
-- Name: tm_hl7_initiator_configuration fk_tmy114lg4ja4513atca5hshlo; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_hl7_initiator_configuration
    ADD CONSTRAINT fk_tmy114lg4ja4513atca5hshlo FOREIGN KEY (configuration_id) REFERENCES public.tm_configuration(id);


--
-- Name: tm_test_types_sessions fk_tnbs6to19xl7kgmnb385f4wih; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_test_types_sessions
    ADD CONSTRAINT fk_tnbs6to19xl7kgmnb385f4wih FOREIGN KEY (test_type_id) REFERENCES public.tm_test_type(id);


--
-- Name: tf_transaction_status_type_aud fk_to2vmdf96j8l3ek5jv1sf1qul; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_transaction_status_type_aud
    ADD CONSTRAINT fk_to2vmdf96j8l3ek5jv1sf1qul FOREIGN KEY (rev) REFERENCES public.revinfo(rev);


--
-- Name: tf_ws_transaction_usage fk_tqey90ukaddb48sit5ikipdps; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_ws_transaction_usage
    ADD CONSTRAINT fk_tqey90ukaddb48sit5ikipdps FOREIGN KEY (transaction_id) REFERENCES public.tf_transaction(id);


--
-- Name: tm_step_instance_msg_validation fk_tqv6bgjhpj50iyioxo3hbddwk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tm_step_instance_msg_validation
    ADD CONSTRAINT fk_tqv6bgjhpj50iyioxo3hbddwk FOREIGN KEY (tm_step_instance_message_id) REFERENCES public.tm_step_instance_message(id);


--
-- Name: tf_audit_message fk_ululbp46ig0t093pgkr38y6h; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY public.tf_audit_message
    ADD CONSTRAINT fk_ululbp46ig0t093pgkr38y6h FOREIGN KEY (document_section) REFERENCES public.tf_document_sections(id);


--
-- PostgreSQL database dump complete
--

