#!/bin/bash
set -e

cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")"

psql --username "$POSTGRES_USER" --dbname "gazelle" <<-EOSQL
    CREATE EXTENSION pgcrypto;
EOSQL

psql --username "gazelle" --dbname "gazelle" < ./sql/schema.sql
psql --username "gazelle" --dbname "gazelle" < ./sql/init.sql
psql --username "gazelle" --dbname "gazelle" < ./sql/init_data.sql

psql --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    create database keycloak;
    create user keycloak with encrypted password 'keycloak';
    grant all privileges on database keycloak to keycloak;
EOSQL
