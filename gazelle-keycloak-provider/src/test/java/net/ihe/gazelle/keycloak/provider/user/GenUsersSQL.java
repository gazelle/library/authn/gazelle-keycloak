package net.ihe.gazelle.keycloak.provider.user;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GenUsersSQL {

    public static void main(String[] args) throws IOException {
        try (var fw = new FileWriter(new File("/tmp/users.sql"));
             var bw = new BufferedWriter(fw)) {
            for (int i = 0; i < 5000; i++) {
                String username = "userA" + i;
                bw.write("insert into usr_users (id,email,password,username,firstname,lastname,institution_id,activated,blocked) values\n" +
                        "    (nextval('usr_users_id_seq'),'" + username + "@gazelle.com',MD5('aZeRtY')," +
                        "'" + username + "','" + username + " fn','" + username + " ln',\n" +
                        "        (SELECT  id from usr_institution where keyword = 'KEREVAL'),true,false);\n");
                bw.write("insert into usr_user_role (user_id,role_id) values\n" +
                        "    ((select id from usr_users where username = '" + username + "'), (select id from usr_role where name ='monitor_role'));\n");
                bw.write("insert into usr_user_role (user_id,role_id) values\n" +
                        "    ((select id from usr_users where username = '" + username + "'), (select id from usr_role where name ='vendor_role'));\n");
                bw.write("insert into usr_user_role (user_id,role_id) values\n" +
                        "    ((select id from usr_users where username = '" + username + "'), (select id from usr_role where name ='user_role'));");
            }
        }
    }

}
