package net.ihe.gazelle.keycloak.provider.user.tests.util;

import org.keycloak.component.ComponentModel;

public class GazelleComponentModel extends ComponentModel {

    public GazelleComponentModel() {
        super();
        setId("id");
        setName("name");
        setProviderId("providerid");
        setProviderType("providertype");
        setParentId("parentid");
        setSubType("subtype");
    }
}
