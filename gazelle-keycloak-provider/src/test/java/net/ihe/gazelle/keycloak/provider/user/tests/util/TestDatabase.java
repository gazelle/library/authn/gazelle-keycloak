package net.ihe.gazelle.keycloak.provider.user.tests.util;

import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import lombok.SneakyThrows;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleQueryExecutor;
import org.apache.ibatis.jdbc.ScriptRunner;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TestDatabase {

    public static final TestDatabase INSTANCE = new TestDatabase();

    public static final String DRIVER_CLASS = "org.postgresql.Driver";

    private final EmbeddedPostgres postgres;

    public static void main(String[] args) {
        System.out.println(INSTANCE.getJdbcUrl());
    }

    @SneakyThrows
    protected TestDatabase() {
        Class.forName(DRIVER_CLASS);
        postgres = EmbeddedPostgres.start();

        new GazelleQueryExecutor(() -> postgres.getPostgresDatabase().getConnection())
                .executeUpdate("CREATE EXTENSION pgcrypto;", List.of());

        new GazelleQueryExecutor(() -> postgres.getPostgresDatabase().getConnection())
                .executeUpdate("create user gazelle with encrypted password 'gazelle';", List.of());

        ScriptRunner sr = new ScriptRunner(postgres.getPostgresDatabase().getConnection());
        runScript(sr, "/postgres/sql/schema.sql");
        runScript(sr, "/postgres/sql/init.sql");
        runScript(sr, "/postgres/sql/init_data.sql");
    }

    private void runScript(ScriptRunner sr, String sqlFile) {
        InputStream resourceAsStream = TestDatabase.class.getResourceAsStream(sqlFile);
        sr.runScript(new InputStreamReader(resourceAsStream));
    }

    public Connection getConnection() throws SQLException {
        return postgres.getPostgresDatabase().getConnection();
    }

    public String getJdbcUrl() {
        return postgres.getJdbcUrl("postgres", "postgres");
    }

    public String getJdbcUsername() {
        return "postgres";
    }

    public String getJdbcPassword() {
        return "postgres";
    }
}
