package net.ihe.gazelle.keycloak.provider.user.service;

import lombok.SneakyThrows;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleSQLException;
import net.ihe.gazelle.keycloak.provider.user.groups.GroupService;
import net.ihe.gazelle.keycloak.provider.user.roles.RoleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.storage.federated.UserFederatedStorageProvider;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GazelleUserServiceTest {

    @Mock
    KeycloakSession session;

    @Mock
    ComponentModel storageProviderModel;

    @Mock
    protected GroupService groupService;

    @Mock
    protected RoleService roleService;

    @Mock
    GazelleDAO gazelleDAO;

    @InjectMocks
    GazelleUserService gazelleUserService;

    @Mock
    RealmModel realm;

    @Mock
    ResultSet rs;

    @Mock
    UserFederatedStorageProvider userFederatedStorageProvider;

    @Mock
    UserModel user1;

    @Mock
    GroupModel group;

    @Test
    @SneakyThrows
    void mapUser() {
        when(storageProviderModel.getId()).thenReturn("gazelle");
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        when(rs.getString("username")).thenReturn("user1");
        when(rs.getString("email")).thenReturn("user1@gmail.com");
        when(rs.getString("firstname")).thenReturn("firstname");
        when(rs.getString("lastname")).thenReturn("lastname");
        UserModel user = gazelleUserService.mapUser(realm, rs);
        assertEquals("f:gazelle:user1", user.getId());
        assertEquals("user1", user.getUsername());
    }

    @Test
    void getUserByUsername() {
        when(gazelleDAO.getUserByUsername(eq("user1"), any())).thenReturn(user1);
        assertEquals(user1, gazelleUserService.getUserByUsername(realm, "user1"));
    }

    @Test
    void getUserByEmail() {
        when(gazelleDAO.getUserByEmail(eq("user1@gmail.com"), any())).thenReturn(user1);
        assertEquals(user1, gazelleUserService.getUserByEmail(realm, "user1@gmail.com"));
    }

    @Test
    void updatePassword1() {
        when(gazelleDAO.updatePasswordForUsername("user1", "gazelle")).thenReturn(true);
        assertTrue(gazelleUserService.updatePassword("user1", "gazelle"));
    }

    @Test
    void updatePassword2() {
        when(gazelleDAO.updatePasswordForUsername("user1", "gazelle")).thenReturn(false);
        assertFalse(gazelleUserService.updatePassword("user1", "gazelle"));
    }

    @Test
    void validatePassword1() {
        when(gazelleDAO.verifyPasswordForUsername("user1", "gazelle")).thenReturn(false);
        assertFalse(gazelleUserService.validatePassword("user1", "gazelle"));
    }

    @Test
    void validatePassword2() {
        when(gazelleDAO.verifyPasswordForUsername("user1", "gazelle")).thenReturn(true);
        assertTrue(gazelleUserService.validatePassword("user1", "gazelle"));
    }

    @Test
    void getUsersCount() {
        when(gazelleDAO.getUsersCount()).thenReturn(1234);
        assertEquals(1234, gazelleUserService.getUsersCount());
    }

    @Test
    void getUsersCountNull() {
        when(gazelleDAO.getUsersCount()).thenReturn(null);
        assertEquals(0, gazelleUserService.getUsersCount());
    }

    @Test
    void getUsersCount2() {
        when(gazelleDAO.getUsersCount("abcd", Set.of("a", "b", "c"))).thenReturn(1235);
        assertEquals(1235, gazelleUserService.getUsersCount("abcd", Set.of("a", "b", "c")));
    }

    @Test
    void getUsersCount2Null() {
        when(gazelleDAO.getUsersCount("abcd", Set.of("a", "b", "c"))).thenReturn(null);
        assertEquals(0, gazelleUserService.getUsersCount("abcd", Set.of("a", "b", "c")));
    }

    @Test
    void getUsers() {
        when(gazelleDAO.getUsers(eq(100), eq(200), any())).thenReturn(List.of(user1));
        assertEquals(List.of(user1), gazelleUserService.getUsers(realm, 100, 200));
    }

    @Test
    void searchForUser() {
        when(gazelleDAO.searchForUser(eq("user"), eq(100), eq(200), any())).thenReturn(List.of(user1));
        assertEquals(List.of(user1), gazelleUserService.searchForUser(realm, "user", 100, 200));
    }

    @Test
    void searchForUser2() {
        when(gazelleDAO.getUsers(eq(100), eq(200), any())).thenReturn(List.of(user1));
        assertEquals(List.of(user1), gazelleUserService.searchForUser(realm, "", 100, 200));
    }

    @Test
    void searchForUser3() {
        when(gazelleDAO.getUsers(eq(100), eq(200), any())).thenReturn(List.of(user1));
        assertEquals(List.of(user1), gazelleUserService.searchForUser(realm, null, 100, 200));
    }

    @Test
    void getGroupMembers() {
        when(group.getId()).thenReturn("gazelle:group1");
        when(gazelleDAO.getInstitutionId("group1")).thenReturn(1234);
        when(gazelleDAO.getUsersByInstitution(eq(1234), eq(100), eq(200), any())).thenReturn(List.of(user1));
        assertEquals(List.of(user1), gazelleUserService.getGroupMembers(realm, group, 100, 200));
    }

    @Test
    void getGroupMembers2() {
        when(group.getId()).thenReturn("gazelle:group1");
        when(gazelleDAO.getInstitutionId("group1")).thenReturn(null);
        assertEquals(List.of(), gazelleUserService.getGroupMembers(realm, group, 100, 200));
    }

    @SneakyThrows
    @Test
    void mapFailure() {
        when(rs.getString("username")).thenThrow(new SQLException());
        assertThrows(GazelleSQLException.class, () -> gazelleUserService.mapUser(realm, rs));
    }
}