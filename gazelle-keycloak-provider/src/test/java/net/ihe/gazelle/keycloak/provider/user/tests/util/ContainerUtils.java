package net.ihe.gazelle.keycloak.provider.user.tests.util;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

@Slf4j
public class ContainerUtils {

    static Network network = Network.newNetwork();

    static PostgreSQLContainer<?> postgres;

    static KeycloakContainer keycloak;

    @SneakyThrows
    public synchronized static PostgreSQLContainer<?> getPostgres() {
        if (postgres == null) {
            // start a postgres 9 container with a gazelle DB
            postgres = new PostgreSQLContainer<>(DockerImageName.parse("postgres").withTag("9"))
                    .withDatabaseName("gazelle")
                    .withUsername("gazelle")
                    .withPassword("gazelle")
                    .withNetwork(network)
                    .withNetworkAliases("postgres")
                    .withReuse(true);
            log.info("Starting postgres");
            postgres.start();
            postgres.copyFileToContainer(MountableFile.forClasspathResource("/postgres/init.sh"), "/tmp/init.sh");
            postgres.copyFileToContainer(MountableFile.forClasspathResource("/postgres/sql/schema.sql"), "/tmp/sql/schema.sql");
            postgres.copyFileToContainer(MountableFile.forClasspathResource("/postgres/sql/init.sql"), "/tmp/sql/init.sql");
            postgres.copyFileToContainer(MountableFile.forClasspathResource("/postgres/sql/init_data.sql"), "/tmp/sql/init_data.sql");
            // create schema, init, sample data (users)
            Container.ExecResult execResult = postgres.execInContainer("bash", "/tmp/init.sh");
            log.debug(execResult.getStdout());
            if (execResult.getStderr() != null && !execResult.getStderr().isEmpty()) {
                log.error(execResult.getStderr());
                throw new RuntimeException("Failed to init postgres");
            }
        }
        return postgres;
    }

    @SneakyThrows
    public synchronized static KeycloakContainer getKeycloak() {
        getPostgres();
        if (keycloak == null) {
            // start a keycloak
            keycloak = new KeycloakContainer()
                    .withNetwork(network)
                    .withEnv("KC_DB", "postgres")
                    .withEnv("KC_DB_URL", "jdbc:postgresql://postgres:5432/gazelle")
                    .withEnv("KC_DB_USERNAME", "gazelle")
                    .withEnv("KC_DB_PASSWORD", "gazelle")
                    // add gazelle user provider from classes
                    .withProviderClassesFrom("target/classes")
                    // import gazelle realm on startup
                    .withRealmImportFile("/keycloak/realm-export.json");
            log.info("Starting keycloak");
            keycloak.start();
        }
        return keycloak;
    }
}
