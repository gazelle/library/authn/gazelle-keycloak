package net.ihe.gazelle.keycloak.provider.user.it;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.sdk.OIDCTokenResponse;
import com.nimbusds.openid.connect.sdk.op.OIDCProviderMetadata;
import lombok.SneakyThrows;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Verifier {

    public static void main(String[] args) {
        // run tests against local keycloak (started with docker-compose at root
        verifyUsers("http://localhost:8080/");
        verifyChangeInstitution("http://localhost:8080/", Verifier::getConnection);
        verifyChangeRoles("http://localhost:8080/", Verifier::getConnection);
        verifyUserChange("http://localhost:8080/", Verifier::getConnection);
    }

    @SneakyThrows
    private static Connection getConnection() {
        // get a connection for tests against local keycloak
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/gazelle",
                "gazelle",
                "gazelle");
    }

    private static User.UserBuilder initBuilder(String username) {
        return User.builder()
                .inputUsername(username)
                .inputPassword("aZeRtY")
                .tokenAttributes(
                        Map.of(
                                "name", username + " fn " + username + " ln",
                                "preferred_username", username,
                                "given_name", username + " fn",
                                "family_name", username + " ln",
                                "email", (username + "@gazelle.com").toLowerCase()
                        )
                )
                ;
    }

    public static void verifyUsers(String authServerUrl) {
        User user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        user = initBuilder("user2")
                .roles(Set.of("offline_access", "monitor", "monitor_role", "accounting_role", "uma_authorization"))
                .groups(Set.of("institution2"))
                .build();
        Verifier.verify(authServerUrl, user);

        user = initBuilder("user3")
                .roles(Set.of("offline_access", "vendor_role", "vendor_admin_role", "uma_authorization"))
                .groups(Set.of("institution3"))
                .build();
        Verifier.verify(authServerUrl, user);

        user = initBuilder("user4")
                .roles(Set.of("offline_access", "user_role", "uma_authorization"))
                .groups(Set.of("institution4"))
                .build();
        Verifier.verify(authServerUrl, user);
    }

    public static void verifyUserChange(String authServerUrl, Supplier<Connection> connectionSupplier) {
        User user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        executeSQL("update usr_users set email = 'useR1@ihe.com' where username = 'useR1';", connectionSupplier);
        executeSQL("update usr_users set firstname = 'firstname' where username = 'useR1';", connectionSupplier);
        executeSQL("update usr_users set lastname = 'lastname' where username = 'useR1';", connectionSupplier);

        user = initBuilder("useR1")
                .tokenAttributes(
                        Map.of(
                                "name", "firstname lastname",
                                "preferred_username", "useR1",
                                "given_name", "firstname",
                                "family_name", "lastname",
                                "email", "user1@ihe.com"
                        )
                )
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        executeSQL("update usr_users set email = 'useR1@gazelle.com' where username = 'useR1';", connectionSupplier);
        executeSQL("update usr_users set firstname = 'useR1 fn' where username = 'useR1';", connectionSupplier);
        executeSQL("update usr_users set lastname = 'useR1 ln' where username = 'useR1';", connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);
    }

    public static void verifyChangeInstitution(String authServerUrl, Supplier<Connection> connectionSupplier) {

        User user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        executeSQL("update usr_users set institution_id = (SELECT  id from usr_institution where keyword = 'institution2') where username = 'useR1';", connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("institution2"))
                .build();
        Verifier.verify(authServerUrl, user);

        executeSQL("update usr_users set institution_id = (SELECT  id from usr_institution where keyword = 'INSTITution1') where username = 'useR1';", connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);
    }

    public static void verifyChangeRoles(String authServerUrl, Supplier<Connection> connectionSupplier) {

        User user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        setRoles("useR1", Set.of("monitor_role"), connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "monitor", "monitor_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        setRoles("useR1", Set.of("monitor_role", "admin_role", "vendor_admin_role", "vendor_role"), connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "monitor", "monitor_role", "admin", "admin_role", "vendor_admin_role", "vendor_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);

        setRoles("useR1", Set.of("admin_role"), connectionSupplier);

        user = initBuilder("useR1")
                .roles(Set.of("offline_access", "admin", "admin_role", "uma_authorization"))
                .groups(Set.of("INSTITution1"))
                .build();
        Verifier.verify(authServerUrl, user);
    }

    private static void setRoles(String username, Set<String> roles, Supplier<Connection> connectionSupplier) {
        executeSQL("delete from usr_user_role where user_id = (SELECT id from usr_users where username = '" + username + "');", connectionSupplier);
        for (String role : roles) {
            executeSQL("insert into usr_user_role (user_id,role_id) values " +
                    "((select id from usr_users where username = '" + username + "'), " +
                    "(select id from usr_role where name ='" + role + "'));", connectionSupplier);
        }
    }

    @SneakyThrows
    public static void verify(String baseUri, User user) {
        // retrieve OIDC response
        OIDCTokenResponse oidcTokenResponse = getOidcTokenResponse(baseUri, user.getInputUsername(), user.getInputPassword());
        // verify id token claims
        JWT idToken = oidcTokenResponse.getOIDCTokens().getIDToken();
        verifyJwt(user, idToken);
        // verify access token claims
        JWT accessToken = JWTParser.parse(oidcTokenResponse.getOIDCTokens().getAccessToken().getValue());
        verifyJwt(user, accessToken);
    }

    @SneakyThrows
    private static OIDCTokenResponse getOidcTokenResponse(String baseUri, String username, String password) {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        Issuer issuer = new Issuer(baseUri + "realms/gazelle");
        // retrieve well known
        OIDCProviderMetadata opMetadata = OIDCProviderMetadata.resolve(issuer);

        // login with password grant_type
        HttpPost request = new HttpPost(opMetadata.getTokenEndpointURI());
        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(List.of(
                new BasicNameValuePair("grant_type", "password"),
                new BasicNameValuePair("username", username),
                new BasicNameValuePair("password", password),
                new BasicNameValuePair("client_id", "rocketchat"),
                new BasicNameValuePair("scope", "openid")
        ), StandardCharsets.UTF_8);
        formEntity.setContentType(ContentType.APPLICATION_FORM_URLENCODED.getMimeType());
        request.setEntity(formEntity);
        CloseableHttpResponse response = httpClient.execute(request);
        String responseString = new String(response.getEntity().getContent().readAllBytes(), StandardCharsets.UTF_8);
        JSONObject jsonObject = (JSONObject) new JSONParser(JSONParser.MODE_PERMISSIVE).parse(responseString);
        // parse response as an OIDCTokenResponse
        return OIDCTokenResponse.parse(jsonObject);
    }

    private static void verifyJwt(User user, JWT jwt) throws ParseException {
        // verify roles
        Set<String> roles = new HashSet<>(jwt.getJWTClaimsSet().getStringListClaim("roles"));
        assertEquals(user.getRoles(), roles);
        // verify groups
        Set<String> groups = new HashSet<>(jwt.getJWTClaimsSet().getStringListClaim("groups"));
        assertEquals(user.getGroups(), groups);
        // verify attributes
        for (Map.Entry<String, String> entry : user.getTokenAttributes().entrySet()) {
            assertEquals(entry.getValue(), jwt.getJWTClaimsSet().getStringClaim(entry.getKey()));
        }
    }

    @SneakyThrows
    private static void executeSQL(String sql, Supplier<Connection> connectionSupplier) {
        Class.forName("org.postgresql.Driver");
        try (Connection c = connectionSupplier.get();
             PreparedStatement st = c.prepareStatement(sql)) {
            st.execute();
        }
    }
}
