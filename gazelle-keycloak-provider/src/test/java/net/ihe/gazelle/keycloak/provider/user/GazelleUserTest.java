package net.ihe.gazelle.keycloak.provider.user;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.common.util.MultivaluedHashMap;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.federated.UserFederatedStorageProvider;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GazelleUserTest {

    @Mock
    private KeycloakSession session;
    @Mock
    private RealmModel realm;
    @Mock
    private ComponentModel componentModel;
    @Mock
    private UserFederatedStorageProvider userFederatedStorageProvider;

    @Test
    void getSetUsername() {
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        GazelleUser gazelleUser = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        assertEquals("username", gazelleUser.getUsername());
        gazelleUser.setUsername("another");
        assertEquals("another", gazelleUser.getUsername());
    }

    @Test
    void getSetGazelleRoles() {
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        doNothing().when(userFederatedStorageProvider).setSingleAttribute(realm, "username", "GAZELLE_ROLES", "[roles]");
        doNothing().when(userFederatedStorageProvider).setSingleAttribute(realm, "username", "EMAIL", null);
        doNothing().when(userFederatedStorageProvider).setSingleAttribute(realm, "username", "FIRST_NAME", null);
        doNothing().when(userFederatedStorageProvider).setSingleAttribute(realm, "username", "LAST_NAME", null);
        when(userFederatedStorageProvider.getAttributes(realm, "username")).thenReturn(new MultivaluedHashMap<>(Map.of("GAZELLE_ROLES", List.of("[roles]"))));
        GazelleUser gazelleUser = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        gazelleUser.setGazelleRoles("[roles]");
        assertEquals("[roles]", gazelleUser.getGazelleRoles());
    }

    @Test
    void getRealm() {
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        GazelleUser gazelleUser = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        assertEquals(realm, gazelleUser.getRealm());
    }

    @Test
    void getId() {
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        when(componentModel.getId()).thenReturn("gazelle");
        GazelleUser gazelleUser = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        assertEquals("f:gazelle:username", gazelleUser.getId());
    }

    @Test
    void testEquals() {
        when(session.userFederatedStorage()).thenReturn(userFederatedStorageProvider);
        when(componentModel.getId()).thenReturn("gazelle");
        GazelleUser gazelleUser = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        GazelleUser gazelleUser2 = new GazelleUser(session, realm, componentModel, "username", null, null, null);
        assertEquals(gazelleUser, gazelleUser2);
        assertEquals(gazelleUser2, gazelleUser);
        assertEquals(gazelleUser.hashCode(), gazelleUser2.hashCode());
    }

}