package net.ihe.gazelle.keycloak.provider.user.roles;

import net.ihe.gazelle.keycloak.provider.user.GazelleUser;
import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class RoleServiceTest {

    @Mock
    GazelleUser gazelleUser;

    @Mock
    GazelleDAO gazelleDAO;

    @Mock
    RealmModel realm;

    @Mock
    RoleModel monitor_role;

    @Mock
    RoleModel monitor;

    @Mock
    RoleModel admin_role;

    @Mock
    RoleModel vendor_role;

    @Mock
    RoleModel keycloak_role_1;

    @Mock
    RoleModel keycloak_role_2;

    @Test
    void updateRolesNull() {
        RoleService roleService = new RoleService(null, Set::of);
        roleService.updateRoles(null);
        verifyNoInteractions(gazelleDAO);
    }

    @Test
    void updateRolesNullUsername() {
        RoleService roleService = new RoleService(null, Set::of);
        when(gazelleUser.getUsername()).thenReturn(null);
        roleService.updateRoles(gazelleUser);
        verifyNoInteractions(gazelleDAO);
    }

    @Test
    void updateRolesNullRoleMappings() {

        RoleService roleService = new RoleService(gazelleDAO, () -> null);

        when(gazelleUser.getUsername()).thenReturn("user1");

        when(gazelleUser.getRoleMappings()).thenReturn(Set.of());
        when(gazelleDAO.getRoles("user1")).thenReturn(Set.of());

        roleService.updateRoles(gazelleUser);

        verify(gazelleUser, atLeastOnce()).setGazelleRoles("[]");
    }

    @Test
    void updateRoles() {

        RoleService roleService = new RoleService(gazelleDAO, () -> Set.of(
                "monitor_role=monitor",
                "another_role=another",
                "arole=",
                "=role",
                ""
        ));

        when(realm.getRole("monitor_role")).thenReturn(monitor_role);
        when(realm.getRole("monitor")).thenReturn(null);
        when(realm.addRole(getKeycloakId("monitor"), "monitor")).thenReturn(monitor);
        when(realm.getRole("admin_role")).thenReturn(admin_role);
        when(realm.getRole("vendor_role")).thenReturn(vendor_role);

        when(vendor_role.getName()).thenReturn("vendor_role");
        when(vendor_role.getId()).thenReturn(getKeycloakId("vendor_role"));
        when(keycloak_role_1.getId()).thenReturn("keycloak_role_1");
        when(keycloak_role_2.getId()).thenReturn(null);
        when(gazelleUser.getUsername()).thenReturn("user1");
        when(gazelleUser.getRealm()).thenReturn(realm);

        when(gazelleUser.getRoleMappings()).thenReturn(Set.of(monitor_role, vendor_role, keycloak_role_1, keycloak_role_2));
        when(gazelleDAO.getRoles("user1")).thenReturn(Set.of("monitor_role", "admin_role"));

        roleService.updateRoles(gazelleUser);

        verify(gazelleUser, atLeastOnce()).deleteRoleMapping(vendor_role);
        verify(gazelleUser, atLeastOnce()).grantRole(admin_role);
        verify(gazelleUser, atLeastOnce()).grantRole(monitor);
        verify(gazelleUser, atLeastOnce()).setGazelleRoles("[admin_role,monitor,monitor_role]");

    }

    private String getKeycloakId(String roleName) {
        String nameForId = GazelleUserStorageProviderConstants.GAZELLE_PREFIX + roleName;
        return UUID.nameUUIDFromBytes(nameForId.getBytes(StandardCharsets.UTF_8)).toString();
    }

}