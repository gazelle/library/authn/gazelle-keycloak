package net.ihe.gazelle.keycloak.provider.user.it;

import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.Set;

/**
 * User for verification against keycloak
 */
@Data
@Builder
public class User {

    String inputUsername;

    String inputPassword;

    Map<String, String> tokenAttributes;

    Set<String> roles;

    Set<String> groups;

}
