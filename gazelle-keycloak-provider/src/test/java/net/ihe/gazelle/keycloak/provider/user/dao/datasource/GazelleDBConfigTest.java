package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GazelleDBConfigTest {

    @Test
    void getters() {
        GazelleDBConfig gazelleDBConfig = new GazelleDBConfig("driverClass", "jdbcUrl", "username", "password");
        assertEquals("driverClass", gazelleDBConfig.getDriverClass());
        assertEquals("jdbcUrl", gazelleDBConfig.getJdbcUrl());
        assertEquals("username", gazelleDBConfig.getUsername());
        assertEquals("password", gazelleDBConfig.getPassword());
    }

    @Test
    void equalsHashcode() {
        EqualsVerifier.simple().forClass(GazelleDBConfig.class).verify();
    }

}
