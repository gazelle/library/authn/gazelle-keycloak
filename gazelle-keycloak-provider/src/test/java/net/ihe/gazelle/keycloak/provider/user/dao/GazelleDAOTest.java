package net.ihe.gazelle.keycloak.provider.user.dao;

import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import net.ihe.gazelle.keycloak.provider.user.tests.util.TestDatabase;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class GazelleDAOTest {

    private GazelleDAO getGazelleDAO() {
        return new GazelleDAO(TestDatabase.INSTANCE::getConnection);
    }

    @Test
    void getRoles() {
        assertEquals(Set.of(), getGazelleDAO().getRoles(null));
        assertEquals(Set.of(), getGazelleDAO().getRoles("none"));
        assertEquals(Set.of("admin_role"), getGazelleDAO().getRoles("user1"));
        assertEquals(Set.of("admin_role"), getGazelleDAO().getRoles("UsEr1"));
    }

    @Test
    void getInstitutionKeyword() {
        assertEquals(List.of(), getGazelleDAO().getInstitutionKeywords(null));
        assertEquals(List.of(), getGazelleDAO().getInstitutionKeywords("none"));
        assertEquals(List.of("INSTITution1"), getGazelleDAO().getInstitutionKeywords("user1"));
        assertEquals(List.of("INSTITution1"), getGazelleDAO().getInstitutionKeywords("UsEr1"));
    }

    @Test
    void getUserByUsername() {
        assertNull(getGazelleDAO().getUserByUsername(null, rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByUsername("none", rs -> rs.getString("email")));
        assertEquals("useR1@gazelle.com", getGazelleDAO().getUserByUsername("user1", rs -> rs.getString("email")));
        assertEquals("useR1@gazelle.com", getGazelleDAO().getUserByUsername("UsEr1", rs -> rs.getString("email")));

        assertNull(getGazelleDAO().getUserByUsername("user5", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByUsername("user5b", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByUsername("user6", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByUsername("USEr6", rs -> rs.getString("email")));
    }

    @Test
    void getUserByEmail() {
        assertNull(getGazelleDAO().getUserByEmail(null, rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByEmail("none", rs -> rs.getString("email")));
        assertEquals("useR1@gazelle.com", getGazelleDAO().getUserByEmail("useR1@gazelle.com", rs -> rs.getString("email")));
        assertEquals("useR1@gazelle.com", getGazelleDAO().getUserByEmail("UsEr1@gazelle.com", rs -> rs.getString("email")));

        assertNull(getGazelleDAO().getUserByEmail("user5@gazelle.com", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByEmail("User5@gazelle.com", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByEmail("user6@gazelle.com", rs -> rs.getString("email")));
        assertNull(getGazelleDAO().getUserByEmail("user6b@gazelle.com", rs -> rs.getString("email")));
    }

    @Test
    void verifyPasswordForUsername() {
        assertFalse(getGazelleDAO().verifyPasswordForUsername(null, null));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("none", null));
        assertFalse(getGazelleDAO().verifyPasswordForUsername(null, "password"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("none", "password"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user1", "password"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user1", "azerty"));
        assertTrue(getGazelleDAO().verifyPasswordForUsername("user1", "aZeRtY"));
        assertTrue(getGazelleDAO().verifyPasswordForUsername("UsEr1", "aZeRtY"));

        assertFalse(getGazelleDAO().verifyPasswordForUsername("user5", "aZeRtY"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user5b", "aZeRtY"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user6", "aZeRtY"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("USEr6", "aZeRtY"));
    }

    @Test
    void updatePasswordForUsername() {
        assertTrue(getGazelleDAO().verifyPasswordForUsername("user1", "aZeRtY"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user1", "gazelle"));
        getGazelleDAO().updatePasswordForUsername("user1", "gazelle");
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user1", "aZeRtY"));
        assertTrue(getGazelleDAO().verifyPasswordForUsername("user1", "gazelle"));
        getGazelleDAO().updatePasswordForUsername("user1", "aZeRtY");
        assertTrue(getGazelleDAO().verifyPasswordForUsername("user1", "aZeRtY"));
        assertFalse(getGazelleDAO().verifyPasswordForUsername("user1", "gazelle"));
    }

    @Test
    void updatePasswordForUsernameNull() {
        assertFalse(getGazelleDAO().updatePasswordForUsername(null, "d46135a8a67e67eb5fec4ff276600498"));
        assertFalse(getGazelleDAO().updatePasswordForUsername("user1", null));
        assertFalse(getGazelleDAO().updatePasswordForUsername("none", "d46135a8a67e67eb5fec4ff276600498"));
    }

    @Test
    void getUsersCount() {
        assertEquals(4, getGazelleDAO().getUsersCount());
    }

    @Test
    void getUsersCount1() {
        assertEquals(4, getGazelleDAO().getUsersCount(null, null));
    }

    @Test
    void getUsersCount2() {
        assertEquals(4, getGazelleDAO().getUsersCount(null, Set.of()));
    }

    @Test
    void getUsersCount3() {
        assertEquals(4, getGazelleDAO().getUsersCount("user", null));
        assertEquals(1, getGazelleDAO().getUsersCount("user1", null));
        assertEquals(1, getGazelleDAO().getUsersCount("UsEr1", null));

        assertEquals(0, getGazelleDAO().getUsersCount("user5", null));
        assertEquals(0, getGazelleDAO().getUsersCount("user6", null));
    }

    @Test
    void getUsersCount4() {
        assertEquals(4, getGazelleDAO().getUsersCount(null, Set.of("not_a_gazelle_group")));
        assertEquals(1, getGazelleDAO().getUsersCount(null, Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution1")));
        assertEquals(3, getGazelleDAO().getUsersCount(null, Set.of(
                GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution1",
                GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution2",
                GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution3"
        )));
        assertEquals(1, getGazelleDAO().getUsersCount("user1", Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution1")));
        assertEquals(1, getGazelleDAO().getUsersCount("UsEr1", Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution1")));
        assertEquals(1, getGazelleDAO().getUsersCount("UsEr1", Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "instiTUtion1")));
        assertEquals(0, getGazelleDAO().getUsersCount("user2", Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution1")));

        assertEquals(1, getGazelleDAO().getUsersCount(null, Set.of(GazelleUserStorageProviderConstants.GAZELLE_PREFIX + "institution4")));
    }

    @Test
    void getUsers() {
        assertThat(List.of("useR1@gazelle.com", "user2@gazelle.com", "user3@gazelle.com", "user4@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().getUsers(0, 100, rs -> rs.getString("email")));
    }

    @Test
    void searchForUser() {
        assertThat(List.of("useR1@gazelle.com", "user2@gazelle.com", "user3@gazelle.com", "user4@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser(null, 0, 100, rs -> rs.getString("email")));
        assertThat(List.of("user2@gazelle.com", "user3@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser(null, 1, 2, rs -> rs.getString("email")));
        assertThat(List.of("useR1@gazelle.com", "user2@gazelle.com", "user3@gazelle.com", "user4@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser("", 0, 100, rs -> rs.getString("email")));
        assertThat(List.of("useR1@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser("user1", 0, 100, rs -> rs.getString("email")));
        assertThat(List.of("useR1@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser("UsEr1", 0, 100, rs -> rs.getString("email")));
        assertThat(List.of())
                .hasSameElementsAs(getGazelleDAO().searchForUser("user1", 5, 100, rs -> rs.getString("email")));
        assertThat(List.of("useR1@gazelle.com", "user2@gazelle.com", "user3@gazelle.com", "user4@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser("gazelle", 0, 100, rs -> rs.getString("email")));
        assertThat(List.of("useR1@gazelle.com", "user2@gazelle.com", "user3@gazelle.com", "user4@gazelle.com"))
                .hasSameElementsAs(getGazelleDAO().searchForUser("GAZEllE", 0, 100, rs -> rs.getString("email")));

        assertThat(List.of())
                .hasSameElementsAs(getGazelleDAO().searchForUser("user5", 0, 100, rs -> rs.getString("email")));
        assertThat(List.of())
                .hasSameElementsAs(getGazelleDAO().searchForUser("user6", 0, 100, rs -> rs.getString("email")));
    }

    @Test
    void getInstitutionId() {
        assertNull(getGazelleDAO().getInstitutionId(null));
        assertNull(getGazelleDAO().getInstitutionId("none"));
        assertEquals(1, getGazelleDAO().getInstitutionId("institution1"));
        assertEquals(1, getGazelleDAO().getInstitutionId("instiTUtion1"));
    }

    @Test
    void getUsersByInstitution() {
        assertEquals(List.of(), getGazelleDAO().getUsersByInstitution(null, 0, 100, rs -> rs.getString("email")));
        assertEquals(List.of(), getGazelleDAO().getUsersByInstitution(-1, 0, 100, rs -> rs.getString("email")));
        assertEquals(List.of("useR1@gazelle.com"), getGazelleDAO().getUsersByInstitution(1, 0, 100, rs -> rs.getString("email")));
        assertEquals(List.of(), getGazelleDAO().getUsersByInstitution(1, 1, 100, rs -> rs.getString("email")));

        Integer institutionId = getGazelleDAO().getInstitutionId("institution4");
        assertEquals(List.of("user4@gazelle.com"), getGazelleDAO().getUsersByInstitution(institutionId, 0, 100, rs -> rs.getString("email")));
    }
}