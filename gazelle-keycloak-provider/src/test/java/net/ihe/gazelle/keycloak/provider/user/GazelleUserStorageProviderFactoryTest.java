package net.ihe.gazelle.keycloak.provider.user;

import net.ihe.gazelle.keycloak.provider.user.tests.util.TestDatabase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GazelleUserStorageProviderFactoryTest {

    @Mock
    private KeycloakSession session;
    @Mock
    private RealmModel realm;
    @Mock
    private ComponentModel componentModel;
    @Mock
    private ComponentModel oldComponentModel;

    void configureComponentModel() {
        when(componentModel.get(CONFIG_KEY_DRIVER_CLASS)).thenReturn(TestDatabase.DRIVER_CLASS);
        when(componentModel.get(CONFIG_KEY_JDBC_URL)).thenReturn(TestDatabase.INSTANCE.getJdbcUrl());
        when(componentModel.get(CONFIG_KEY_DB_USERNAME)).thenReturn(TestDatabase.INSTANCE.getJdbcUsername());
        when(componentModel.get(CONFIG_KEY_DB_PASSWORD)).thenReturn(TestDatabase.INSTANCE.getJdbcPassword());
    }

    @Test
    void create() {
        configureComponentModel();
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProviderFactory().create(session, componentModel);
        assertNotNull(gazelleUserStorageProvider);
    }

    @Test
    void getId() {
        GazelleUserStorageProviderFactory gazelleUserStorageProviderFactory = new GazelleUserStorageProviderFactory();
        assertEquals("gazelle-user-provider", gazelleUserStorageProviderFactory.getId());
    }

    @Test
    void getConfigProperties() {
        List<ProviderConfigProperty> configProperties = new GazelleUserStorageProviderFactory().getConfigProperties();
        Set<String> names = configProperties.stream().map(ProviderConfigProperty::getName).collect(Collectors.toSet());
        assertEquals(Set.of("jdbcDriver", "jdbcUrl", "username", "password", "validationQuery", "roleMapping"), names);
    }

    @Test
    void validateConfiguration() {
        GazelleUserStorageProviderFactory gazelleUserStorageProviderFactory = new GazelleUserStorageProviderFactory();
        when(componentModel.get(CONFIG_KEY_VALIDATION_QUERY)).thenReturn("select 1");
        configureComponentModel();
        gazelleUserStorageProviderFactory.validateConfiguration(session, realm, componentModel);
    }

    @Test
    void validateConfigurationFails() {
        GazelleUserStorageProviderFactory gazelleUserStorageProviderFactory = new GazelleUserStorageProviderFactory();
        when(componentModel.get(CONFIG_KEY_VALIDATION_QUERY)).thenReturn("efgergreger");
        configureComponentModel();
        assertThrows(ComponentValidationException.class, () ->
                gazelleUserStorageProviderFactory.validateConfiguration(session, realm, componentModel)
        );
    }

    @Test
    void onUpdate() {
        GazelleUserStorageProviderFactory gazelleUserStorageProviderFactory = new GazelleUserStorageProviderFactory();
        assertDoesNotThrow(() -> gazelleUserStorageProviderFactory.onUpdate(session, realm, oldComponentModel, componentModel));
        // no mock called
    }

    @Test
    void onCreate() {
        GazelleUserStorageProviderFactory gazelleUserStorageProviderFactory = new GazelleUserStorageProviderFactory();
        assertDoesNotThrow(() -> gazelleUserStorageProviderFactory.onCreate(session, realm, componentModel));
        // no mock called
    }
}