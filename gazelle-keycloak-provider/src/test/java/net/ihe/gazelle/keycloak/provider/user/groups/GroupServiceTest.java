package net.ihe.gazelle.keycloak.provider.user.groups;

import net.ihe.gazelle.keycloak.provider.user.GazelleUser;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.models.GroupModel;
import org.keycloak.models.RealmModel;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GroupServiceTest {

    @Mock
    GazelleDAO gazelleDAO;
    @Mock
    RealmModel realm;
    @Mock
    GroupModel institution1;
    @Mock
    GroupModel institution2;
    @Mock
    GroupModel keycloakGroup;
    @Mock
    GazelleUser gazelleUser;

    @InjectMocks
    GroupService groupService;

    @Test
    void updateGroupsNull() {
        groupService.updateGroups(null);
        verifyNoInteractions(gazelleDAO);
    }

    @Test
    void updateGroupsNullUsername() {
        GazelleUser gazelleUser = mock(GazelleUser.class);
        when(gazelleUser.getUsername()).thenReturn(null);
        groupService.updateGroups(gazelleUser);
        verifyNoInteractions(gazelleDAO);
    }

    @Test
    void updateGroupsUser1_1() {
        when(gazelleUser.getUsername()).thenReturn("user1");
        when(gazelleUser.getRealm()).thenReturn(realm);
        when(realm.getGroupById("gazelle:institution1")).thenReturn(institution1);

        when(gazelleDAO.getInstitutionKeywords("user1")).thenReturn(List.of("institution1"));
        when(gazelleUser.getGroups()).thenReturn(Set.of());

        groupService.updateGroups(gazelleUser);
        verify(gazelleUser).joinGroup(institution1);
    }

    @Test
    void updateGroupsUser1_2() {
        when(gazelleUser.getUsername()).thenReturn("user1");
        when(gazelleUser.getRealm()).thenReturn(realm);
        when(realm.getGroupById("gazelle:institution1")).thenReturn(null);
        when(realm.createGroup("gazelle:institution1", "institution1")).thenReturn(institution1);

        when(gazelleDAO.getInstitutionKeywords("user1")).thenReturn(List.of("institution1"));
        when(gazelleUser.getGroups()).thenReturn(Set.of());

        groupService.updateGroups(gazelleUser);
        verify(gazelleUser).joinGroup(institution1);
    }

    @Test
    void updateGroupsUser1_3() {
        GroupService groupService = new GroupService(gazelleDAO);

        when(gazelleUser.getUsername()).thenReturn("user1");
        when(gazelleUser.getRealm()).thenReturn(realm);
        when(institution1.getId()).thenReturn("gazelle:institution1");
        when(institution2.getId()).thenReturn("gazelle:institution2");
        when(keycloakGroup.getId()).thenReturn("keycloak:group");
        when(realm.getGroupById("gazelle:institution2")).thenReturn(institution2);

        when(gazelleDAO.getInstitutionKeywords("user1")).thenReturn(List.of("institution1"));
        when(gazelleUser.getGroups()).thenReturn(Set.of(institution1, institution2, keycloakGroup));

        groupService.updateGroups(gazelleUser);
        verify(gazelleUser).leaveGroup(institution2);
    }

    @Test
    void updateGroupsUser1_4() {
        GroupService groupService = new GroupService(gazelleDAO);
        when(gazelleUser.getUsername()).thenReturn("user1");
        when(gazelleUser.getRealm()).thenReturn(realm);
        when(institution1.getId()).thenReturn("gazelle:institution1");
        when(institution2.getId()).thenReturn("gazelle:institution2");
        when(keycloakGroup.getId()).thenReturn("keycloak:group");
        when(realm.getGroupById("gazelle:institution1")).thenReturn(institution1);
        when(realm.getGroupById("gazelle:institution2")).thenReturn(institution2);

        when(gazelleDAO.getInstitutionKeywords("user1")).thenReturn(null);
        when(gazelleUser.getGroups()).thenReturn(Set.of(institution1, institution2, keycloakGroup));

        groupService.updateGroups(gazelleUser);
        verify(gazelleUser).leaveGroup(institution1);
        verify(gazelleUser).leaveGroup(institution2);
    }

}