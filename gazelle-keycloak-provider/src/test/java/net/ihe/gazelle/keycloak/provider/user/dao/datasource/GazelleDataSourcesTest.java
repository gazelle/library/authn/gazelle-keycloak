package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import net.ihe.gazelle.keycloak.provider.user.dao.GazelleSQLException;
import net.ihe.gazelle.keycloak.provider.user.tests.util.TestDatabase;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class GazelleDataSourcesTest {

    @Test
    void getDatasource() {

        GazelleDBConfig gazelleDBConfig = new GazelleDBConfig(
                TestDatabase.DRIVER_CLASS,
                TestDatabase.INSTANCE.getJdbcUrl(),
                TestDatabase.INSTANCE.getJdbcUsername(),
                TestDatabase.INSTANCE.getJdbcPassword());
        DataSource dataSource1 = GazelleDataSources.INSTANCE.getDataSource(gazelleDBConfig);
        assertNotNull(dataSource1);
        GazelleDataSources.INSTANCE.datasourceCache.invalidateAll();
        DataSource dataSource2 = GazelleDataSources.INSTANCE.getDataSource(gazelleDBConfig);
        assertNotNull(dataSource2);
        assertNotEquals(dataSource1, dataSource2);
    }

    @Test
    void getDatasourceNull() {
        GazelleDBConfig gazelleDBConfig = new GazelleDBConfig(
                TestDatabase.DRIVER_CLASS,
                TestDatabase.INSTANCE.getJdbcUrl(),
                TestDatabase.INSTANCE.getJdbcUsername(),
                TestDatabase.INSTANCE.getJdbcPassword());
        GazelleDataSources gazelleDataSources = new GazelleDataSources(e -> {
            throw new SQLException();
        });
        assertThrows(GazelleSQLException.class, () -> gazelleDataSources.getDataSource(gazelleDBConfig));
    }

}
