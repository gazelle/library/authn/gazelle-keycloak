package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import lombok.SneakyThrows;
import net.ihe.gazelle.keycloak.provider.user.tests.util.TestDatabase;
import org.junit.jupiter.api.Test;
import org.keycloak.component.ComponentModel;

import java.sql.Connection;
import java.sql.PreparedStatement;

import static net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GazelleConnectionProviderTest {

    @SneakyThrows
    @Test
    void get() {
        ComponentModel componentModel = new ComponentModel();

        componentModel.put(CONFIG_KEY_DRIVER_CLASS, TestDatabase.DRIVER_CLASS);
        componentModel.put(CONFIG_KEY_JDBC_URL, TestDatabase.INSTANCE.getJdbcUrl());
        componentModel.put(CONFIG_KEY_DB_USERNAME, TestDatabase.INSTANCE.getJdbcUsername());
        componentModel.put(CONFIG_KEY_DB_PASSWORD, TestDatabase.INSTANCE.getJdbcPassword());

        GazelleConnectionProvider gazelleConnectionProvider = new GazelleConnectionProvider(componentModel);
        try (Connection c = gazelleConnectionProvider.get();
             PreparedStatement st = c.prepareStatement("select 1")
        ) {
            assertTrue(st.execute());
        }
    }
}