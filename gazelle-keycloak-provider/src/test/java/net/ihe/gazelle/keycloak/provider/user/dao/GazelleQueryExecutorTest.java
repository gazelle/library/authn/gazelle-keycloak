package net.ihe.gazelle.keycloak.provider.user.dao;

import lombok.SneakyThrows;
import net.ihe.gazelle.keycloak.provider.user.tests.util.TestDatabase;
import net.ihe.gazelle.keycloak.provider.user.util.FunctionWithSQLException;
import net.ihe.gazelle.keycloak.provider.user.util.GazelleKeycloakProviderException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GazelleQueryExecutorTest {

    @Mock
    private Connection connection;

    @Mock
    private PreparedStatement preparedStatement;

    private GazelleQueryExecutor getGazelleQueryExecutor() {
        return new GazelleQueryExecutor(TestDatabase.INSTANCE::getConnection);
    }

    @Test
    void executeQuerySingleResult() {
        assertEquals("useR1@gazelle.com", getGazelleQueryExecutor().executeQuerySingleResult(
                "select * from usr_users where username=?",
                List.of("useR1"),
                rs -> rs.getString("email")
        ));
        assertNull(getGazelleQueryExecutor().executeQuerySingleResult(
                "select * from usr_users order by username",
                List.of(),
                rs -> rs.getString("email")
        ));
        assertNull(getGazelleQueryExecutor().executeQuerySingleResult(
                "select * from usr_users where username=?",
                List.of("toto"),
                rs -> rs.getString("email")
        ));
    }

    @Test
    void executeUpdate() {
        assertEquals(1, getGazelleQueryExecutor().executeUpdate(
                "update usr_users set email = ? where username = ?",
                List.of("user1@ihe.com", "useR1")
        ));
        assertEquals(1, getGazelleQueryExecutor().executeUpdate(
                "update usr_users set email = ? where username = ?",
                List.of("useR1@gazelle.com", "useR1")
        ));
    }

    @Test
    void executeUpdateAutoCommit() {
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> {
            Connection connection = TestDatabase.INSTANCE.getConnection();
            connection.setAutoCommit(false);
            return connection;
        });
        assertEquals(1, gazelleQueryExecutor.executeUpdate(
                "update usr_users set email = ? where username = ?",
                List.of("user1@ihe.com", "useR1")
        ));
        assertEquals(1, gazelleQueryExecutor.executeUpdate(
                "update usr_users set email = ? where username = ?",
                List.of("useR1@gazelle.com", "useR1")
        ));
    }

    @Test
    void executeUpdateThrows() {
        List<Object> parameters = List.of("user1@ihe.com", "user1");
        GazelleQueryExecutor gazelleQueryExecutor = getGazelleQueryExecutor();
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeUpdate(
                "zefefzfezfzefezf",
                parameters
        ));
    }

    @Test
    void executeQuery() {
        assertEquals(List.of("useR1@gazelle.com"), getGazelleQueryExecutor().executeQuery(
                "select * from usr_users where username=?",
                List.of("useR1"),
                rs -> rs.getString("email")
        ));
        assertThat(List.of(
                "useR1@gazelle.com",
                "user2@gazelle.com",
                "user3@gazelle.com",
                "user4@gazelle.com",
                "user5@gazelle.com",
                "User5@gazelle.com",
                "user6@gazelle.com",
                "user6b@gazelle.com"
        ))
                .hasSameElementsAs(
                        getGazelleQueryExecutor().executeQuery(
                                "select * from usr_users order by username",
                                List.of(),
                                rs -> rs.getString("email")
                        )
                );
        assertEquals(List.of(), getGazelleQueryExecutor().executeQuery(
                "select * from usr_users where username=?",
                List.of("toto"),
                rs -> rs.getString("email")
        ));
    }

    @Test
    void executeQueryThrows1() {
        GazelleQueryExecutor gazelleQueryExecutor = getGazelleQueryExecutor();
        List<Object> parameters = List.of("user1");
        FunctionWithSQLException<ResultSet, String> mapper = rs -> rs.getString("email");
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeQuery(
                "select * from not_exists where username=?",
                parameters,
                mapper
        ));
    }

    @Test
    void executeQueryThrows2() {
        GazelleQueryExecutor gazelleQueryExecutor = getGazelleQueryExecutor();
        List<Object> parameters = List.of("user1");
        FunctionWithSQLException<ResultSet, String> mapper = rs -> rs.getString("email");
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeQuery(
                "trhtrhrthrth",
                parameters,
                mapper
        ));
    }

    @SneakyThrows
    @Test
    void getConnectionException() {
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> {
            throw new SQLException();
        });
        List<Object> emptyList = List.of();
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeUpdate("update", emptyList));
    }

    @SneakyThrows
    @Test
    void exceptionRollback() {
        when(connection.getAutoCommit()).thenReturn(true);
        doNothing().when(connection).setAutoCommit(false);
        when(connection.prepareStatement("update")).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenThrow(new SQLException());
        doThrow(new SQLException()).when(connection).rollback();
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> connection);
        List<Object> emptyList = List.of();
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeUpdate("update", emptyList));
    }

    @SneakyThrows
    @Test
    void getConnectionNull() {
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> null);
        List<Object> emptyList = List.of();
        assertThrows(GazelleKeycloakProviderException.class, () -> gazelleQueryExecutor.executeUpdate("update", emptyList));
    }

    @SneakyThrows
    @Test
    void closeConnectionException() {
        when(connection.getAutoCommit()).thenReturn(false);
        doNothing().when(connection).setAutoCommit(false);
        when(connection.prepareStatement("update")).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(12);
        doNothing().when(connection).commit();
        doThrow(new SQLException()).when(connection).close();
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> connection);
        List<Object> emptyList = List.of();
        assertEquals(12, gazelleQueryExecutor.executeUpdate("update", emptyList));
    }

    @SneakyThrows
    @Test
    void getAutocommitException() {
        when(connection.getAutoCommit()).thenThrow(new SQLException());
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(() -> connection);
        List<Object> emptyList = List.of();
        assertThrows(GazelleSQLException.class, () -> gazelleQueryExecutor.executeUpdate("update", emptyList));
    }

}