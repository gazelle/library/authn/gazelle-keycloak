package net.ihe.gazelle.keycloak.provider.user.roles;

import org.junit.jupiter.api.Test;
import org.keycloak.component.ComponentModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoleMappingProviderTest {

    @Test
    void get() {
        ComponentModel componentModel = new ComponentModel();
        componentModel.getConfig().put("roleMapping", List.of("a", "b", "c"));
        RoleMappingProvider roleMappingProvider = new RoleMappingProvider(componentModel);
        assertEquals(List.of("a", "b", "c"), roleMappingProvider.get());
    }
}