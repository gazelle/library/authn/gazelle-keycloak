package net.ihe.gazelle.keycloak.provider.user;

import net.ihe.gazelle.keycloak.provider.user.service.GazelleUserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.cache.UserCache;
import org.keycloak.storage.ReadOnlyException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.STRICT_STUBS)
class GazelleUserStorageProviderTest {

    @Mock
    KeycloakSession session;

    @Mock
    protected ComponentModel storageProviderModel;

    @Mock
    GazelleUserService gazelleUserService;

    @Mock
    RealmModel realm;

    @Mock
    UserModel user1;

    @Mock
    GroupModel group;

    @Mock
    CredentialInput credentialInput;

    @Mock
    UserCache userCache;

    @InjectMocks
    GazelleUserStorageProvider gazelleUserStorageProvider;

    @Test
    void close() {
        assertDoesNotThrow(() -> gazelleUserStorageProvider.close());
        // no mock called
    }

    @Test
    void getUserById() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUserByUsername(realm, "username")).thenReturn(user1);
        UserModel user = gazelleUserStorageProvider.getUserById("f:gazelle:username", realm);
        assertEquals(user, user1);
    }

    @Test
    void getUserByUsername() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUserByUsername(realm, "username")).thenReturn(user1);
        UserModel user = gazelleUserStorageProvider.getUserByUsername("username", realm);
        assertEquals(user, user1);
    }

    @Test
    void getUserByEmail() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUserByEmail(realm, "username@gazelle.com")).thenReturn(user1);
        UserModel user = gazelleUserStorageProvider.getUserByEmail("username@gazelle.com", realm);
        assertEquals(user, user1);
    }

    @Test
    void supportsCredentialType() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        assertTrue(gazelleUserStorageProvider.supportsCredentialType("password"));
        assertFalse(gazelleUserStorageProvider.supportsCredentialType(null));
        assertFalse(gazelleUserStorageProvider.supportsCredentialType("password-history"));
    }

    @Test
    void isConfiguredFor() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        assertTrue(gazelleUserStorageProvider.isConfiguredFor(realm, user1, "password"));
        assertFalse(gazelleUserStorageProvider.isConfiguredFor(realm, user1, null));
        assertFalse(gazelleUserStorageProvider.isConfiguredFor(realm, user1, "password-history"));
    }

    @Test
    void isValid1() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(credentialInput.getType()).thenReturn("password-history");
        assertFalse(gazelleUserStorageProvider.isValid(realm, user1, credentialInput));
    }

    @Test
    void isValid2() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(credentialInput.getType()).thenReturn("password");
        when(credentialInput.getChallengeResponse()).thenReturn("XXXXX");
        when(user1.getId()).thenReturn("f:gazelle:username");
        when(gazelleUserService.validatePassword("username", "XXXXX")).thenReturn(false);
        assertFalse(gazelleUserStorageProvider.isValid(realm, user1, credentialInput));
    }

    @Test
    void isValid3() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(credentialInput.getType()).thenReturn("password");
        when(credentialInput.getChallengeResponse()).thenReturn("XXXXX");
        when(user1.getId()).thenReturn("f:gazelle:username");
        when(gazelleUserService.validatePassword("username", "XXXXX")).thenReturn(true);
        when(session.getProvider(UserCache.class)).thenReturn(userCache);
        doNothing().when(userCache).evict(any(), any());
        assertTrue(gazelleUserStorageProvider.isValid(realm, user1, credentialInput));
    }

    @Test
    void updatePassword() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(credentialInput.getType()).thenReturn("password");
        when(credentialInput.getChallengeResponse()).thenReturn("XXXXX");
        when(user1.getId()).thenReturn("f:gazelle:username");
        when(gazelleUserService.updatePassword("username", "XXXXX")).thenReturn(true);
        assertTrue(gazelleUserStorageProvider.updateCredential(realm, user1, credentialInput));
    }

    @Test
    void updatePasswordUnsupported() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(credentialInput.getType()).thenReturn("password-history");
        assertThrows(ReadOnlyException.class, () -> gazelleUserStorageProvider.updateCredential(realm, user1, credentialInput));
    }

    @Test
    void getDisableableCredentialTypes() {
        assertEquals(Set.of(), gazelleUserStorageProvider.getDisableableCredentialTypes(realm, user1));
    }

    @Test
    void disableCredentialType() {
        // noop
        assertDoesNotThrow(() -> gazelleUserStorageProvider.disableCredentialType(realm, user1, "password"));
    }

    @Test
    void getUsersCount() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount()).thenReturn(1234);
        assertEquals(1234, gazelleUserStorageProvider.getUsersCount(realm));
    }

    @Test
    void getUsers1() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getUsers(realm, 0, 500_000)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.getUsers(realm));
    }

    @Test
    void getUsers2() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getUsers(realm, 100, 200)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.getUsers(realm, 100, 200));
    }

    @Test
    void searchForUser1() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.searchForUser(realm, "user", 0, 5000)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.searchForUser("user", realm));
    }

    @Test
    void searchForUser2() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.searchForUser(realm, "user", 100, 200)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.searchForUser("user", realm, 100, 200));
    }

    @Test
    void searchForUser3() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getUsers(realm, 0, 5000)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.searchForUser(Map.of("abcd", "defg"), realm));
    }

    @Test
    void searchForUser4() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getUsers(realm, 100, 200)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.searchForUser(Map.of("abcd", "defg"), realm, 100, 200));
    }

    @Test
    void getUsersCount1() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount(null, Set.of("a", "b", "c"))).thenReturn(12);
        assertEquals(12, gazelleUserStorageProvider.getUsersCount(realm, Set.of("a", "b", "c")));
    }

    @Test
    void getUsersCount2() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount("abcd", null)).thenReturn(13);
        assertEquals(13, gazelleUserStorageProvider.getUsersCount(realm, "abcd"));
    }

    @Test
    void getUsersCount3() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount("abcd", Set.of("a", "b", "c"))).thenReturn(14);
        assertEquals(14, gazelleUserStorageProvider.getUsersCount(realm, "abcd", Set.of("a", "b", "c")));
    }

    @Test
    void getUsersCount4() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount()).thenReturn(15);
        assertEquals(15, gazelleUserStorageProvider.getUsersCount(realm, Map.of()));
    }

    @Test
    void getUsersCount5() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount(null, Set.of("a", "b", "c"))).thenReturn(16);
        assertEquals(16, gazelleUserStorageProvider.getUsersCount(realm, Map.of(), Set.of("a", "b", "c")));
    }

    @Test
    void getUsersCount6() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        when(gazelleUserService.getUsersCount()).thenReturn(17);
        assertEquals(17, gazelleUserStorageProvider.getUsersCount(realm, true));
    }

    @Test
    void getGroupMembers1() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getGroupMembers(realm, group, 0, 5000)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.getGroupMembers(realm, group));
    }

    @Test
    void getGroupMembers2() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getGroupMembers(realm, group, 100, 200)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.getGroupMembers(realm, group, 100, 200));
    }

    @Test
    void searchForUserByUserAttribute() {
        GazelleUserStorageProvider gazelleUserStorageProvider = new GazelleUserStorageProvider(session, gazelleUserService);
        List<UserModel> result = List.of(user1);
        when(gazelleUserService.getUsers(realm, 0, 5000)).thenReturn(result);
        assertEquals(result, gazelleUserStorageProvider.searchForUserByUserAttribute("abcd", "efgh", realm));
    }

}