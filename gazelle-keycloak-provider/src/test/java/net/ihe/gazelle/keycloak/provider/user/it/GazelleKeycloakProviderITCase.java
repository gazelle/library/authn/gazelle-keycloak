package net.ihe.gazelle.keycloak.provider.user.it;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.tests.util.ContainerUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;

@Disabled
@Slf4j
class GazelleKeycloakProviderITCase {

    @SneakyThrows
    @Test
    void verifyUsers() {
        // verify users
        Verifier.verifyUsers(ContainerUtils.getKeycloak().getAuthServerUrl());
    }

    @SneakyThrows
    @Test
    void verifyChangeInstitution() {
        // verify institution change
        Verifier.verifyChangeInstitution(ContainerUtils.getKeycloak().getAuthServerUrl(), GazelleKeycloakProviderITCase::getConnection);
    }

    @SneakyThrows
    @Test
    void verifyChangeRoles() {
        // verify roles change
        Verifier.verifyChangeRoles(ContainerUtils.getKeycloak().getAuthServerUrl(), GazelleKeycloakProviderITCase::getConnection);
    }

    @SneakyThrows
    @Test
    void verifyUserChange() {
        // verify user change (email, firstname, lastname)
        Verifier.verifyUserChange(ContainerUtils.getKeycloak().getAuthServerUrl(), GazelleKeycloakProviderITCase::getConnection);
    }

    @SneakyThrows
    private static Connection getConnection() {
        // provide a JDBC connection
        return DriverManager.getConnection(ContainerUtils.getPostgres().getJdbcUrl(),
                ContainerUtils.getPostgres().getUsername(),
                ContainerUtils.getPostgres().getPassword());
    }

}
