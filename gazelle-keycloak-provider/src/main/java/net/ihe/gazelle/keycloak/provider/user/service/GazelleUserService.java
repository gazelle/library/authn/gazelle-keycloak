package net.ihe.gazelle.keycloak.provider.user.service;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.GazelleUser;
import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleSQLException;
import net.ihe.gazelle.keycloak.provider.user.groups.GroupService;
import net.ihe.gazelle.keycloak.provider.user.roles.RoleService;
import org.keycloak.component.ComponentModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Gazelle user service.
 */
@Slf4j
public class GazelleUserService {

    private final KeycloakSession session;
    private final ComponentModel storageProviderModel;
    private final GroupService groupService;
    private final RoleService roleService;
    private final GazelleDAO gazelleDAO;

    /**
     * Instantiates a new Gazelle user service.
     *
     * @param session              keycloak session
     * @param storageProviderModel storage provider model
     * @param gazelleDAO           gazelle dao
     * @param groupService         group service
     * @param roleService          role service
     */
    public GazelleUserService(KeycloakSession session, ComponentModel storageProviderModel,
                              GazelleDAO gazelleDAO,
                              GroupService groupService, RoleService roleService) {
        this.session = session;
        this.storageProviderModel = storageProviderModel;
        this.gazelleDAO = gazelleDAO;
        this.groupService = groupService;
        this.roleService = roleService;
    }

    /**
     * Map a usr_users row to a Keycloak user
     *
     * @param realm realm
     * @param rs    SQL resultset
     * @return gazelle keycloak user
     */
    protected UserModel mapUser(RealmModel realm, ResultSet rs) {

        GazelleUser gazelleUser;
        try {
            String username = rs.getString(GazelleDAO.USERNAME);
            log.debug("mapUser {}", username);

            // create a new user
            // the keycloak user id is based on provider id + username
            // this user is only a facade, all data is stored in user federation storage provided by keycloak
            gazelleUser = new GazelleUser(session, realm, storageProviderModel,
                    username,
                    rs.getString(GazelleDAO.EMAIL),
                    rs.getString(GazelleDAO.FIRSTNAME),
                    rs.getString(GazelleDAO.LASTNAME)
            );
        } catch (SQLException e) {
            throw new GazelleSQLException("Failed to map user", e);
        }

        groupService.updateGroups(gazelleUser);
        roleService.updateRoles(gazelleUser);

        return gazelleUser;
    }

    /**
     * Gets user by username.
     *
     * @param realm    the realm
     * @param username the username
     * @return the user by username
     */
    public UserModel getUserByUsername(RealmModel realm, String username) {
        return gazelleDAO.getUserByUsername(username, rs -> mapUser(realm, rs));
    }

    /**
     * Gets user by email.
     *
     * @param realm the realm
     * @param email the email
     * @return the user by email
     */
    public UserModel getUserByEmail(RealmModel realm, String email) {
        return gazelleDAO.getUserByEmail(email, rs -> mapUser(realm, rs));
    }

    /**
     * Validate password.
     *
     * @param username      the username
     * @param inputPassword the input password
     * @return the boolean
     */
    public boolean validatePassword(String username, String inputPassword) {
        return this.gazelleDAO.verifyPasswordForUsername(username, inputPassword);
    }

    public boolean updatePassword(String username, String inputPassword) {
        return this.gazelleDAO.updatePasswordForUsername(username, inputPassword);
    }

    /**
     * Gets users count.
     *
     * @return the users count
     */
    public int getUsersCount() {
        // simple sql query
        Integer count = gazelleDAO.getUsersCount();
        if (count == null) {
            return 0;
        }
        return count;
    }

    /**
     * Gets users count.
     *
     * @param search   search
     * @param groupIds groupIds
     * @return the users count
     */
    public int getUsersCount(String search, Set<String> groupIds) {
        // simple sql query
        Integer count = gazelleDAO.getUsersCount(search, groupIds);
        if (count == null) {
            return 0;
        }
        return count;
    }

    /**
     * Gets users.
     *
     * @param realm       the realm
     * @param firstResult the first result
     * @param maxResults  the max results
     * @return the users
     */
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
        return gazelleDAO.getUsers(firstResult, maxResults, rs -> mapUser(realm, rs));
    }

    /**
     * Search for user list.
     *
     * @param realm       the realm
     * @param search      the search
     * @param firstResult the first result
     * @param maxResults  the max results
     * @return the list
     */
    public List<UserModel> searchForUser(RealmModel realm, String search, int firstResult, int maxResults) {
        if (search == null || search.isEmpty()) {
            return getUsers(realm, firstResult, maxResults);
        }
        return gazelleDAO.searchForUser(search, firstResult, maxResults, rs -> mapUser(realm, rs));
    }

    /**
     * Gets group members.
     *
     * @param realm       the realm
     * @param group       the group
     * @param firstResult the first result
     * @param maxResults  the max results
     * @return the group members
     */
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        String keyword = group.getId().replace(GazelleUserStorageProviderConstants.GAZELLE_PREFIX, "");
        Integer institutionId = gazelleDAO.getInstitutionId(keyword);
        if (institutionId == null) {
            return List.of();
        }
        return gazelleDAO.getUsersByInstitution(institutionId, firstResult, maxResults, rs -> mapUser(realm, rs));
    }

}
