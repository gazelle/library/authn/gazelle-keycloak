package net.ihe.gazelle.keycloak.provider.user.dao;

import java.sql.SQLException;

/**
 * Gazelle query exception.
 */
public class GazelleSQLException extends RuntimeException {

    /**
     * Instantiates a new Gazelle query exception.
     *
     * @param message message
     * @param cause   SQLException
     */
    public GazelleSQLException(String message, SQLException cause) {
        super(message, cause);
    }

}
