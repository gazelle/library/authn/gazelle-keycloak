package net.ihe.gazelle.keycloak.provider.user.roles;

import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import org.keycloak.component.ComponentModel;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * Role mapping provider, based on configuration
 */
public class RoleMappingProvider implements Supplier<Collection<String>> {
    private final ComponentModel storageProviderModel;

    /**
     * Instantiates a new Role mapping provider.
     *
     * @param storageProviderModel storage provider model
     */
    public RoleMappingProvider(ComponentModel storageProviderModel) {
        this.storageProviderModel = storageProviderModel;
    }

    @Override
    public Collection<String> get() {
        return storageProviderModel.getConfig().get(GazelleUserStorageProviderConstants.CONFIG_KEY_ROLE_MAPPING);
    }
}
