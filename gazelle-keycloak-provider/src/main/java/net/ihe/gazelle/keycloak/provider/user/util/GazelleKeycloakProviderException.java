package net.ihe.gazelle.keycloak.provider.user.util;

/**
 * Gazelle keycloak provider exception (as a runtime exception)
 */
public class GazelleKeycloakProviderException extends RuntimeException {

    /**
     * Instantiates a new Gazelle keycloak provider exception.
     *
     * @param message message
     */
    public GazelleKeycloakProviderException(String message) {
        super(message);
    }

}
