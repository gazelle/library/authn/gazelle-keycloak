package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import lombok.Value;

/**
 * DB config (cache key)
 */
@Value
public class GazelleDBConfig {

    String driverClass;

    String jdbcUrl;

    String username;

    String password;

}
