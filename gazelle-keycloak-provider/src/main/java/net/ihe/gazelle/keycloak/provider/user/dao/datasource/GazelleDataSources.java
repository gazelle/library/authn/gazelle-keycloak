package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.RemovalCause;
import io.agroal.api.AgroalDataSource;
import io.agroal.api.configuration.AgroalDataSourceConfiguration;
import io.agroal.api.configuration.supplier.AgroalDataSourceConfigurationSupplier;
import io.agroal.api.security.NamePrincipal;
import io.agroal.api.security.SimplePassword;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleSQLException;
import net.ihe.gazelle.keycloak.provider.user.util.FunctionWithSQLException;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.time.Duration;

import static io.agroal.api.configuration.AgroalConnectionFactoryConfiguration.TransactionIsolation.SERIALIZABLE;
import static io.agroal.api.configuration.AgroalConnectionPoolConfiguration.ConnectionValidator.defaultValidator;
import static java.time.Duration.ofSeconds;

/**
 * Gazelle data sources cache
 */
public class GazelleDataSources {

    public static final GazelleDataSources INSTANCE = new GazelleDataSources();

    /**
     * cache with datasources
     */
    final LoadingCache<GazelleDBConfig, AgroalDataSource> datasourceCache = Caffeine.newBuilder()
            // remove datasource after 1 hour unused
            .expireAfterAccess(Duration.ofHours(1))
            // close datasource on removal
            .removalListener(this::close)
            // loader
            .build(this::open);

    final FunctionWithSQLException<AgroalDataSourceConfigurationSupplier, AgroalDataSource> dataSourceResolver;

    protected GazelleDataSources() {
        this(AgroalDataSource::from);
    }

    protected GazelleDataSources(FunctionWithSQLException<AgroalDataSourceConfigurationSupplier, AgroalDataSource> dataSourceResolver) {
        this.dataSourceResolver = dataSourceResolver;
    }

    /**
     * Gets Gazelle data source.
     *
     * @param gazelleDBConfig gazelle db config
     * @return gazelle data source
     */
    public DataSource getDataSource(GazelleDBConfig gazelleDBConfig) {
        return datasourceCache.get(gazelleDBConfig);
    }

    /**
     * Open agroal data source.
     *
     * @param gazelleDBConfig gazelle db config
     * @return agroal data source
     */
    protected AgroalDataSource open(GazelleDBConfig gazelleDBConfig) {
        // create an agroal datasource
        AgroalDataSourceConfigurationSupplier configuration = new AgroalDataSourceConfigurationSupplier()
                .dataSourceImplementation(AgroalDataSourceConfiguration.DataSourceImplementation.AGROAL)
                .metricsEnabled(false)
                .connectionPoolConfiguration(cp -> cp
                        .minSize(1)
                        .maxSize(5)
                        .initialSize(1)
                        .connectionValidator(defaultValidator())
                        .acquisitionTimeout(ofSeconds(5))
                        .leakTimeout(ofSeconds(5))
                        .validationTimeout(ofSeconds(50))
                        .reapTimeout(ofSeconds(500))
                        .connectionFactoryConfiguration(cf -> cf
                                .jdbcUrl(gazelleDBConfig.getJdbcUrl())
                                .connectionProviderClassName(gazelleDBConfig.getDriverClass())
                                .autoCommit(false)
                                .jdbcTransactionIsolation(SERIALIZABLE)
                                .principal(new NamePrincipal(gazelleDBConfig.getUsername()))
                                .credential(new SimplePassword(gazelleDBConfig.getPassword()))
                        )
                );
        try {
            return dataSourceResolver.apply(configuration);
        } catch (SQLException e) {
            throw new GazelleSQLException("Failed to create datasource", e);
        }
    }

    /**
     * Close.
     *
     * @param gazelleDBConfig  the gazelle db config
     * @param agroalDataSource the agroal data source
     * @param removalCause     the removal cause
     */
    protected void close(GazelleDBConfig gazelleDBConfig,
                         AgroalDataSource agroalDataSource,
                         RemovalCause removalCause) {
        // close datasource
        agroalDataSource.close();
    }

}
