package net.ihe.gazelle.keycloak.provider.user.groups;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.GazelleUser;
import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import org.keycloak.models.GroupModel;
import org.keycloak.models.RealmModel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Group service.
 */
@Slf4j
public class GroupService {

    private final GazelleDAO gazelleDAO;

    /**
     * Instantiates a new Group service.
     *
     * @param gazelleDAO gazelle dao
     */
    public GroupService(GazelleDAO gazelleDAO) {
        this.gazelleDAO = gazelleDAO;
    }

    /**
     * Update groups for a user
     *
     * @param gazelleUser gazelle user
     */
    public void updateGroups(GazelleUser gazelleUser) {
        if (gazelleUser == null || gazelleUser.getUsername() == null) {
            // is user is null or username is null, no action
            return;
        }
        // retrieve institution from gazelle db
        // a single institution for the moment
        List<String> institutionsKeyword = gazelleDAO.getInstitutionKeywords(gazelleUser.getUsername());
        log.debug("mapUser {}, institutions : {}", gazelleUser.getUsername(), institutionsKeyword);
        // update user groups according to institution
        updateGroups(gazelleUser, institutionsKeyword);
    }

    private void updateGroups(GazelleUser gazelleUser, List<String> institutionsKeyword) {
        // list existing groups for user
        Set<String> existingGroups = gazelleUser.getGroups().stream().map(GroupModel::getId)
                .filter(groupId -> groupId.startsWith(GazelleUserStorageProviderConstants.GAZELLE_PREFIX))
                .collect(Collectors.toSet());
        Set<String> wantedGroups;
        if (institutionsKeyword == null) {
            wantedGroups = Set.of();
        } else {
            wantedGroups = institutionsKeyword.stream().map(keyword -> GazelleUserStorageProviderConstants.GAZELLE_PREFIX + keyword)
                    .collect(Collectors.toSet());
        }
        log.debug("existing groups for {} : {}", gazelleUser.getUsername(), existingGroups);
        log.debug("wanted groups for {} : {}", gazelleUser.getUsername(), wantedGroups);

        // compute groups to add
        Set<String> groupsToAdd = new HashSet<>(wantedGroups);
        groupsToAdd.removeAll(existingGroups);

        // compute groups to remove
        Set<String> groupsToRemove = new HashSet<>(existingGroups);
        groupsToRemove.removeAll(wantedGroups);

        // perform pending group actions

        for (String groupId : groupsToRemove) {
            log.info("removing group {} for {}", groupId, gazelleUser.getUsername());
            gazelleUser.leaveGroup(getGroupModel(gazelleUser.getRealm(), groupId));
        }
        for (String groupId : groupsToAdd) {
            log.info("adding group {} for {}", groupId, gazelleUser.getUsername());
            gazelleUser.joinGroup(getGroupModel(gazelleUser.getRealm(), groupId));
        }
    }

    private GroupModel getGroupModel(RealmModel realm, String groupId) {
        log.debug("check group exists : {}", groupId);
        // retrieve group from realm
        GroupModel group = realm.getGroupById(groupId);
        if (group == null) {
            // group doesn't exist in realm, create it
            log.info("creating group {}", groupId);
            String name = groupId.replace(GazelleUserStorageProviderConstants.GAZELLE_PREFIX, "");
            group = realm.createGroup(groupId, name);
        }
        return group;
    }

}
