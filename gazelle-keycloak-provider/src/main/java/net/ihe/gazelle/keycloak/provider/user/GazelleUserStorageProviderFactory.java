package net.ihe.gazelle.keycloak.provider.user;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleQueryExecutor;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleSQLException;
import net.ihe.gazelle.keycloak.provider.user.dao.datasource.GazelleConnectionProvider;
import org.keycloak.component.ComponentModel;
import org.keycloak.component.ComponentValidationException;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.provider.ProviderConfigurationBuilder;
import org.keycloak.storage.UserStorageProviderFactory;

import java.util.List;

import static net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants.*;

/**
 * Gazelle user storage provider factory.
 * <p>
 * Declared in src/main/resources/META-INF/services/org.keycloak.storage.UserStorageProviderFactory
 */
@Slf4j
public class GazelleUserStorageProviderFactory implements UserStorageProviderFactory<GazelleUserStorageProvider> {
    /**
     * Config metadata.
     */
    protected final List<ProviderConfigProperty> configMetadata;

    /**
     * Instantiates a new Gazelle user storage provider factory.
     */
    public GazelleUserStorageProviderFactory() {
        log.debug("GazelleUserStorageProviderFactory created");

        // Create config metadata
        configMetadata = ProviderConfigurationBuilder.create()
                .property()
                .name(CONFIG_KEY_DRIVER_CLASS)
                .label("JDBC Driver")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("org.postgresql.Driver")
                .helpText("JDBC Driver used to connect to the user database")
                .add()
                .property()
                .name(CONFIG_KEY_JDBC_URL)
                .label("JDBC URL")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("jdbc:postgresql://postgres:5432/gazelle")
                .helpText("JDBC URL used to connect to the user database")
                .add()
                .property()
                .name(CONFIG_KEY_DB_USERNAME)
                .label("Database User")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("gazelle")
                .helpText("Username used to connect to the database")
                .add()
                .property()
                .name(CONFIG_KEY_DB_PASSWORD)
                .label("Database Password")
                .type(ProviderConfigProperty.STRING_TYPE)
                .defaultValue("gazelle")
                .helpText("Password used to connect to the database")
                .secret(true)
                .add()
                .property()
                .name(CONFIG_KEY_VALIDATION_QUERY)
                .label("SQL Validation Query")
                .type(ProviderConfigProperty.STRING_TYPE)
                .helpText("SQL query used to validate a connection")
                .defaultValue("select 1")
                .add()
                .property()
                .name(CONFIG_KEY_ROLE_MAPPING)
                .label("Role mappings")
                .type(ProviderConfigProperty.MULTIVALUED_STRING_TYPE)
                .helpText("Role mapping (gazelle_role=keycloak_role)")
                .add()
                .build();

    }

    @Override
    public GazelleUserStorageProvider create(KeycloakSession session, ComponentModel componentModel) {
        log.debug("creating new GazelleUserStorageProvider");
        return new GazelleUserStorageProvider(session, componentModel);
    }

    @Override
    public String getId() {
        log.debug("getId()");
        return "gazelle-user-provider";
    }


    // Configuration support methods
    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configMetadata;
    }

    @Override
    public void validateConfiguration(KeycloakSession session, RealmModel realm, ComponentModel componentModel) {

        log.info("Testing connection...");
        GazelleQueryExecutor gazelleQueryExecutor = new GazelleQueryExecutor(new GazelleConnectionProvider(componentModel));
        try {
            gazelleQueryExecutor.executeQuerySingleResult(
                    componentModel.get(CONFIG_KEY_VALIDATION_QUERY),
                    List.of(),
                    rs -> null);
            log.debug("Connection OK !");
        } catch (GazelleSQLException gazelleSQLException) {
            throw new ComponentValidationException(gazelleSQLException);
        }
    }

    @Override
    public void onUpdate(KeycloakSession session, RealmModel realm, ComponentModel oldModel, ComponentModel newModel) {
        log.debug("onUpdate()");
    }

    @Override
    public void onCreate(KeycloakSession session, RealmModel realm, ComponentModel model) {
        log.debug("onCreate()");
    }
}
