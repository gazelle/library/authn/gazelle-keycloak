package net.ihe.gazelle.keycloak.provider.user.roles;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.GazelleUser;
import net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import org.keycloak.models.RealmModel;
import org.keycloak.models.RoleModel;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Role service.
 */
@Slf4j
public class RoleService {

    public static String getKeycloakId(String roleName) {
        // this creates a UUID based on roleName
        // gazelle: is add before generating a UUID using role name string
        String nameForId = GazelleUserStorageProviderConstants.GAZELLE_PREFIX + roleName;
        return UUID.nameUUIDFromBytes(nameForId.getBytes(StandardCharsets.UTF_8)).toString();
    }

    private final GazelleDAO gazelleDAO;
    private final Supplier<Collection<String>> roleMappingProvider;

    /**
     * Instantiates a new Role service.
     *
     * @param gazelleDAO          gazelle dao
     * @param roleMappingProvider role mapping provider
     */
    public RoleService(GazelleDAO gazelleDAO, Supplier<Collection<String>> roleMappingProvider) {
        this.gazelleDAO = gazelleDAO;
        this.roleMappingProvider = roleMappingProvider;
    }

    /**
     * Update roles for a user
     *
     * @param gazelleUser gazelle user
     */
    public void updateRoles(GazelleUser gazelleUser) {
        if (gazelleUser == null || gazelleUser.getUsername() == null) {
            // is user is null or username is null, no action
            return;
        }
        // retrieve roles from gazelle db
        Set<String> roleNames = getRoleNames(gazelleUser.getUsername());
        log.debug("mapUser {}, roleNames : {}", gazelleUser.getUsername(), roleNames);
        // update user roles according to gazelle roles
        updateRoles(gazelleUser, roleNames);

    }

    private Set<String> getRoleNames(String username) {
        // retrieve user roles in gazelle db
        Set<String> roles = new TreeSet<>(gazelleDAO.getRoles(username));

        // check role mappings
        Collection<String> roleMappings = roleMappingProvider.get();
        if (roleMappings != null) {
            for (String roleMapping : roleMappings) {
                String[] split = roleMapping.split("=");
                if (split.length == 2 && !split[0].isBlank() && roles.contains(split[0])) {
                    roles.add(split[1]);
                }
            }
        }
        return roles;
    }

    private void updateRoles(GazelleUser gazelleUser, Set<String> updatedRoles) {
        // list existing roles for user
        Set<String> existingRoles = gazelleUser.getRoleMappings().stream()
                .filter(roleModel -> roleModel.getId() != null)
                // gazelle roles are those with their id computed using role name
                .filter(roleModel -> roleModel.getId().equals(getKeycloakId(roleModel.getName())))
                .map(RoleModel::getName).collect(Collectors.toSet());
        log.debug("existing roles for {} : {}", gazelleUser.getUsername(), existingRoles);
        log.debug("wanted roles for {} : {}", gazelleUser.getUsername(), updatedRoles);

        // compute roles to add
        Set<String> rolesToAdd = new HashSet<>(updatedRoles);
        rolesToAdd.removeAll(existingRoles);

        // compute roles to remove
        Set<String> rolesToRemove = new HashSet<>(existingRoles);
        rolesToRemove.removeAll(updatedRoles);

        // perform pending role actions

        for (String roleName : rolesToRemove) {
            log.info("removing role {} for {}", roleName, gazelleUser.getUsername());
            gazelleUser.deleteRoleMapping(getRoleModel(gazelleUser.getRealm(), roleName));
        }
        for (String roleName : rolesToAdd) {
            log.info("adding role {} for {}", roleName, gazelleUser.getUsername());
            gazelleUser.grantRole(getRoleModel(gazelleUser.getRealm(), roleName));
        }

        String gazelleRoles = "[" + updatedRoles.stream().sorted().collect(Collectors.joining(",")) + "]";
        gazelleUser.setGazelleRoles(gazelleRoles);
    }

    private RoleModel getRoleModel(RealmModel realm, String roleName) {
        // retrieve role from realm
        RoleModel role = realm.getRole(roleName);
        if (role == null) {
            // if role doesn't exist realm, create it
            String id = getKeycloakId(roleName);
            log.info("creating role {} {}", id, roleName);
            role = realm.addRole(id, roleName);
        }
        return role;
    }

}
