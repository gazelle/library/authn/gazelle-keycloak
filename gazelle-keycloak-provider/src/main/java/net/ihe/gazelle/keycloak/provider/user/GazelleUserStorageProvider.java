package net.ihe.gazelle.keycloak.provider.user;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.dao.GazelleDAO;
import net.ihe.gazelle.keycloak.provider.user.dao.datasource.GazelleConnectionProvider;
import net.ihe.gazelle.keycloak.provider.user.groups.GroupService;
import net.ihe.gazelle.keycloak.provider.user.roles.RoleMappingProvider;
import net.ihe.gazelle.keycloak.provider.user.roles.RoleService;
import net.ihe.gazelle.keycloak.provider.user.service.GazelleUserService;
import org.keycloak.component.ComponentModel;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialInputUpdater;
import org.keycloak.credential.CredentialInputValidator;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.cache.UserCache;
import org.keycloak.models.credential.PasswordCredentialModel;
import org.keycloak.storage.ReadOnlyException;
import org.keycloak.storage.StorageId;
import org.keycloak.storage.UserStorageProvider;
import org.keycloak.storage.user.UserLookupProvider;
import org.keycloak.storage.user.UserQueryProvider;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Gazelle user storage provider.
 * <p>
 * Implement interfaces from Keycloak SPI
 * <p>
 * Allows finding/listing users and validate credentials
 */
@Slf4j
public class GazelleUserStorageProvider implements UserStorageProvider,
        UserLookupProvider,
        CredentialInputValidator,
        CredentialInputUpdater,
        UserQueryProvider {

    private final KeycloakSession session;
    private final GazelleUserService gazelleUserService;

    /**
     * Instantiates a new Gazelle user storage provider.
     *
     * @param session              keycloak session
     * @param storageProviderModel provider configuration
     */
    public GazelleUserStorageProvider(KeycloakSession session, ComponentModel storageProviderModel) {
        this(session, storageProviderModel, new GazelleConnectionProvider(storageProviderModel));
    }

    public GazelleUserStorageProvider(KeycloakSession session, ComponentModel storageProviderModel,
                                      GazelleConnectionProvider gazelleConnectionProvider) {
        this(session, storageProviderModel, new GazelleDAO(gazelleConnectionProvider));
    }

    public GazelleUserStorageProvider(KeycloakSession session, ComponentModel storageProviderModel, GazelleDAO gazelleDAO) {
        this(session,
                storageProviderModel,
                new GroupService(gazelleDAO),
                new RoleService(gazelleDAO, new RoleMappingProvider(storageProviderModel)),
                gazelleDAO);
    }

    public GazelleUserStorageProvider(KeycloakSession session, ComponentModel storageProviderModel, GroupService groupService, RoleService roleService, GazelleDAO gazelleDAO) {
        this(session, new GazelleUserService(session, storageProviderModel, gazelleDAO, groupService, roleService));
    }

    public GazelleUserStorageProvider(KeycloakSession session, GazelleUserService gazelleUserService) {
        this.session = session;
        this.gazelleUserService = gazelleUserService;
    }

    @Override
    public void close() {
        log.debug("close()");
    }

    @Override
    public UserModel getUserById(String id, RealmModel realm) {
        log.debug("getUserById({})", id);
        // parse id
        StorageId sid = new StorageId(id);
        // external id is gazelle username
        return gazelleUserService.getUserByUsername(realm, sid.getExternalId());
    }

    @Override
    public UserModel getUserByUsername(String username, RealmModel realm) {
        log.debug("getUserByUsername({})", username);
        return gazelleUserService.getUserByUsername(realm, username);
    }

    @Override
    public UserModel getUserByEmail(String email, RealmModel realm) {
        log.debug("getUserByEmail({})", email);
        return gazelleUserService.getUserByEmail(realm, email);
    }

    @Override
    public boolean supportsCredentialType(String credentialType) {
        log.debug("supportsCredentialType({})", credentialType);
        return PasswordCredentialModel.TYPE.equals(credentialType);
    }

    @Override
    public boolean isConfiguredFor(RealmModel realm, UserModel user, String credentialType) {
        log.debug("isConfiguredFor(realm={},user={},credentialType={})", realm.getName(), user.getUsername(), credentialType);
        // In our case, password is the only type of credential, so we allways return 'true' if
        // this is the credentialType
        return supportsCredentialType(credentialType);
    }

    @Override
    public boolean isValid(RealmModel realm, UserModel user, CredentialInput credentialInput) {
        // validate password

        log.debug("isValid(realm={},user={},credentialInput.type={})", realm.getName(), user.getUsername(), credentialInput.getType());
        if (!this.supportsCredentialType(credentialInput.getType())) {
            return false;
        }
        // parse id
        StorageId sid = new StorageId(user.getId());
        // external id is gazelle username
        String username = sid.getExternalId();

        // check password validity
        boolean valid = gazelleUserService.validatePassword(username, credentialInput.getChallengeResponse());
        if (valid) {
            // force refresh of user with updated attributes
            session.getProvider(UserCache.class).evict(realm, user);
        }
        return valid;
    }

    // UserQueryProvider implementation

    @Override
    public int getUsersCount(RealmModel realm) {
        log.debug("getUsersCount: realm={}", realm.getName());
        return gazelleUserService.getUsersCount();
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm) {
        // retrieve all users
        return getUsers(realm, 0, 500_000);
    }

    @Override
    public List<UserModel> getUsers(RealmModel realm, int firstResult, int maxResults) {
        log.debug("getUsers: realm={}", realm.getName());
        return gazelleUserService.getUsers(realm, firstResult, maxResults);
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm) {
        // retrieve all users matching a search
        return searchForUser(search, realm, 0, 5000);
    }

    @Override
    public int getUsersCount(RealmModel realm, Set<String> groupIds) {
        return gazelleUserService.getUsersCount(null, groupIds);
    }

    @Override
    public int getUsersCount(RealmModel realm, String search) {
        return gazelleUserService.getUsersCount(search, null);
    }

    @Override
    public int getUsersCount(RealmModel realm, String search, Set<String> groupIds) {
        return gazelleUserService.getUsersCount(search, groupIds);
    }

    @Override
    public int getUsersCount(RealmModel realm, Map<String, String> params) {
        log.debug("getUsersCount: realm={}", realm.getName());
        return gazelleUserService.getUsersCount();
    }

    @Override
    public int getUsersCount(RealmModel realm, Map<String, String> params, Set<String> groupIds) {
        return gazelleUserService.getUsersCount(null, groupIds);
    }

    @Override
    public int getUsersCount(RealmModel realm, boolean includeServiceAccount) {
        log.debug("getUsersCount: realm={}, includeServiceAccount={}", realm.getName(), includeServiceAccount);
        return gazelleUserService.getUsersCount();
    }

    @Override
    public List<UserModel> searchForUser(String search, RealmModel realm, int firstResult, int maxResults) {
        log.debug("searchForUser: realm={}", realm.getName());
        return gazelleUserService.searchForUser(realm, search, firstResult, maxResults);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm) {
        return searchForUser(params, realm, 0, 5000);
    }

    @Override
    public List<UserModel> searchForUser(Map<String, String> params, RealmModel realm, int firstResult, int maxResults) {
        // we can't search by specific attribute, return all users
        return getUsers(realm, firstResult, maxResults);
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group, int firstResult, int maxResults) {
        return gazelleUserService.getGroupMembers(realm, group, firstResult, maxResults);
    }

    @Override
    public List<UserModel> getGroupMembers(RealmModel realm, GroupModel group) {
        return getGroupMembers(realm, group, 0, 5000);
    }

    @Override
    public List<UserModel> searchForUserByUserAttribute(String attrName, String attrValue, RealmModel realm) {
        return searchForUser(Map.of(attrName, attrValue), realm);
    }

    // CredentialInputUpdater

    @Override
    public boolean updateCredential(RealmModel realm, UserModel user, CredentialInput input) {
        if (supportsCredentialType(input.getType())) {
            // parse id
            StorageId sid = new StorageId(user.getId());
            // external id is gazelle username
            String username = sid.getExternalId();

            // check password validity
            return gazelleUserService.updatePassword(username, input.getChallengeResponse());
        } else {
            throw new ReadOnlyException("user is read only for this update");
        }
    }

    @Override
    public void disableCredentialType(RealmModel realm, UserModel user, String credentialType) {
        // noop
    }

    @Override
    public Set<String> getDisableableCredentialTypes(RealmModel realm, UserModel user) {
        return Set.of();
    }

}
