package net.ihe.gazelle.keycloak.provider.user.dao.datasource;

import net.ihe.gazelle.keycloak.provider.user.util.SupplierWithSQLException;
import org.keycloak.component.ComponentModel;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants.*;

/**
 * Gazelle connection provider.
 */
public class GazelleConnectionProvider implements SupplierWithSQLException<Connection> {

    // current datasource
    protected DataSource dataSource;

    /**
     * Instantiates a new Gazelle connection provider.
     *
     * @param componentModel Provider configuration
     */
    public GazelleConnectionProvider(ComponentModel componentModel) {
        GazelleDBConfig gazelleDBConfig = new GazelleDBConfig(
                componentModel.get(CONFIG_KEY_DRIVER_CLASS),
                componentModel.get(CONFIG_KEY_JDBC_URL),
                componentModel.get(CONFIG_KEY_DB_USERNAME),
                componentModel.get(CONFIG_KEY_DB_PASSWORD)
        );
        this.dataSource = GazelleDataSources.INSTANCE.getDataSource(gazelleDBConfig);
    }

    @Override
    public Connection get() throws SQLException {
        // create a connection from provider configuration
        return dataSource.getConnection();
    }

}
