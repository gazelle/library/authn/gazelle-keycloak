package net.ihe.gazelle.keycloak.provider.user;

/**
 * Gazelle user storage provider constants.
 */
public class GazelleUserStorageProviderConstants {

    private GazelleUserStorageProviderConstants() {
        // utility class
    }

    /**
     * The constant CONFIG_KEY_DRIVER_CLASS.
     */
    public static final String CONFIG_KEY_DRIVER_CLASS = "jdbcDriver";
    /**
     * The constant CONFIG_KEY_JDBC_URL.
     */
    public static final String CONFIG_KEY_JDBC_URL = "jdbcUrl";
    /**
     * The constant CONFIG_KEY_DB_USERNAME.
     */
    public static final String CONFIG_KEY_DB_USERNAME = "username";
    /**
     * The constant CONFIG_KEY_DB_PASSWORD.
     */
    public static final String CONFIG_KEY_DB_PASSWORD = "password";
    /**
     * The constant CONFIG_KEY_VALIDATION_QUERY.
     */
    public static final String CONFIG_KEY_VALIDATION_QUERY = "validationQuery";

    /**
     * The constant CONFIG_KEY_ROLE_MAPPING.
     */
    public static final String CONFIG_KEY_ROLE_MAPPING = "roleMapping";

    /**
     * The constant GAZELLE_PREFIX.
     */
    public static final String GAZELLE_PREFIX = "gazelle:";

}
