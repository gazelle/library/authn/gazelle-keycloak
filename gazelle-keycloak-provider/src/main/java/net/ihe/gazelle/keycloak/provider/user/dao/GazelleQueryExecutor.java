package net.ihe.gazelle.keycloak.provider.user.dao;

import lombok.extern.slf4j.Slf4j;
import net.ihe.gazelle.keycloak.provider.user.util.FunctionWithSQLException;
import net.ihe.gazelle.keycloak.provider.user.util.GazelleKeycloakProviderException;
import net.ihe.gazelle.keycloak.provider.user.util.SupplierWithSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Util for accessing Gazelle DB
 */
@Slf4j
public class GazelleQueryExecutor {

    /**
     * Connection supplier.
     */
    protected final SupplierWithSQLException<Connection> connectionSupplier;

    /**
     * Instantiates a new Gazelle query executor.
     *
     * @param connectionSupplier connection supplier
     */
    public GazelleQueryExecutor(SupplierWithSQLException<Connection> connectionSupplier) {
        this.connectionSupplier = connectionSupplier;
    }

    /**
     * Execute query with a single result
     *
     * @param <T>        output type
     * @param sql        sql
     * @param parameters sql parameters
     * @param mapper     resultset mapper
     * @return unique result
     */
    public <T> T executeQuerySingleResult(String sql, List<Object> parameters,
                                          FunctionWithSQLException<ResultSet, T> mapper) {
        List<T> result = executeQuery(sql, parameters, mapper);
        // no result
        if (result.isEmpty()) {
            return null;
        }
        // more than two results
        if (result.size() > 1) {
            return null;
        }
        // return single result
        return result.get(0);
    }

    /**
     * Execute query with a list as result
     *
     * @param <T>        output type
     * @param sql        sql
     * @param parameters sql parameters
     * @param mapper     resultset mapper
     * @return result list
     */
    public <T> List<T> executeQuery(String sql, List<Object> parameters,
                                    FunctionWithSQLException<ResultSet, T> mapper) {
        log.debug("Query : {}", sql);
        return doInTransaction(c -> {
            List<T> result = new ArrayList<>();
            PreparedStatement st = c.prepareStatement(sql);
            try {
                // set parameters
                for (int i = 0; i < parameters.size(); i++) {
                    st.setObject(i + 1, parameters.get(i));
                }
                // execute query
                st.execute();
                ResultSet rs = st.getResultSet();

                // map each result line
                while (rs.next()) {
                    result.add(mapper.apply(rs));
                }
            } finally {
                st.close();
            }
            return result;
        });
    }

    /**
     * Execute update query
     *
     * @param sql        sql
     * @param parameters sql parameters
     * @return result list
     */
    public int executeUpdate(String sql, List<Object> parameters) {
        log.debug("Query : {}", sql);
        return doInTransaction(c -> {
            PreparedStatement st = c.prepareStatement(sql);
            try {
                // set parameters
                for (int i = 0; i < parameters.size(); i++) {
                    st.setObject(i + 1, parameters.get(i));
                }
                // execute query
                return st.executeUpdate();
            } finally {
                st.close();
            }
        });
    }

    protected <A> A doInTransaction(FunctionWithSQLException<Connection, A> f) {
        // retrieve connection
        Connection connection = getConnection();

        boolean initialAutocommit = false;
        A returnValue;
        try {
            // set auto commit as false
            initialAutocommit = disableAutoCommit(connection);
            // perform something on connection
            returnValue = f.apply(connection);
            connection.commit();
            return returnValue;
        } catch (SQLException sqlException) {
            try {
                // rollback exception
                connection.rollback();
            } catch (SQLException e) {
                log.error("Failed to rollback", e);
            }
            throw new GazelleSQLException("Failed to execute query", sqlException);
        } finally {
            closeConnection(connection, initialAutocommit);
        }
    }

    private Connection getConnection() {
        try {
            Connection connection = connectionSupplier.get();
            if (connection == null) {
                throw new GazelleKeycloakProviderException("connection is null");
            }
            return connection;
        } catch (SQLException e) {
            throw new GazelleSQLException("failed to get connection", e);
        }
    }

    private boolean disableAutoCommit(Connection connection) {
        try {
            boolean initialAutocommit = connection.getAutoCommit();
            connection.setAutoCommit(false);
            return initialAutocommit;
        } catch (SQLException e) {
            throw new GazelleSQLException("failed to disable autocommit", e);
        }
    }

    private void closeConnection(Connection connection, boolean initialAutocommit) {
        try {
            // reset connection auto commit
            if (initialAutocommit) {
                connection.setAutoCommit(true);
            }
            // close connection
            connection.close();
        } catch (SQLException e) {
            log.error("Failed to close connection", e);
        }
    }

}