package net.ihe.gazelle.keycloak.provider.user;

import org.keycloak.component.ComponentModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.storage.adapter.AbstractUserAdapterFederatedStorage;

/**
 * Gazelle user for Keycloak.
 * <p>
 * Based on AbstractUserAdapterFederatedStorage for storing attributes in Keycloak
 */
public class GazelleUser extends AbstractUserAdapterFederatedStorage {
    public static final String GAZELLE_ROLES_ATTRIBUTE = "GAZELLE_ROLES";

    /**
     * username
     */
    String username;

    /**
     * Instantiates a new Gazelle user.
     *
     * @param session        keycloak session
     * @param realm          keycloak realm
     * @param componentModel provider configuration
     * @param username       username
     * @param email          email
     * @param firstName      first name
     * @param lastName       last name
     */
    public GazelleUser(KeycloakSession session, RealmModel realm, ComponentModel componentModel,
                       String username, String email, String firstName, String lastName) {
        // calls super
        super(session, realm, componentModel);
        // this is the single attribute that should be present in this class
        setUsername(username);
        // set attributes (storing data in keycloak)
        this.setEmail(email);
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGazelleRoles(String gazelleRoles) {
        setSingleAttribute(GAZELLE_ROLES_ATTRIBUTE, gazelleRoles);
    }

    public String getGazelleRoles() {
        return getFirstAttribute(GAZELLE_ROLES_ATTRIBUTE);
    }

    public RealmModel getRealm() {
        return this.realm;
    }

    @Override
    public boolean equals(Object o) {
        // based on id
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        // based on id
        return super.hashCode();
    }
}