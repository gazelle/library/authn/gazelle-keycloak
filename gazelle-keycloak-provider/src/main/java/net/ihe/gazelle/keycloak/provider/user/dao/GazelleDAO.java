package net.ihe.gazelle.keycloak.provider.user.dao;

import net.ihe.gazelle.keycloak.provider.user.util.FunctionWithSQLException;
import net.ihe.gazelle.keycloak.provider.user.util.SupplierWithSQLException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.*;
import java.util.stream.Collectors;

import static net.ihe.gazelle.keycloak.provider.user.GazelleUserStorageProviderConstants.GAZELLE_PREFIX;

/**
 * Gazelle DAO
 */
public class GazelleDAO {

    /**
     * The constant BASE_QUERY.
     */
    public static final String BASE_QUERY = "select distinct username, firstname, lastname, email from usr_users uu";

    /**
     * The constant USERNAME.
     */
    public static final String USERNAME = "username";

    /**
     * The constant EMAIL.
     */
    public static final String EMAIL = "email";

    /**
     * The constant FIRSTNAME.
     */
    public static final String FIRSTNAME = "firstname";

    /**
     * The constant LASTNAME.
     */
    public static final String LASTNAME = "lastname";

    /**
     * The constant BASE_WHERE.
     *
     * User MUST be activated and not blocked.
     *
     * username and email must be unique, case insensitive
     *
     */
    public static final String BASE_WHERE = "activated = true and blocked = false and " +
            "not exists (select 1 from usr_users uu2 where (lower(uu.username) = lower(uu2.username) or lower(uu.email) = lower(uu2.email)) and (uu.username != uu2.username))";

    /**
     * The constant SQL_USER_BY_USERNAME.
     */
    public static final String SQL_USER_BY_USERNAME = BASE_QUERY + " where lower(username) = lower(?) AND " + BASE_WHERE;

    /**
     * The constant SQL_USER_BY_EMAIL.
     */
    public static final String SQL_USER_BY_EMAIL = BASE_QUERY + " where lower(email) = lower(?) AND " + BASE_WHERE;

    /**
     * The constant SQL_VERIFY_PASSWORD_BY_USERNAME.
     * <p>
     * For a more secure password storage, use this query :
     * "select (password = MD5(?) or (password = crypt(MD5(?), password))) from usr_users uu where lower(username) = lower(?) AND " + BASE_WHERE
     */
    public static final String SQL_VERIFY_PASSWORD_BY_USERNAME = "select password = MD5(?) " +
            "from usr_users uu where lower(username) = lower(?) AND " + BASE_WHERE;

    /**
     * The constant SQL_UPDATE_PASSWORD_BY_USERNAME.
     * <p>
     * For a more secure password storage, use this query :
     * update usr_users set password = crypt(MD5(?), gen_salt('bf', 12)) where lower(username) = lower(?)
     */
    public static final String SQL_UPDATE_PASSWORD_BY_USERNAME = "update usr_users set password = MD5(?) where lower(username) = lower(?)";

    /**
     * The constant SQL_USERS_COUNT.
     */
    public static final String SQL_USERS_COUNT = "select count(*) from usr_users uu where " + BASE_WHERE;

    /**
     * The constant ORDER_BY_USERNAME_LIMIT_OFFSET.
     */
    public static final String ORDER_BY_USERNAME_LIMIT_OFFSET = " order by username limit ? offset ?";

    /**
     * The constant SQL_USERS.
     */
    public static final String SQL_USERS = BASE_QUERY + " where " + BASE_WHERE + ORDER_BY_USERNAME_LIMIT_OFFSET;

    /**
     * The constant SQL_USER_SEARCH.
     */
    public static final String SQL_USER_SEARCH = BASE_QUERY + " where (username ILIKE ? or email ILIKE ?) and " + BASE_WHERE +
            ORDER_BY_USERNAME_LIMIT_OFFSET;

    /**
     * The constant SQL_USERS_BY_INSTITUTION_ID.
     */
    public static final String SQL_USERS_BY_INSTITUTION_ID = BASE_QUERY + " where institution_id = ? and " + BASE_WHERE +
            ORDER_BY_USERNAME_LIMIT_OFFSET;

    /**
     * The constant INSTITUTION_QUERY.
     */
    public static final String INSTITUTION_QUERY = "select usr_institution.keyword as institution_keyword, " +
            "usr_institution.name as institution_name " +
            "from usr_users, usr_institution " +
            "where usr_users.institution_id = usr_institution.id and " +
            "lower(usr_users.username) = lower(?)";
    /**
     * The constant INSTITUTION_KEYWORD.
     */
    public static final String INSTITUTION_KEYWORD = "institution_keyword";

    /**
     * The constant ROLES_QUERY.
     */
    public static final String ROLES_QUERY = "select usr_role.name as role_name " +
            "from usr_role, usr_user_role, usr_users " +
            "where usr_users.id = usr_user_role.user_id and " +
            "usr_user_role.role_id = usr_role.id and " +
            "lower(usr_users.username) = lower(?)";

    /**
     * The constant INSTITUTION_ID.
     */
    public static final String INSTITUTION_ID = "id";

    /**
     * The constant SQL_INSTITUTION_BY_KEYWORD.
     */
    public static final String SQL_INSTITUTION_BY_KEYWORD = "select id " +
            "from usr_institution " +
            "where lower(keyword) = lower(?)";

    /**
     * The constant ROLE_NAME.
     */
    public static final String ROLE_NAME = "role_name";

    private final GazelleQueryExecutor gazelleQueryExecutor;

    /**
     * Instantiates a new Gazelle DAO.
     *
     * @param gazelleConnectionProvider Gazelle connection provider
     */
    public GazelleDAO(SupplierWithSQLException<Connection> gazelleConnectionProvider) {
        this.gazelleQueryExecutor = new GazelleQueryExecutor(gazelleConnectionProvider);
    }

    /**
     * Gets roles for a username
     *
     * @param username username
     * @return roles
     */
    public Set<String> getRoles(String username) {
        if (username == null) {
            // no roles if username is null
            return Set.of();
        }
        // returns a sorted set of roles
        return new TreeSet<>(gazelleQueryExecutor.executeQuery(
                ROLES_QUERY,
                List.of(username),
                rs -> rs.getString(ROLE_NAME)
        ));
    }

    /**
     * Gets institution keywords for username
     *
     * @param username username
     * @return institution keywords
     */
    public List<String> getInstitutionKeywords(String username) {
        if (username == null) {
            // no institutions if username is null
            return List.of();
        }
        // retrieve institution keyword in gazelle db
        // returns a list, but contains a single institution at the moment
        return gazelleQueryExecutor.executeQuery(
                INSTITUTION_QUERY,
                List.of(username),
                rs -> rs.getString(INSTITUTION_KEYWORD)
        );
    }

    /**
     * Gets a user by its username.
     *
     * @param <T>      user type
     * @param username username
     * @param mapper   mapper
     * @return user by username
     */
    public <T> T getUserByUsername(String username, FunctionWithSQLException<ResultSet, T> mapper) {
        if (username == null) {
            // returns null if no username
            return null;
        }
        // execute query based on username
        return gazelleQueryExecutor.executeQuerySingleResult(
                SQL_USER_BY_USERNAME,
                List.of(username),
                mapper
        );
    }

    /**
     * Gets user by email.
     *
     * @param <T>    user type
     * @param email  email
     * @param mapper mapper
     * @return user by email
     */
    public <T> T getUserByEmail(String email, FunctionWithSQLException<ResultSet, T> mapper) {
        if (email == null) {
            // returns null if no mail
            return null;
        }
        // execute query based on username
        return gazelleQueryExecutor.executeQuerySingleResult(
                SQL_USER_BY_EMAIL,
                List.of(email),
                mapper
        );
    }

    /**
     * Verify password for username.
     *
     * @param username      username
     * @param inputPassword inputPassword
     * @return password for username
     */
    public boolean verifyPasswordForUsername(String username, String inputPassword) {
        if (username == null || inputPassword == null) {
            // returns false if no username or no password
            return false;
        }
        // execute query based on username
        Boolean result = gazelleQueryExecutor.executeQuerySingleResult(
                SQL_VERIFY_PASSWORD_BY_USERNAME,
                List.of(inputPassword, username),
                rs -> rs.getBoolean(1)
        );
        if (result == null) {
            return false;
        }
        return result;
    }

    public boolean updatePasswordForUsername(String username, String inputPassword) {
        if (username == null || inputPassword == null) {
            // returns false if no username or no password
            return false;
        }
        // execute query based on username
        int modified = gazelleQueryExecutor.executeUpdate(
                SQL_UPDATE_PASSWORD_BY_USERNAME,
                List.of(inputPassword, username)
        );
        return (modified == 1);
    }

    /**
     * Gets users count.
     *
     * @return users count
     */
    public Integer getUsersCount() {
        return gazelleQueryExecutor.executeQuerySingleResult(
                SQL_USERS_COUNT,
                List.of(),
                rs -> rs.getInt(1)
        );
    }

    public Integer getUsersCount(String search, Set<String> groupIds) {

        String sql = SQL_USERS_COUNT;
        List<Object> parameters = new ArrayList<>();
        if (search != null) {
            String s = "%" + search + "%";
            sql = sql + " AND (username ILIKE ? or email ILIKE ?)";
            parameters.add(s);
            parameters.add(s);
        }
        if (groupIds != null && !groupIds.isEmpty()) {
            Set<Integer> institutionIds = groupIds.stream().filter(Objects::nonNull).filter(id -> id.startsWith(GAZELLE_PREFIX))
                    .map(id -> id.replace(GAZELLE_PREFIX, ""))
                    .map(this::getInstitutionId)
                    .collect(Collectors.toSet());
            if (!institutionIds.isEmpty()) {
                sql = sql + " AND institution_id in (" +
                        institutionIds.stream().map(s -> "?").collect(Collectors.joining(",")) +
                        ")";
                parameters.addAll(institutionIds);
            }
        }
        return gazelleQueryExecutor.executeQuerySingleResult(
                sql,
                parameters,
                rs -> rs.getInt(1)
        );
    }

    /**
     * Gets users.
     *
     * @param <T>         user type
     * @param firstResult offset
     * @param maxResults  limit
     * @param mapper      mapper
     * @return users
     */
    public <T> List<T> getUsers(int firstResult, int maxResults, FunctionWithSQLException<ResultSet, T> mapper) {
        // retrieve users with offset/limit
        return gazelleQueryExecutor.executeQuery(
                SQL_USERS,
                List.of(maxResults, firstResult),
                mapper
        );
    }

    /**
     * Search for user.
     *
     * @param <T>         user type
     * @param search      search (username or email)
     * @param firstResult offset
     * @param maxResults  limit
     * @param mapper      mapper
     * @return list of users
     */
    public <T> List<T> searchForUser(String search, int firstResult, int maxResults, FunctionWithSQLException<ResultSet, T> mapper) {
        if (search == null) {
            // if no search provided, return all users
            return getUsers(firstResult, maxResults, mapper);
        }
        // retrieve all users with email or username matching search, with limit and offset
        String s = "%" + search + "%";
        return gazelleQueryExecutor.executeQuery(
                SQL_USER_SEARCH,
                List.of(s, s, maxResults, firstResult),
                mapper
        );
    }

    /**
     * Gets institution id by keyword.
     *
     * @param keyword institution keyword
     * @return institution id
     */
    public Integer getInstitutionId(String keyword) {
        if (keyword == null) {
            // return null if no keyword
            return null;
        }
        return gazelleQueryExecutor.executeQuerySingleResult(
                SQL_INSTITUTION_BY_KEYWORD,
                List.of(keyword),
                rs -> rs.getInt(INSTITUTION_ID)
        );
    }

    /**
     * Gets users by institution id.
     *
     * @param <T>           user type
     * @param institutionId institution id
     * @param firstResult   offset
     * @param maxResults    limit
     * @param mapper        mapper
     * @return users of institution
     */
    public <T> List<T> getUsersByInstitution(Integer institutionId, int firstResult, int maxResults, FunctionWithSQLException<ResultSet, T> mapper) {
        if (institutionId == null) {
            // if id is null, returns an empty list
            return List.of();
        }
        // retrieve all users from institution, with limit and offset
        return gazelleQueryExecutor.executeQuery(
                SQL_USERS_BY_INSTITUTION_ID,
                List.of(institutionId, maxResults, firstResult),
                mapper
        );
    }

}
