# gazelle-keycloak-provider

## Goal

Use current Gazelle DB as a user source for a Keycloak realm.

Users should be able to login, getting their Gazelle roles and their institution (keyword) as group.

Users should be able to change their password through Keycloak, changing password hash in Gazelle.

## Keycloak provider

In [service file](src/main/resources/META-INF/services/org.keycloak.storage.UserStorageProviderFactory), the provider factory is declared.

Configuration metadata is declared in this [provider](src/main/java/net/ihe/gazelle/keycloak/provider/user/GazelleUserStorageProviderFactory.java).

It creates instances of [GazelleUserStorageProvider](src/main/java/net/ihe/gazelle/keycloak/provider/user/GazelleUserStorageProvider.java).

This class implements Keycloak interfaces :

- UserStorageProvider : "A class implementing this interface represents a user storage provider to Keycloak"
- UserLookupProvider : retrieve users by keycloak id/username/email
- UserQueryProvider : search for users
- CredentialInputValidator : validate user password
- CredentialInputUpdater : update user password

`GazelleUserStorageProvider` is only a facade calling [GazelleUserService](src/main/java/net/ihe/gazelle/keycloak/provider/user/service/GazelleUserService.java).
This service calls the DAO to interact with Gazelle DB.

`GazelleUserService` also calls `RoleService` and `GroupService` for updating roles/group(institution) of user.

In the end, Keycloak user is stored in a `GazelleUser`. It extends `AbstractUserAdapterFederatedStorage`, allowing to store attributes in Keycloak storage.

Connections are pooled using Agroal, provided from Keycloak 17 based on Quarkus. As providers are created dynamically, connection pool is cached in `GazelleDataSources`.

## Links

- [Using Custom User Providers with Keycloak](https://www.baeldung.com/java-keycloak-custom-user-providers)
- https://github.com/keycloak/keycloak-quickstarts/tree/latest/user-storage-jpa
- https://github.com/keycloak/keycloak-quickstarts/tree/latest/user-storage-simple
- https://github.com/ArnaultMICHEL/keycloak-ldap-extdb-role-mapper
