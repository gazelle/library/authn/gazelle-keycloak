FROM quay.io/keycloak/keycloak:18.0.0

USER root
RUN microdnf install -y wget
USER keycloak

ENV KC_METRICS_ENABLED=true
ENV KC_HEALTH_ENABLED=true
ENV KC_DB=postgres

WORKDIR /opt/keycloak
RUN keytool -genkeypair -storepass password -storetype PKCS12 -keyalg RSA -keysize 2048 -dname "CN=server" -alias server -ext "SAN:c=DNS:localhost,IP:127.0.0.1" -keystore conf/server.keystore

CMD ["start-dev", "--import-realm"]

COPY gazelle-keycloak-provider/target/*.jar /opt/keycloak/providers/
COPY gazelle-keycloak-theme/target/*.jar /opt/keycloak/providers/
COPY gazelle-keycloak-cas-protocol/target/*.jar /opt/keycloak/providers/
COPY realm-gazelle-docker.json /opt/keycloak/data/import/
