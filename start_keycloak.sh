#!/bin/bash

mvn clean package -DskipTests

# replace config values
./create_realm_jsons.sh

echo "Starting Keycloak"
docker-compose up -d --remove-orphans --build

echo "Waiting for Keycloak"

wait-for-url() {
    echo "Testing $1"
    timeout -s TERM 45 bash -c \
    'while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' ${0})" != "200" ]];\
    do echo "Waiting for ${0}" && sleep 2;\
    done' ${1}
    echo "OK!"
    curl -I $1
}

wait-for-url http://keycloak.localhost:28080/realms/master

echo "Showing logs"
docker-compose logs -f
