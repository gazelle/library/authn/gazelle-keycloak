# gazelle-keycloak

This repository contains Keycloak modules for using Keycloak with Gazelle.

JDK 11+/Maven 3.8.1+ is required to build this project.

Installation documentation for production is in [https://gitlab.inria.fr/gazelle/documentation/gazelle-user-documentation](https://gitlab.inria.fr/gazelle/documentation/gazelle-user-documentation).

## Purpose

CAS is a great SSO tool but a modern IDP (Identity and Access Management) like [Keycloak](https://www.keycloak.org/) paves a better way for the future of Gazelle authentication tools.

## Strategy

As for CAS, Gazelle Keycloak :

- uses Gazelle TM database for retrieving user data (attributes, password verification).
- allows to customize UI, a custom theme is provided for login forms.
- provides CAS protocol endpoints, allowing to migrate away from CAS step by step.

## Local Keycloak

Docker and docker-compose are required for this step. It is possible to use a local Keycloak but this is out of the scope (JDK11, realm provisioning, ...).

If mails should be tested, start a mailhog Docker container using :

```docker run --rm -p 8025:8025 -p 1025:1025 mailhog/mailhog```

The mailhog is then accessible through [http://localhost:8025](http://localhost:8025). No message is sent over Internet.

It requires a Keycloak Postgres database :

```sql
CREATE USER keycloak;
ALTER USER keycloak WITH ENCRYPTED PASSWORD 'keycloak';
CREATE DATABASE "keycloak" OWNER keycloak;
```

Configure it in [.env](.env) :

```properties
KC_DB_URL=jdbc:postgresql://localhost:5432/keycloak
KC_DB_USERNAME=keycloak
KC_DB_PASSWORD=keycloak
```

The database is initialized by Keycloak.

Next, configure the location of Gazelle TM DB in [.env](.env) :

```properties
GAZELLE_DB_URL=jdbc:postgresql://localhost:5432/gazelle
DB_USER=gazelle
DB_PASSWORD=gazelle
```

Start Keycloak by executing [start_keycloak.sh](start_keycloak.sh).

To reimport a new version of realm, run [refresh_realm.sh](refresh_realm.sh).

Keycloak is available at [http://keycloak.localhost:28080/](http://keycloak.localhost:28080/).

To use this Keycloak as a CAS replacement, update `/opt/gazelle/cas/XXXX.properties` files with these properties :

```properties
casServerUrlPrefix=http://keycloak.localhost:28080/realms/gazelle/protocol/cas
casServerLoginUrl=http://keycloak.localhost:28080/realms/gazelle/protocol/cas/login
casLogoutUrl=http://keycloak.localhost:28080/realms/gazelle/protocol/cas/logout
```

## Gazelle realm lifecycle

Realm is stored in three files :

- [realm-gazelle-export.json](realm-gazelle-export.json) : the realm exported from Keycloak, with roles and clients
- [realm-gazelle.json](realm-gazelle.json) : derived resource, realm prepared for an import via Keycloak UI or `kc.sh import --file`
- [realm-gazelle-docker.json](realm-gazelle-docker.json) : derived resource, realm prepared for an import via `kc.sh start --import-realm`

`realm-gazelle-export.json` is transformed in derived resources with [create_realm_jsons.sh](create_realm_jsons.sh). This script is called in [start_keycloak.sh](start_keycloak.sh) and [refresh_realm.sh](refresh_realm.sh).

When changed, [realm-gazelle-docker.json](realm-gazelle-docker.json) should be copied in [indus project](https://gitlab.inria.fr/gazelle/private/industrialization/docker/gazelle-keycloak) .

## Modules

### gazelle-keycloak-provider

It allows to get and use Gazelle users in Keycloak.

Roles are imported as standard Keycloak roles, user institution is set in user groups.

See the [documentation](gazelle-keycloak-provider/README.md).

### gazelle-keycloak-theme

It provides a login theme for Gazelle.

See the [documentation](gazelle-keycloak-theme/README.md#development).

### gazelle-keycloak-cas-protocol

This module provides CAS protocol support for Keycloak.

See the [documentation](gazelle-keycloak-cas-protocol/README.md).

## Debug tokens

Clone https://github.com/authts/sample-angular-oidc-client-ts

Change src/environments/environment.ts :

```javascript
export const environment = {
  production: false,
  stsAuthority: "http://keycloak.localhost:28080/realms/gazelle/",
  clientId: "rocketchat",
  clientRoot: 'http://localhost:4200/',
  clientScope: 'openid profile email',
  apiRoot: 'https://demo.identityserver.io/api/',
};
``` 

Start with `ng serve` and open [client](http://localhost:4200/).

Login using Gazelle credentials and inspect tokens.
