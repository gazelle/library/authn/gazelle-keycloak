#!/bin/bash

# replace config values
./create_realm_jsons.sh

docker cp `pwd`/realm-gazelle.json "$(docker-compose ps -q keycloak)":/tmp/realm-gazelle.json
docker-compose exec -e DEBUG=false keycloak /opt/keycloak/bin/kc.sh import --file /tmp/realm-gazelle.json

