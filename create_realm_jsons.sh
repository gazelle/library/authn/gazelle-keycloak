#!/bin/bash

# replace config values with env vars definition
jq \
  '.smtpServer.auth = "true" |
  .smtpServer.user = "${SMTP_USERNAME}" |
  .smtpServer.password = "${SMTP_PASSWORD}" |
  .smtpServer.port = "${SMTP_PORT}" |
  .smtpServer.host = "${SMTP_HOST}" |
  .smtpServer.from = "no-reply@${FQDN}" |
  ( .clients[] | select(.clientId == "cas") ).redirectUris[0] |= "${REDIRECT_URI}" |
  ( .clients[] | select(.clientId == "rocketchat") ).redirectUris[0] |= "${REDIRECT_URI}" |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.jdbcUrl[0] = "${GAZELLE_DB_URL}" |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.username[0] = "${DB_USER}" |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.password[0] = "${DB_PASSWORD}"' \
   realm-gazelle-export.json > realm-gazelle-docker.json

# export variable defined in .env
export $(cat .env | xargs)

# replace config values with env vars values from .env
jq \
  '.smtpServer.auth = "true" |
  .smtpServer.user = env.SMTP_USERNAME |
  .smtpServer.password = env.SMTP_PASSWORD |
  .smtpServer.port = env.SMTP_PORT |
  .smtpServer.host = env.SMTP_HOST |
  .smtpServer.from = "no-reply@$localhost" |
  ( .clients[] | select(.clientId == "cas") ).redirectUris[0] |= env.REDIRECT_URI |
  ( .clients[] | select(.clientId == "rocketchat") ).redirectUris[0] |= env.REDIRECT_URI |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.jdbcUrl[0] = env.GAZELLE_DB_URL |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.username[0] = env.DB_USER |
  .components["org.keycloak.storage.UserStorageProvider"][0].config.password[0] = env.DB_PASSWORD' \
   realm-gazelle-export.json > realm-gazelle.json
