#!/bin/bash

export KEYCLOAK_VERSION=18.0.0
wget -O /tmp/keycloak-themes.jar "https://repo1.maven.org/maven2/org/keycloak/keycloak-themes/$KEYCLOAK_VERSION/keycloak-themes-$KEYCLOAK_VERSION.jar"
unzip -p /tmp/keycloak-themes.jar theme/base/login/login.ftl > src/main/resources/theme/gazelle/login/login.ftl
