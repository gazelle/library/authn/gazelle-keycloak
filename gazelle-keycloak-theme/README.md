# gazelle-keycloak-theme

Keycloak documentation : https://www.keycloak.org/docs/latest/server_development/#_themes

## Update theme

As login is a modified version of provided Keycloak version, a Keycloak upgrade might break things.

To reset the login.ftl to a newer version, see [this script](refresh_login_ftl.sh).

## Theming

### CSS

Changed background to lightgrey

Header is replaced with IHE logo. Text is not displayed with text-indent.

As the layout is responsive, the logo is resized on small screens.

## login.ftl

displayInfo is set to true, doesn't depend on Keycloak user registration activation.

In info section, the link to Gazelle registration is hardcoded (using URL configured in properties).

## Deployment

GAZELLE_URL should be provided as an env var (used by properties for registration link).

Output jar file should be put in /opt/keycloak/providers/ of Keycloak, and `kc.sh build` should be run.

## Development

Start `start_keycloak_theme_design.sh`, then connect to [Keycloak Administration Console](http://localhost:8080/admin/) (admin/admin).

Create a `gazelle` realm and set login them to `gazelle`.

Preview [login page](http://localhost:8080/realms/gazelle/account) by clicking on Login.
