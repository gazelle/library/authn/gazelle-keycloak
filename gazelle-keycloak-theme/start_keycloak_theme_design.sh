#!/bin/bash

export KEYCLOAK_VERSION=18.0.0
docker run \
   -p 8080:8080 \
   --name keycloak-testing-container \
   -e KEYCLOAK_ADMIN=admin \
   -e KEYCLOAK_ADMIN_PASSWORD=admin \
   -v `pwd`/src/main/resources/theme/gazelle:/opt/keycloak/themes/gazelle:rw \
   -it --rm "quay.io/keycloak/keycloak:$KEYCLOAK_VERSION" \
   start-dev --spi-theme-static-max-age=-1 --spi-theme-cache-themes=false --spi-theme-cache-templates=false
